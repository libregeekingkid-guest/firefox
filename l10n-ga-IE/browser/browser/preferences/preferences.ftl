# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

do-not-track-description = Inis do shuímh “Ná Lorgaítear Mé” mura bhfuil tú ag iarraidh go lorgófaí thú
do-not-track-learn-more = Tuilleadh eolais
do-not-track-option-default =
    .label = Agus Cosaint ar Lorgaireacht ar siúl amháin
do-not-track-option-always =
    .label = I gCónaí
pref-page =
    .title = { PLATFORM() ->
            [windows] Roghanna
           *[other] Sainroghanna
        }
pane-general-title = Ginearálta
category-general =
    .tooltiptext = { pane-general-title }
pane-search-title = Cuardaigh
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = Príobháideachas agus Slándáil
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = Cuntas Firefox
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = Tacaíocht { -brand-short-name }
focus-search =
    .key = f
close-button =
    .aria-label = Dún

## Browser Restart Dialog

feature-enable-requires-restart = Ní mór duit { -brand-short-name } a atosú chun an ghné seo a chur i bhfeidhm.
feature-disable-requires-restart = Ní mór duit { -brand-short-name } a atosú chun an ghné seo a dhíchumasú.
should-restart-title = Atosaigh { -brand-short-name }
should-restart-ok = Atosaigh { -brand-short-name } anois
revert-no-restart-button = Fill
restart-later = Atosaigh Ar Ball
