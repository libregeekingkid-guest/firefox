# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#### Security

# LOCALIZATION NOTE: phishBefore uses %S to represent the name of the provider
#                    whose privacy policy must be accepted (for enabling
#                    check-every-page-as-I-load-it phishing protection).
phishBeforeText=Odabirom ove opcije, adresa web stranice koju gledate bit će poslana u %S. Za nastavak, molim vas da pregledate i prihvatite sljedeće uvjete korištenja.

#### Fonts

labelDefaultFont=Uobičajeno (%S)

veryLargeMinimumFontTitle=Velika vrijednost za najmanji font
veryLargeMinimumFontWarning=Postavili ste veliku vrijednost za najmanju veličinu fonta (više od 24 piksela). Ovo može uzrokovati poteškoće i ozbiljne probleme kod korištenja važnih konfiguracijskih stranica, poput ove.
acceptVeryLargeMinimumFont=Svejedno zadrži moje promjene

#### Permissions Manager

trackingprotectionpermissionstext2=Na ovim stranicama ste isključili zaštitu od praćenja.
trackingprotectionpermissionstitle=Iznimke - zaštita od praćenja
cookiepermissionstext=Možete odabrati koje web stranice smiju ili ne smiju koristiti kolačiće. Upišite točnu adresu stranice koju želite urediti, te kliknite na Blokiraj, Dopusti za ovu sesiju ili Dopusti.
cookiepermissionstitle=Iznimke - kolačići
addonspermissionstext=Možete odabrati koje web stranice smiju instalirati dodatke. Upišite točnu adresu web stranice kojoj to želite dopustiti, te kliknite na Dopusti.
addons_permissions_title2=Dopuštene stranice - Instalacija dodataka
popuppermissionstext=Možete odrediti koje web stranice smiju prikazivati skočne prozore. Upišite točnu adresu web stranice kojoj to želite dopustiti, te kliknite na Dopusti.
popuppermissionstitle2=Dopuštene stranice - skočni prozori
notificationspermissionstext5=Sljedeće stranice zatražile su dozvolu da vam šalju obavijesti. Možete odrediti kojim web stranicama je dozvoljeno slati vam obavijesti.
notificationspermissionstitle2=Postavke - dozvole za obavijesti
locationpermissionstext=Sljedeće stranice zatražile su dozvolu za pristup vašoj lokaciji. Možete odrediti kojim web stranicama je dozvoljen pristup vašoj lokaciji.
locationpermissionstitle=Postavke - lokacijske dozvole
camerapermissionstext=Sljedeće stranice zatražile su dozvolu za pristup vašoj kameri. Možete odrediti kojim web stranicama je dozvoljen pristup vašoj kameri.
camerapermissionstitle=Postavke - dozvole pristupa kameri
microphonepermissionstext=Sljedeće stranice zatražile su dozvolu za pristup vašem mikrofonu. Možete odrediti kojim web stranicama je dozvoljen pristup vašem mikrofonu.
microphonepermissionstitle=Postavke - dozvole pristupa mikrofonu
invalidURI=Molim vas, upišite valjano ime poslužitelja
invalidURITitle=Upisano ime poslužitelja je netočno
savedLoginsExceptions_title=Iznimke - spremljene prijave
savedLoginsExceptions_desc3=Prijave za sljedeće stranice neće biti spremljene

# LOCALIZATION NOTE(pauseNotifications.label): %S is replaced with the
# brandShortName of the application.
pauseNotifications.label=Pauziraj obavijesti dok se %S ponovno ne pokrene
pauseNotifications.accesskey=n

#### Block List Manager

blockliststext=Možete odabrati koje će popise Firefox koristiti za blokiranje Web elemenata koji mogu pratiti vaše aktivnosti pretraživanja.
blockliststitle=Popis blokiranja
# LOCALIZATION NOTE (mozNameTemplate): This template constructs the name of the
# block list in the block lists dialog. It combines the list name and
# description.
#   e.g. mozNameTemplate : "Standard (Recommended). This list does a pretty good job."
#   %1$S = list name (fooName), %2$S = list descriptive text (fooDesc)
mozNameTemplate=%1$S %2$S
# LOCALIZATION NOTE (mozstdName, etc.): These labels appear in the tracking
# protection block lists dialog, mozNameTemplate is used to create the final
# string. Note that in the future these two strings (name, desc) could be
# displayed on two different lines.
mozstdName=Disconnect.me osnovna zaštita (preporučeno).
mozstdDesc=Dopušta neke pratitelje, da bi web stranice mogle ispravno funkcionirati.
mozfullName=Disconnect.me stroga zaštita.
mozfullDesc2=Blokira poznate pratitelje. Neke stranice možda neće ispravno funkcionirati.

#### Master Password

pw_change2empty_in_fips_mode=Trenutačno ste u FIPS modu. FIPS ne dopušta praznu glavnu lozinku.
pw_change_failed_title=Promjena lozinke nije uspjela

#### Fonts

# LOCALIZATION NOTE: Next two strings are for language name representations with
#   and without the region.
#   e.g. languageRegionCodeFormat : "French/Canada  [fr-ca]" languageCodeFormat : "French  [fr]"
#   %1$S = language name, %2$S = region name, %3$S = language-region code
languageRegionCodeFormat=%1$S/%2$S  [%3$S]
#   %1$S = language name, %2$S = language-region code
languageCodeFormat=%1$S  [%2$S]

#### Downloads

desktopFolderName=Radna površina
downloadsFolderName=Preuzimanja
chooseDownloadFolderTitle=Izaberite mapu za preuzimanja:

#### Applications

fileEnding=%S datoteka
saveFile=Spremi datoteku

# LOCALIZATION NOTE (useApp, useDefault): %S = Application name
useApp=Koristi %S
useDefault=Koristi %S (uobičajeno)

useOtherApp=Koristi drugi…
fpTitleChooseApp=Odaberite program pomoćnika
manageApp=Detalji programa…
webFeed=Web kanal
videoPodcastFeed=Video podcast
audioPodcastFeed=Podcast
alwaysAsk=Uvijek pitaj
portableDocumentFormat=Portable Document Format (PDF)

# LOCALIZATION NOTE (usePluginIn):
# %1$S = plugin name (for example "QuickTime Plugin-in 7.2")
# %2$S = brandShortName from brand.properties (for example "Minefield")
usePluginIn=Koristi %S (u %S)

# LOCALIZATION NOTE (previewInApp, addLiveBookmarksInApp): %S = brandShortName
previewInApp=Pogled u %S
addLiveBookmarksInApp=Dodaj živu zabilješku u %S

# LOCALIZATION NOTE (typeDescriptionWithType):
# %1$S = type description (for example "Portable Document Format")
# %2$S = type (for example "application/pdf")
typeDescriptionWithType=%S (%S)


#### Cookie Viewer

hostColon=Poslužitelj:
domainColon=Domena:
forSecureOnly=Samo kriptirane veze
forAnyConnection=Sve vrste veza
expireAtEndOfSession=Na kraju sesije
can=Dopusti
canAccessFirstParty=Dopusti samo od prve strane
canSession=Dopusti za sesiju
cannot=Blokiraj
prompt=Uvijek pitaj
noCookieSelected=<nema označenih kolačića>
cookiesAll=Sljedeći kolačići su pohranjeni na vašem računalu:
cookiesFiltered=Sljedeći kolačići odgovaraju vašem traženom pojmu:

# LOCALIZATION NOTE (removeAllCookies, removeAllShownCookies):
# removeAllCookies and removeAllShownCookies are both used on the same one button,
# never displayed together and can share the same accesskey.
# When only partial cookies are shown as a result of keyword search,
# removeAllShownCookies is displayed as button label.
# removeAllCookies is displayed when no keyword search and all cookies are shown.
removeAllCookies.label=Ukloni sve
removeAllCookies.accesskey=e
removeAllShownCookies.label=Ukloni sve prikazano
removeAllShownCookies.accesskey=s

# LOCALIZATION NOTE (removeSelectedCookies):
# Semicolon-separated list of plural forms. See:
# http://developer.mozilla.org/en/docs/Localization_and_Plurals
# If you need to display the number of selected elements in your language,
# you can use #1 in your localization as a placeholder for the number.
# For example this is the English string with numbers:
# removeSelectedCookied=Remove #1 Selected;Remove #1 Selected
removeSelectedCookies.label=Ukloni odab&rani;Ukloni odabrane;Ukloni odabrane
removeSelectedCookies.accesskey=R

defaultUserContextLabel=Niti jedan

####Preferences::Advanced::Network
#LOCALIZATION NOTE: The next string is for the disk usage of the web content cache.
#   e.g., "Your web content cache is currently using 200 MB"
#   %1$S = size
#   %2$S = unit (MB, KB, etc.)
actualDiskCacheSize=Vaša priručna memorija web sadržaja trenutno zauzima %1$S %2$S diskovnog prostora
actualDiskCacheSizeCalculated=Izračunavanje veličine privremene memorije web sadržaja…

####Preferences::Advanced::Network
#LOCALIZATION NOTE: The next string is for the disk usage of the application cache.
#   e.g., "Your application cache is currently using 200 MB"
#   %1$S = size
#   %2$S = unit (MB, KB, etc.)
actualAppCacheSize=Vaša priručna memorija aplikacija trenutno zauzima %1$S %2$S diskovnog prostora\u0020

####Preferences::Advanced::Network
#LOCALIZATION NOTE: The next string is for the total usage of site data.
#   e.g., "The total usage is currently using 200 MB"
#   %1$S = size
#   %2$S = unit (MB, KB, etc.)
totalSiteDataSize=Vaša podaci stranica trenutno zauzimaju %1$S %2$S diskovnog prostora
loadingSiteDataSize=Izračunavanje veličine podataka…
clearSiteDataPromptTitle=Obriši kolačiće i podatke pretraživanja
clearSiteDataPromptText=Odabiranje ‘Obriši sada’ će obrisati sve kolačiće i podatke pretraživanja u Firefoxu. Ovo će vas vjerovatno odjaviti s web stranica i ukloniti izvanmrežni web sadržaj.
clearSiteDataNow=Obriši sada
persistent=Trajno
siteUsage=%1$S %2$S
acceptRemove=Ukloni
# LOCALIZATION NOTE (siteDataSettings2.description): %S = brandShortName
siteDataSettings2.description=Sljedeće stranice pohranjuju podatke na vašem računalu. %S zadržava podatke stranica s trajnom pohranom dok ih ne obrišete, i briše podatke stranice koje nemaju trajnu pohranu kada zatreba prostora.
# LOCALIZATION NOTE (removeAllSiteData, removeAllSiteDataShown):
# removeAllSiteData and removeAllSiteDataShown are both used on the same one button,
# never displayed together and can share the same accesskey.
# When only partial sites are shown as a result of keyword search,
# removeAllShown is displayed as button label.
# removeAll is displayed when no keyword search and all sites are shown.
removeAllSiteData.label=Ukloni sve
removeAllSiteData.accesskey=e
removeAllSiteDataShown.label=Ukloni sve prikazane
removeAllSiteDataShown.accesskey=e
spaceAlert.learnMoreButton.label=Saznajte više
spaceAlert.learnMoreButton.accesskey=S
spaceAlert.over5GB.prefButton.label=Otvorite podešavanja
spaceAlert.over5GB.prefButton.accesskey=O
# LOCALIZATION NOTE (spaceAlert.over5GB.prefButtonWin.label): On Windows Preferences is called Options
spaceAlert.over5GB.prefButtonWin.label=Otvorite mogućnosti
spaceAlert.over5GB.prefButtonWin.accesskey=O
# LOCALIZATION NOTE (spaceAlert.over5GB.message): %S = brandShortName
spaceAlert.over5GB.message=%S ostaje bez prostora. Sadržaji stranica se možda neće dobro prikazati. Možete obrisati pohranjene podatke stranica u Postavke > Napredno > Podaci stranica.
# LOCALIZATION NOTE (spaceAlert.over5GB.messageWin):
# - On Windows Preferences is called Options
# - %S = brandShortName
spaceAlert.over5GB.messageWin=%S ostaje bez prostora. Sadržaji stranica se možda neće dobro prikazati. Možete obrisati pohranjene podatke stranica u Mogućnosti > Napredno > Podaci stranica.
spaceAlert.under5GB.okButton.label=U redu, razumijem
spaceAlert.under5GB.okButton.accesskey=U
# LOCALIZATION NOTE (spaceAlert.under5GB.message): %S = brandShortName
spaceAlert.under5GB.message=%S ostaje bez prostora. Sadržaji stranica se možda neće dobro prikazati. Posjetite “Saznajte više” kako biste optimizirali svoje korištenje diskovnog prostora za bolje iskustvo pretraživanja.

# LOCALIZATION NOTE (featureEnableRequiresRestart, featureDisableRequiresRestart, restartTitle): %S = brandShortName
featureDisableRequiresRestart=%S se mora ponovno pokrenuti da bi se onemogućila ova mogućnost
shouldRestartTitle=Ponovno pokreni %S
okToRestartButton=Ponovno pokreni %S sada
revertNoRestartButton=Povrat

restartNow=Ponovno pokreni
restartLater=Ponovno pokreni poslije

disableContainersAlertTitle=Zatvori sve sadržajne kartice?

# LOCALIZATION NOTE (disableContainersMsg): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #S is the number of container tabs
disableContainersMsg=Ukoliko sada onemogućite sadržajne kartice, #S sadržajna kartica biti će zatvorena. Jeste li sigurni da želite onemogućiti sadržajne kartice?;Ukoliko sada onemogućite sadržajne kartice, #S sadržajne kartice biti će zatvorene. Jeste li sigurni da želite onemogućiti sadržajne kartice?;Ukoliko sada onemogućite sadržajne kartice, #S sadržajnih kartica biti će zatvoreno. Jeste li sigurni da želite onemogućiti sadržajne kartice?

# LOCALIZATION NOTE (disableContainersOkButton): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #S is the number of container tabs
disableContainersOkButton=Zatvori #S sadržajnu karticu;Zatvori #S sadržajne kartice;Zatvori #S sadržajnih kartica

disableContainersButton2=Drži omogućeno

removeContainerAlertTitle=Ukloni ovaj spremnik?

# LOCALIZATION NOTE (removeContainerMsg): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #S is the number of container tabs
removeContainerMsg=Ukoliko sada uklonite ovaj spremnik, #S sadržajna kartica biti će zatvorena. Jeste li sigurni da želite ukloniti ovaj spremnik?;Ukoliko sada uklonite ovaj spremnik, #S sadržajne kartice biti će zatvorene. Jeste li sigurni da želite ukloniti ovaj spremnik?;Ukoliko sada uklonite ovaj spremnik, #S sadržajnih kartica biti će zatvoreno. Jeste li sigurni da želite ukloniti ovaj spremnik?

removeContainerOkButton=Ukloni ovaj spremnik
removeContainerButton2=Nemoj ukloniti ovaj spremnik

# Search Input
# LOCALIZATION NOTE: Please keep the placeholder string shorter than around 30 characters to avoid truncation.
searchInput.labelWin=Pronađi u Mogućnostima
searchInput.labelUnix=Pronađi u Postavkama

# Search Results Pane
# LOCALIZATION NOTE %S will be replaced by the word being searched
searchResults.sorryMessageWin=Žao nam je! Nema rezultata u Mogućnostima za “%S”.
searchResults.sorryMessageUnix=Žao nam je! Nema rezultata u Postavkama za “%S”.
# LOCALIZATION NOTE (searchResults.needHelp2): %1$S is a link to SUMO, %2$S is
# the browser name
searchResults.needHelp2=Trebate li pomoć? Posjetite <html:a id="need-help-link" target="_blank" href="%1$S">%2$S podrška</html:a>

# LOCALIZATION NOTE (searchResults.needHelp3): %S will be replaced with a link to the support page.
# The label of the link is in searchResults.needHelpSupportLink .
# LOCALIZATION NOTE (searchResults.needHelpSupportLink): %S will be replaced with the browser name.

# LOCALIZATION NOTE %S is the default value of the `dom.ipc.processCount` pref.
defaultContentProcessCount=%S (zadano)

# LOCALIZATION NOTE (extensionControlled.homepage_override):
# This string is shown to notify the user that their home page is being controlled by an extension.
extensionControlled.homepage_override = Dodatak %S kontrolira vašu početnu stranicu.


# LOCALIZATION NOTE (extensionControlled.newTabURL):
# This string is shown to notify the user that their new tab page is being controlled by an extension.
extensionControlled.newTabURL = Proširenje, %S, kontrolira vašu stranicu novog taba.


# LOCALIZATION NOTE (extensionControlled.defaultSearch):
# This string is shown to notify the user that the default search engine is being controlled
# by an extension. %S is the icon and name of the extension.

# LOCALIZATION NOTE (extensionControlled.privacy.containers):
# This string is shown to notify the user that Container Tabs are being enabled by an extension
# %S is the container addon controlling it
extensionControlled.privacy.containers = Dodatak, %S, zahtijeva sadržajne kartice.

# LOCALIZATION NOTE (extensionControlled.websites.trackingProtectionMode):
# This string is shown to notify the user that their tracking protection preferences are being controlled by an extension.

# LOCALIZATION NOTE (extensionControlled.proxyConfig):
# This string is shown to notify the user that their proxy configuration preferences are being controlled by an extension.
# %1$S is the icon and name of the extension.
# %2$S is the brandShortName from brand.properties (for example "Nightly")

# LOCALIZATION NOTE (extensionControlled.enable):
# %1$S is replaced with the icon for the add-ons menu.
# %2$S is replaced with the icon for the toolbar menu.
# This string is shown to notify the user how to enable an extension that they disabled.

# LOCALIZATION NOTE (connectionDesc.label):
# %S is the brandShortName from brand.properties (for example "Nightly")
