<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "
Podatkovni vir pinga:
">
<!ENTITY aboutTelemetry.showCurrentPingData "
Trenutni podatki pinga
">
<!ENTITY aboutTelemetry.showArchivedPingData "
Arhivirani podatki pinga
">
<!ENTITY aboutTelemetry.showSubsessionData "
Prikaži podatke podseje
">
<!ENTITY aboutTelemetry.choosePing "
Izberite ping:
">
<!ENTITY aboutTelemetry.archivePingType "
Vrsta pinga
">
<!ENTITY aboutTelemetry.archivePingHeader "
Ping
">
<!ENTITY aboutTelemetry.optionGroupToday "
Danes
">
<!ENTITY aboutTelemetry.optionGroupYesterday "
Včeraj
">
<!ENTITY aboutTelemetry.optionGroupOlder "
Starejše
">
<!ENTITY aboutTelemetry.payloadChoiceHeader "
  Koristna vsebina">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Telemetrija">
<!ENTITY aboutTelemetry.moreInformations "Iščete več informacij?">
<!ENTITY aboutTelemetry.firefoxDataDoc "<a>Dokumentacija o Firefoxovih podatkih</a> vsebuje vodnike o tem, kako uporabljati naša podatkovna orodja.">
<!ENTITY aboutTelemetry.telemetryClientDoc "<a>Dokumentacija o Firefoxovem odjemalcu za telemetrijo</a> vsebuje definicije konceptov, dokumentacijo API in sklice podatkov.">
<!ENTITY aboutTelemetry.telemetryDashboard "<a>Pregledne plošče telemetrije</a> omogočajo predstavitev podatkov, ki jih Mozilla prejme preko telemetrije.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "Odpri v pregledovalniku JSON">

<!ENTITY aboutTelemetry.homeSection "Domov">
<!ENTITY aboutTelemetry.generalDataSection "
  Splošni podatki
">
<!ENTITY aboutTelemetry.environmentDataSection "
  Podatki o okolju">
<!ENTITY aboutTelemetry.sessionInfoSection "Podatki o seji">
<!ENTITY aboutTelemetry.scalarsSection "
  Skalarji">
<!ENTITY aboutTelemetry.keyedScalarsSection "
  Skalarji s ključi">
<!ENTITY aboutTelemetry.histogramsSection "
  Histogrami
">
<!ENTITY aboutTelemetry.keyedHistogramsSection "
  Histogrami s ključi
">
<!ENTITY aboutTelemetry.eventsSection "
  Dogodki">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "
  Enostavne meritve
">
<!ENTITY aboutTelemetry.telemetryLogSection "
  Dnevnik telemetrije
">
<!ENTITY aboutTelemetry.slowSqlSection "
  Počasni stavki SQL
">
<!ENTITY aboutTelemetry.chromeHangsSection "
  Sesutja brskalnika
">
<!ENTITY aboutTelemetry.addonDetailsSection "
  Podrobnosti dodatkov
">
<!ENTITY aboutTelemetry.capturedStacksSection "
  Zajeti skladi">
<!ENTITY aboutTelemetry.lateWritesSection "
  Poznejša pisanja
">
<!ENTITY aboutTelemetry.rawPayloadSection "Neobdelana koristna vsebina">
<!ENTITY aboutTelemetry.raw "Neobdelan JSON">

<!ENTITY aboutTelemetry.fullSqlWarning "
  Opomba: Vključeno je razhroščevanje počasnega SQL. Spodaj se lahko pojavijo celotni nizi SQL, vendar ne bodo poslani telemetriji.
">
<!ENTITY aboutTelemetry.fetchStackSymbols "
  Zbiraj imena funkcij za sklade">
<!ENTITY aboutTelemetry.hideStackSymbols "
  Prikaži neobdelane podatke sklada">
