<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  trackingProtectionHeader2.label      "İzlənmə Qoruması">
<!ENTITY  trackingProtection2.description      "İzləmə saytlar arası səyahət məlumatlarının toplanmasıdır. İzləmə səyahətiniz və şəxsi məlumatlarınızı yığmaqla profilinizi hazırlamaq və sizə reklam məzmunları göstərmək üçün işlədilə bilər.">
<!ENTITY  trackingProtection2.radioGroupLabel  "Bilinən izləyiciləri bloklamaq üçün İzlənmə Qorumasını işlədin">
<!ENTITY  trackingProtection3.description      "İzlənmə Qoruması saytlar arası səyahət məlumatlarınızı yığan onlayn izləyiciləri əngəlləyir.">
<!ENTITY  trackingProtection3.radioGroupLabel  "Bilinin izləyiciləri əngəlləmək üçün İzlənmə Qorumasını işlədin">
<!ENTITY  trackingProtectionAlways.label       "Həmişə">
<!ENTITY  trackingProtectionAlways.accesskey   "H">
<!ENTITY  trackingProtectionPrivate.label      "Ancaq Məxfi Pəncərələrdə">
<!ENTITY  trackingProtectionPrivate.accesskey  "n">
<!ENTITY  trackingProtectionNever.label        "Heç vaxt">
<!ENTITY  trackingProtectionNever.accesskey    "v">
<!ENTITY  trackingProtectionLearnMore.label    "Ətraflı öyrən">
<!ENTITY  trackingProtectionLearnMore2.label    "İzlənmə Qoruması və məxfiliyiniz haqqında ətraflı öyrənin">
<!ENTITY  trackingProtectionExceptions.label   "İstisnalar…">
<!ENTITY  trackingProtectionExceptions.accesskey "a">

<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the release and beta channel. -->
<!ENTITY trackingProtectionPBM6.label         "Bilinən izləyiciləri bloklamaq üçün Məxfi Səyahətdə İzlənmə Qorumasını işlədin">
<!ENTITY trackingProtectionPBM6.accesskey     "v">
<!ENTITY trackingProtectionPBMLearnMore.label "Ətraflı öyrən">
<!ENTITY changeBlockList2.label               "Qara Siyahını Dəyiş…">
<!ENTITY changeBlockList2.accesskey           "C">

<!ENTITY  doNotTrack.description        'Saytlara "Məni İzləmə" siqnalı göndərərək izlənmək istəmədiyinizi bildirin'>
<!ENTITY  doNotTrack.learnMore.label    "Ətraflı öyrən">
<!ENTITY  doNotTrack.default.label      "Ancaq İzlənmə Qoruması işlədərkən">
<!ENTITY  doNotTrack.always.label       "Həmişə">

<!ENTITY  history.label                 "Tarixçə">
<!ENTITY  permissions.label             "İcazələr">

<!ENTITY  addressBar.label              "Ünvan Sətri">
<!ENTITY  addressBar.suggest.label      "Ünvan sətrindən istifadə edərkən bunları təklif et:">
<!ENTITY  locbar.history2.label         "Səyahət tarixçəsi">
<!ENTITY  locbar.history2.accesskey     "H">
<!ENTITY  locbar.bookmarks.label        "Əlfəcinlər">
<!ENTITY  locbar.bookmarks.accesskey    "l">
<!ENTITY  locbar.openpage.label         "Açıq vərəqlər">
<!ENTITY  locbar.openpage.accesskey     "A">
<!ENTITY  locbar.searches.label         "Əsas axtarış mühərrikindən uyğun axtarışlar">
<!ENTITY  locbar.searches.accesskey     "y">

<!ENTITY  suggestionSettings2.label     "Axtarış mühərriyi təklifləri üçün nizamlamaları dəyiş">

<!ENTITY  acceptCookies2.label          "Saytlardan çərəzləri qəbul et">
<!ENTITY  acceptCookies2.accesskey      "A">

<!ENTITY  acceptThirdParty2.pre.label     "Üçüncü tərəf çərəzlərini qəbul et">
<!ENTITY  acceptThirdParty2.pre.accesskey "y">
<!ENTITY  acceptThirdParty.always.label   "Həmişə">
<!ENTITY  acceptThirdParty.never.label    "Heç vaxt">
<!ENTITY  acceptThirdParty.visited.label  "Baxılanlardan">

<!ENTITY  keepUntil2.label              "Saxlama limiti">
<!ENTITY  keepUntil2.accesskey          "u">

<!ENTITY  expire.label                  "vaxtı bitənədək">
<!ENTITY  close.label                   "&brandShortName; qapatanadək">

<!ENTITY  cookieExceptions.label        "İstisnalar…">
<!ENTITY  cookieExceptions.accesskey    "a">

<!ENTITY  showCookies.label             "Çərəzləri Göstər…">
<!ENTITY  showCookies.accesskey         "G">

<!ENTITY  historyHeader2.pre.label         "&brandShortName; səyyahı">
<!ENTITY  historyHeader2.pre.accesskey     "w">
<!ENTITY  historyHeader.remember.label     "Tarixçəni xatırlayacaq">
<!ENTITY  historyHeader.dontremember.label "Tarixçəni heç vaxt xatırlama">
<!ENTITY  historyHeader.custom.label       "Tarixçə üçün fərdi tənzimləmələri işlət">
<!ENTITY  historyHeader.post.label         "">

<!ENTITY  rememberDescription.label      "&brandShortName; sizin səyahət, endirmə, forma və axtarış tarixçənizi xatırlayacaq, və daxil olduğunuz veb saytlardan çərəzləri saxlayacaq.">

<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->
<!ENTITY  rememberActions.pre.label           "İstəsəniz ">
<!ENTITY  rememberActions.clearHistory.label  "yaxın tarixçənizi">
<!ENTITY  rememberActions.middle.label        " və ya ">
<!ENTITY  rememberActions.removeCookies.label "fərdi çərəzləri silə">
<!ENTITY  rememberActions.post.label          " bilərsiz.">

<!ENTITY  dontrememberDescription.label  "&brandShortName; gizli baxışdakı nizamlamalardan istifadə edəcək və siz Web-də gəzərkən keçmişlə bağlı heçnə xatırlamayacaq.">

<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->
<!ENTITY  dontrememberActions.pre.label          "Əlavə olaraq">
<!ENTITY  dontrememberActions.clearHistory.label "bütün keçmişinizi də silə bilirsiniz">
<!ENTITY  dontrememberActions.post.label         "açıqlanmışdır.">

<!ENTITY  privateBrowsingPermanent2.label "Həmişə gizli baxış rejimindən istifadə et">
<!ENTITY  privateBrowsingPermanent2.accesskey "g">

<!ENTITY  rememberHistory2.label      "Endirmə və Tarixçəmi yadda saxla">
<!ENTITY  rememberHistory2.accesskey  "l">

<!ENTITY  rememberSearchForm.label       "Axtarış və form keçmişini xatırla">
<!ENTITY  rememberSearchForm.accesskey   "f">

<!ENTITY  clearOnClose.label             "&brandShortName; bağlananda tarixçəni sil">
<!ENTITY  clearOnClose.accesskey         "s">

<!ENTITY  clearOnCloseSettings.label     "Nizamlar…">
<!ENTITY  clearOnCloseSettings.accesskey "a">

<!ENTITY  browserContainersLearnMore.label      "Ətraflı öyrən">
<!ENTITY  browserContainersEnabled.label        "Konteyner Vərəqlərini Aktivləşdir">
<!ENTITY  browserContainersEnabled.accesskey    "n">
<!ENTITY  browserContainersSettings.label        "Tənzimləmələr…">
<!ENTITY  browserContainersSettings.accesskey    "i">

<!ENTITY  a11yPrivacy.checkbox.label     "Əlçatanlıq xidmətlərinin səyyahınıza girişə icazə verilməsinin qabağın al">
<!ENTITY  a11yPrivacy.checkbox.accesskey "a">
<!ENTITY  a11yPrivacy.learnmore.label    "Ətraflı öyrən">
<!ENTITY enableSafeBrowsingLearnMore.label "Ətraflı öyrən">
