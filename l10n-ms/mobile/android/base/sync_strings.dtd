<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Sambung ke &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Untuk aktifkan peranti baru anda, pilih “Tatacara &syncBrand.shortName.label;” pada peranti anda.'>
<!ENTITY sync.subtitle.pair.label 'Untuk mengaktifkan, pilih “Gandingkan peranti” pada peranti anda yang lain.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Peranti tersebut tidak berada dengan saya…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Log masuk'>
<!ENTITY sync.configure.engines.title.history 'Sejarah'>
<!ENTITY sync.configure.engines.title.tabs 'Tab'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; pada &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Menu Tandabuku'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Tag'>
<!ENTITY bookmarks.folder.toolbar.label 'Bar alatan Tandabuku'>
<!ENTITY bookmarks.folder.other.label 'Tandabuku Lain'>
<!ENTITY bookmarks.folder.desktop.label 'Tandabuku Desktop'>
<!ENTITY bookmarks.folder.mobile.label 'Tandabuku Telefon'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Dipinkan'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Kembali untuk melayar'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Selamat Datang ke &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Log masuk untuk sync tab anda, tandabuku, log masuk &amp; banyak lagi.'>
<!ENTITY fxaccount_getting_started_get_started 'Mulakan'>
<!ENTITY fxaccount_getting_started_old_firefox 'Menggunakan versi lama &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Akaun pelayan'>
<!ENTITY fxaccount_status_sync_now 'Sync sekarang'>
<!ENTITY fxaccount_status_syncing2 'Sedang sync…'>
<!ENTITY fxaccount_status_device_name 'Nama peranti'>
<!ENTITY fxaccount_status_sync_server 'Pelayan Sync'>
<!ENTITY fxaccount_status_needs_verification2 'Akaun anda perlu disahkan. Tap untuk hantar semula email pengesahan.'>
<!ENTITY fxaccount_status_needs_credentials 'Tidak dapat menyambung. Tap untuk mendaftar masuk.'>
<!ENTITY fxaccount_status_needs_upgrade 'Anda perlu menaik taraf &brandShortName; untuk mendaftar masuk.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; telah ditetapkan, tetapi tidak automatik disync. Togol “data Auto-sync” dalam Android Tetapan &gt; Penggunaan Data.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; telah ditatacara, tetapi tidak dapat sync secara automatik. Togol “Auto-sync data” di dalam menu Tetapan Akaun Android &gt;.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Tap untuk daftar masuk ke Akaun Firefox baru anda.'>
<!ENTITY fxaccount_status_choose_what 'Pilih kandungan yang mahu diselaraskan'>
<!ENTITY fxaccount_status_bookmarks 'Tandabuku'>
<!ENTITY fxaccount_status_history 'Sejarah'>
<!ENTITY fxaccount_status_passwords2 'Log masuk'>
<!ENTITY fxaccount_status_tabs 'Buka tab'>
<!ENTITY fxaccount_status_additional_settings 'Tetapan Tambahan'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sync hanya melalui Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Halang &brandShortName; membuat penyelarasan melalui rangkaian selular atau bermeter'>
<!ENTITY fxaccount_status_legal 'Perundangan' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Terma perkhidmatan'>
<!ENTITY fxaccount_status_linkprivacy2 'Notis privasi'>
<!ENTITY fxaccount_remove_account 'Memutuskan sambungan &ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Putuskan sambungan Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Data pelayaran anda akan masih ada dalam peranti ini, tetapi tidak lagi sync dengan akaun anda.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Akaun Firefox &formatS; terputus.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Memutuskan'>

<!ENTITY fxaccount_enable_debug_mode 'Dayakan Mod Nyahpepijat'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; Pilihan'>
<!ENTITY fxaccount_options_configure_title 'Konfigur &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; tidak tersambung'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Tap untuk daftar masuk sebagai &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Siap menaik taraf &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Tap untuk daftar masuk sebagai &formatS;'>
