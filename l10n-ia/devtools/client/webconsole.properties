# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# LOCALIZATION NOTE
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.
# LOCALIZATION NOTE (browserConsole.title): shown as the
# title when opening the browser console popup
browserConsole.title=Consola del navigator
# LOCALIZATION NOTE (timestampFormat): %1$02S = hours (24-hour clock),
# %2$02S = minutes, %3$02S = seconds, %4$03S = milliseconds.
timestampFormat=%02S:%02S:%02S.%03S
helperFuncUnsupportedTypeError=Impossibile vocar pprint sur iste typo de objecto.
# LOCALIZATION NOTE (NetworkPanel.deltaDurationMS): this string is used to
# show the duration between two network events (e.g request and response
# header or response header and response body). Parameters: %S is the duration.
NetworkPanel.durationMS=%Sms

ConsoleAPIDisabled=Le API de registration del consola Web (console.log, console.info, console.warn, console.error) ha essite disactivate per un script de iste pagina.

# LOCALIZATION NOTE (webConsoleWindowTitleAndURL): the Web Console floating
# panel title. For RTL languages you need to set the LRM in the string to give
# the URL the correct direction. Parameters: %S is the web page URL.
webConsoleWindowTitleAndURL=Consola del web - %S

# LOCALIZATION NOTE (webConsoleXhrIndicator): the indicator displayed before
# a URL in the Web Console that was requested using an XMLHttpRequest.
# Should probably be the same as &btnConsoleXhr; in webConsole.dtd
webConsoleXhrIndicator=XHR

# LOCALIZATION NOTE (webConsoleMixedContentWarning): the message displayed
# after a URL in the Web Console that has been flagged for Mixed Content (i.e.
# http content in an https page).
webConsoleMixedContentWarning=Contento miscite

# LOCALIZATION NOTE (webConsoleMoreInfoLabel): the more info tag displayed
# after security related web console messages.
webConsoleMoreInfoLabel=Saper plus

# LOCALIZATION NOTE (scratchpad.linkText): the text used in the right hand
# side of the Web Console command line when JavaScript is being entered, to
# indicate how to jump into scratchpad mode.
scratchpad.linkText=Shift+RETURN - Aperir in le ardesia

# LOCALIZATION NOTE (reflow.*): the console displays reflow activity.
# We can get 2 kind of lines: with JS link or without JS link. It looks like
# that:
# reflow: 12ms
# reflow: 12ms function foobar, file.js line 42
# The 2nd line, from "function" to the end of the line, is a link to the
# JavaScript debugger.
reflow.messageWithNoLink=refluxo: %Sms
reflow.messageWithLink=refluxo: %Sms\u0020
reflow.messageLinkText=function %1$S, %2$S linea %3$S

# LOCALIZATION NOTE (stacktrace.anonymousFunction): this string is used to
# display JavaScript functions that have no given name - they are said to be
# anonymous. Test console.trace() in the webconsole.
stacktrace.anonymousFunction=<anonyme>

# LOCALIZATION NOTE (stacktrace.asyncStack): this string is used to
# indicate that a given stack frame has an async parent.
# %S is the "Async Cause" of the frame.
stacktrace.asyncStack=(Asynchrone: %S)

# LOCALIZATION NOTE (timerStarted): this string is used to display the result
# of the console.time() call. Parameters: %S is the name of the timer.
timerStarted=%S: chronometro initiate

# LOCALIZATION NOTE (timeEnd): this string is used to display the result of
# the console.timeEnd() call. Parameters: %1$S is the name of the timer, %2$S
# is the number of milliseconds.
timeEnd=%1$S: %2$Sms

# LOCALIZATION NOTE (consoleCleared): this string is displayed when receiving a
# call to console.clear() to let the user know the previous messages of the
# console have been removed programmatically.
consoleCleared=Le consola esseva vacuate.

# LOCALIZATION NOTE (noCounterLabel): this string is used to display
# count-messages with no label provided.
noCounterLabel=<nulle etiquetta>

# LOCALIZATION NOTE (noGroupLabel): this string is used to display
# console.group messages with no label provided.
noGroupLabel=<no gruppo de etiquettas>

# LOCALIZATION NOTE (Autocomplete.blank): this string is used when inputnode
# string containing anchor doesn't matches to any property in the content.
Autocomplete.blank=  <- nulle resultato

maxTimersExceeded=Le numero consentite maxime de temporisatores de iste pagina era superate.
timerAlreadyExists=Le temporisator “%S” ja existe.
timerDoesntExist=Le temporisator “%S” non existe.
timerJSError=Falta a processar del nomine del chronometro.

# LOCALIZATION NOTE (maxCountersExceeded): Error message shown when the maximum
# number of console.count()-counters was exceeded.
maxCountersExceeded=Le numero consentite maxime de contatores de iste pagina era superate.

# LOCALIZATION NOTE (longStringEllipsis): the string displayed after a long
# string. This string is clickable such that the rest of the string is
# retrieved from the server.
longStringEllipsis=[…]

# LOCALIZATION NOTE (longStringTooLong): the string displayed after the user
# tries to expand a long string.
longStringTooLong=Le catena de characteres que tu tenta vider es troppo longe pro ser monstrate per le consola web.

# LOCALIZATION NOTE (connectionTimeout): message displayed when the Remote Web
# Console fails to connect to the server due to a timeout.
connectionTimeout=Tempore de connexion expirate. Controla le consola de error sur ambe le extremitates pro possibile messages de error. Reaperi le consola del web e retenta.

# LOCALIZATION NOTE (propertiesFilterPlaceholder): this is the text that
# appears in the filter text box for the properties view container.
propertiesFilterPlaceholder=Filtrar le proprietates

# LOCALIZATION NOTE (emptyPropertiesList): the text that is displayed in the
# properties pane when there are no properties to display.
emptyPropertiesList=Nulle proprietates a monstrar

# LOCALIZATION NOTE (messageRepeats.tooltip2): the tooltip text that is displayed
# when you hover the red bubble that shows how many times a message is repeated
# in the web console output.
# This is a semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of message repeats
# example: 3 repeats
messageRepeats.tooltip2=#1 repetition;#1 repetitiones

# LOCALIZATION NOTE (openNodeInInspector): the text that is displayed in a
# tooltip when hovering over the inspector icon next to a DOM Node in the console
# output
openNodeInInspector=Clicca pro seliger le nodo in le inspector

# LOCALIZATION NOTE (cdFunctionInvalidArgument): the text that is displayed when
# cd() is invoked with an invalid argument.
cdFunctionInvalidArgument=Impossibile usar cd() in le fenestra date. Argumento non valide.

# LOCALIZATION NOTE (selfxss.msg): the text that is displayed when
# a new user of the developer tools pastes code into the console
# %1 is the text of selfxss.okstring
selfxss.msg=Attention al fraudes: paga attention quando tu colla cosas que tu non comprende. Illo poterea consentir a attaccantes de robar tu identitate o de prender le controlo de tu computator. Per favor scribe ‘%S’ ci infra (non necessita pulsar Enter) pro permitter de collar.

# LOCALIZATION NOTE (selfxss.msg): the string to be typed
# in by a new user of the developer tools when they receive the sefxss.msg prompt.
# Please avoid using non-keyboard characters here
selfxss.okstring=permitter de collar

# LOCALIZATION NOTE (messageToggleDetails): the text that is displayed when
# you hover the arrow for expanding/collapsing the message details. For
# console.error() and other messages we show the stacktrace.
messageToggleDetails=Monstrar/celar le detalios del message.

# LOCALIZATION NOTE (groupToggle): the text that is displayed when
# you hover the arrow for expanding/collapsing the messages of a group.
groupToggle=Monstrar/celar gruppo.

# LOCALIZATION NOTE (emptySlotLabel): the text is displayed when an Array
# with empty slots is printed to the console.
# This is a semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of empty slots
# example: 1 empty slot
# example: 5 empty slots
emptySlotLabel=#1 implaciamento disponibile;#1 implaciamentos disponibile

# LOCALIZATION NOTE (table.index, table.iterationIndex, table.key, table.value):
# the column header displayed in the console table widget.
table.index=(indice)
table.iterationIndex=(indice del iterationes)
table.key=Clave
table.value=Valores

# LOCALIZATION NOTE (severity.error, severity.warn, severity.info, severity.log):
# tooltip for icons next to console output
severity.error=Error
severity.warn=Advertentia
severity.info=Informationes
severity.log=Log

# LOCALIZATION NOTE (level.error, level.warn, level.info, level.log, level.debug):
# tooltip for icons next to console output
level.error=Error
level.warn=Advertentia
level.info=Informationes
level.log=Log
level.debug=Depuration

# LOCALIZATION NOTE (webconsole.find.key)
# Key shortcut used to focus the search box on upper right of the console
webconsole.find.key=CmdOrCtrl+F

# LOCALIZATION NOTE (webconsole.close.key)
# Key shortcut used to close the Browser console (doesn't work in regular web console)
webconsole.close.key=CmdOrCtrl+W

# LOCALIZATION NOTE (webconsole.clear.key*)
# Key shortcut used to clear the console output
webconsole.clear.key=Ctrl+Shift+L
webconsole.clear.keyOSX=Ctrl+L

# LOCALIZATION NOTE (webconsole.menu.copyURL.label)
# Label used for a context-menu item displayed for network message logs. Clicking on it
# copies the URL displayed in the message to the clipboard.
webconsole.menu.copyURL.label=Copiar le adresse del ligamine
webconsole.menu.copyURL.accesskey=a

# LOCALIZATION NOTE (webconsole.menu.openURL.label)
# Label used for a context-menu item displayed for network message logs. Clicking on it
# opens the URL displayed in a new browser tab.
webconsole.menu.openURL.label=Aperir le URL in un nove scheda
webconsole.menu.openURL.accesskey=s

# LOCALIZATION NOTE (webconsole.menu.openInNetworkPanel.label)
# Label used for a context-menu item displayed for network message logs. Clicking on it
# opens the network message in the Network panel
webconsole.menu.openInNetworkPanel.label=Aperir in le pannello de rete
webconsole.menu.openInNetworkPanel.accesskey=N

# LOCALIZATION NOTE (webconsole.menu.openInVarView.label)
# Label used for a context-menu item displayed for object/variable logs. Clicking on it
# opens the webconsole variable view for the logged variable.
webconsole.menu.openInVarView.label=Aperir in le vista de variabiles
webconsole.menu.openInVarView.accesskey=V

# LOCALIZATION NOTE (webconsole.menu.storeAsGlobalVar.label)
# Label used for a context-menu item displayed for object/variable logs. Clicking on it
# creates a new global variable pointing to the logged variable.
webconsole.menu.storeAsGlobalVar.label=Stockar como variabile global
webconsole.menu.storeAsGlobalVar.accesskey=S

# LOCALIZATION NOTE (webconsole.menu.copy.label)
# Label used for a context-menu item displayed for any log. Clicking on it will copy the
# content of the log (or the user selection, if any).
webconsole.menu.copy.label=Copiar
webconsole.menu.copy.accesskey=C

# LOCALIZATION NOTE (webconsole.menu.copyMessage.label)
# Label used for a context-menu item displayed for any log. Clicking on it will copy the
# content of the log (or the user selection, if any).
webconsole.menu.copyMessage.label=Copiar le message
webconsole.menu.copyMessage.accesskey=C

# LOCALIZATION NOTE (webconsole.menu.copyObject.label)
# Label used for a context-menu item displayed for object/variable log. Clicking on it
# will copy the object/variable.
webconsole.menu.copyObject.label=Copiar le objecto
webconsole.menu.copyObject.accesskey=o

# LOCALIZATION NOTE (webconsole.menu.selectAll.label)
# Label used for a context-menu item that will select all the content of the webconsole
# output.
webconsole.menu.selectAll.label=Seliger toto
webconsole.menu.selectAll.accesskey=t

# LOCALIZATION NOTE (webconsole.menu.openInSidebar.label)
# Label used for a context-menu item displayed for object/variable logs. Clicking on it
# opens the webconsole sidebar for the logged variable.
webconsole.menu.openInSidebar.label=Aperir in la barra lateral
webconsole.menu.openInSidebar.accesskey=V

# LOCALIZATION NOTE (webconsole.clearButton.tooltip)
# Label used for the tooltip on the clear logs button in the console top toolbar bar.
# Clicking on it will clear the content of the console.
webconsole.clearButton.tooltip=Vacuar le resultatos del consoweb

# LOCALIZATION NOTE (webconsole.toggleFilterButton.tooltip)
# Label used for the tooltip on the toggle filter bar button in the console top
# toolbar bar. Clicking on it will toggle the visibility of an additional bar which
# contains filter buttons.
webconsole.toggleFilterButton.tooltip=Monstrar/celar le barra de filtros

# LOCALIZATION NOTE (webconsole.filterInput.placeholder)
# Label used for for the placeholder on the filter input, in the console top toolbar.
webconsole.filterInput.placeholder=Filtrar le resultatos

# LOCALIZATION NOTE (webconsole.errorsFilterButton.label)
# Label used as the text of the "Errors" button in the additional filter toolbar.
# It shows or hides error messages, either inserted in the page using
# console.error() or as a result of a javascript error..
webconsole.errorsFilterButton.label=Errores

# LOCALIZATION NOTE (webconsole.warningsFilterButton.label)
# Label used as the text of the "Warnings" button in the additional filter toolbar.
# It shows or hides warning messages, inserted in the page using console.warn().
webconsole.warningsFilterButton.label=Advertentias

# LOCALIZATION NOTE (webconsole.logsFilterButton.label)
# Label used as the text of the "Logs" button in the additional filter toolbar.
# It shows or hides log messages, inserted in the page using console.log().
webconsole.logsFilterButton.label=Registros

# LOCALIZATION NOTE (webconsole.infoFilterButton.label)
# Label used as the text of the "Info" button in the additional filter toolbar.
# It shows or hides info messages, inserted in the page using console.info().
webconsole.infoFilterButton.label=Informationes

# LOCALIZATION NOTE (webconsole.debugFilterButton.label)
# Label used as the text of the "Debug" button in the additional filter toolbar.
# It shows or hides debug messages, inserted in the page using console.debug().
webconsole.debugFilterButton.label=Depuration

# LOCALIZATION NOTE (webconsole.cssFilterButton.label)
# Label used as the text of the "CSS" button in the additional filter toolbar.
# It shows or hides CSS warning messages, inserted in the page by the browser
# when there are CSS errors in the page.
webconsole.cssFilterButton.label=CSS

# LOCALIZATION NOTE (webconsole.xhrFilterButton.label)
# Label used as the text of the "XHR" button in the additional filter toolbar.
# It shows or hides messages displayed when the page makes an XMLHttpRequest or
# a fetch call.
webconsole.xhrFilterButton.label=XHR

# LOCALIZATION NOTE (webconsole.requestsFilterButton.label)
# Label used as the text of the "Requests" button in the additional filter toolbar.
# It shows or hides messages displayed when the page makes a network call, for example
# when an image or a scripts is requested.
webconsole.requestsFilterButton.label=Requestas

# LOCALIZATION NOTE (webconsole.filteredMessages.label)
# Text of the "filtered messages" bar, shown when console messages are hidden
# because the user has set non-default filters in the filter bar.
# This is a semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# example: 345 items hidden by filters.
webconsole.filteredMessages.label=#1 elemento celate per le filtros;#1 elementos celate per le filtros

# Label used as the text of the "Reset filters" button in the "filtered messages" bar.
# It resets the default filters of the console to their original values.
webconsole.resetFiltersButton.label=Remontar le filtros

# LOCALIZATION NOTE (webconsole.enablePersistentLogs.label)
webconsole.enablePersistentLogs.label=Conservar le registros
# LOCALIZATION NOTE (webconsole.enablePersistentLogs.tooltip)
webconsole.enablePersistentLogs.tooltip=Si tu activa iste option le resultatos non essera vacuate cata vice que tu naviga a un nove pagina
