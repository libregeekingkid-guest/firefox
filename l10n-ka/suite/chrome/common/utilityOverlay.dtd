<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- these things need to move into utilityOverlay.xul -->
<!ENTITY  offlineGoOfflineCmd.label                 "ავტონომიური რეჟიმი">
<!ENTITY  offlineGoOfflineCmd.accesskey             "k">

<!-- LOCALIZATION NOTE : FILE This file contains the global menu items --> 

<!ENTITY fileMenu.label                   "ფაილი">
<!ENTITY fileMenu.accesskey               "F">
<!ENTITY newMenu.label                    "ახალი">
<!ENTITY newMenu.accesskey                "N">
<!ENTITY newBlankPageCmd.label            "რედაქტორის გვერდი">
<!ENTITY newBlankPageCmd.accesskey        "P">
<!ENTITY newBlankPageCmd.key              "n">
<!ENTITY newPageFromTemplateCmd.label "შაბლონური გვერდი">
<!ENTITY newPageFromTemplateCmd.accesskey "t">
<!ENTITY newPageFromDraftCmd.label "გვერდის ესკიზი">
<!ENTITY newPageFromDraftCmd.accesskey "ე">
<!ENTITY newNavigatorCmd.label "ბრაუზერის სარკმელი">
<!ENTITY newNavigatorCmd.key "N">
<!ENTITY newNavigatorCmd.accesskey "B">
<!ENTITY newPrivateWindowCmd.label "პირადი სარკმელი">
<!ENTITY newPrivateWindowCmd.key "B">
<!ENTITY newPrivateWindowCmd.accesskey "W">
<!ENTITY printSetupCmd.label "გვერდის პარამეტრები…">
<!ENTITY printSetupCmd.accesskey "u">
<!ENTITY printPreviewCmd.label "ბეჭდვის ესკიზი">
<!ENTITY printPreviewCmd.accesskey "v">
<!ENTITY printCmd.label "ამობეჭდვა…">
<!ENTITY printCmd.accesskey "P">
<!ENTITY printCmd.key "P">

<!ENTITY editMenu.label                   "რედაქცია">
<!ENTITY editMenu.accesskey               "E">
<!ENTITY undoCmd.label                    "დაბრუნება">
<!ENTITY undoCmd.key                      "Z">
<!ENTITY undoCmd.accesskey                "U">
<!ENTITY redoCmd.label                    "გამეორება">
<!ENTITY redoCmd.key                      "Y">
<!ENTITY redoCmd.accesskey                "R">
<!ENTITY cutCmd.label                     "ამოჭრა">
<!ENTITY cutCmd.key                       "X">
<!ENTITY cutCmd.accesskey                 "t">
<!ENTITY copyCmd.label                    "კოპირება">
<!ENTITY copyCmd.key                      "C">
<!ENTITY copyCmd.accesskey                "c">
<!ENTITY pasteCmd.label                   "ჩასმა">
<!ENTITY pasteCmd.key                     "V">
<!ENTITY pasteCmd.accesskey               "P">
<!ENTITY pasteGoCmd.label                 "ჩასმა და გადასვლა">
<!ENTITY pasteGoCmd.accesskey             "G">

<!-- LOCALIZATION NOTE (pasteSearchCmd): "Search" is a verb, this is the
     search bar equivalent to the url bar's "Paste & Go"  -->
<!ENTITY pasteSearchCmd.label             "ჩასმა და ძიება">
<!ENTITY pasteSearchCmd.accesskey         "e">
<!ENTITY deleteCmd.label                  "წაშლა">
<!ENTITY deleteCmd.accesskey              "D">
<!ENTITY selectAllCmd.label               "ყველას მონიშვნა">
<!ENTITY selectAllCmd.key                 "A">
<!ENTITY selectAllCmd.accesskey           "A">
<!ENTITY clearHistoryCmd.label				"ძიების ისტორიის გასუფთავება">  
<!ENTITY clearHistoryCmd.accesskey			"H"> 
<!ENTITY showSuggestionsCmd.label			"შემოთავაზებათა ჩვენება">  
<!ENTITY showSuggestionsCmd.accesskey		"S"> 
<!ENTITY preferencesCmd.label				"პარამეტრები…">
<!ENTITY preferencesCmd.key					"E">  
<!ENTITY preferencesCmd.accesskey			"e"> 
<!ENTITY findBarCmd.key "F">
<!-- LOCALIZATION NOTE (findBarCmd.accesskey): This accesskey should be within
     findBarCmd.label found in editorOverlay.dtd, findCmd.label in messenger.dtd
     and messengercompose.dtd and findOnCmd.label found in navigatorOverlay.dtd -->
<!ENTITY findBarCmd.accesskey "მ">
<!ENTITY findReplaceCmd.accesskey "ა">
<!ENTITY findAgainCmd.label "შემდეგის პოვნა">
<!ENTITY findAgainCmd.key "G">
<!ENTITY findAgainCmd.accesskey "g">
<!ENTITY findAgainCmd.key2 "VK_F3">
<!ENTITY findPrevCmd.label "წინას პოვნა">
<!ENTITY findPrevCmd.key "G">
<!ENTITY findPrevCmd.key2 "VK_F3">
<!ENTITY findPrevCmd.accesskey "v">
<!ENTITY findTypeTextCmd.label "ტექსტის ძებნა ტექსტის შეტანისას">
<!ENTITY findTypeTextCmd.accesskey "x">
<!ENTITY findTypeLinksCmd.label "ბმების ძებნა ტექსტის შეტანისას">
<!ENTITY findTypeLinksCmd.accesskey "k">

<!ENTITY viewMenu.label					"ჩვენება"> 
<!ENTITY viewMenu.accesskey               "v">
<!ENTITY viewToolbarsMenu.label				"ჩვენება/დამალვა"> 
<!ENTITY viewToolbarsMenu.accesskey			"w"> 
<!ENTITY showTaskbarCmd.label					"სტატუსის პანელი">  
<!ENTITY showTaskbarCmd.accesskey				"S"> 

<!ENTITY helpMenu.label					"დახმარება"> 
<!ENTITY helpMenu.accesskey				"h"> 

<!ENTITY helpTroubleshootingInfo.label      "თექმომსახურების ინფორმაცია">
<!ENTITY helpTroubleshootingInfo.accesskey  "T">
<!ENTITY releaseCmd.label                   "ვერსიის დოკუმენტაცია">
<!ENTITY releaseCmd.accesskey               "N">
<!ENTITY helpSafeMode.label                 "ხელახლა გახსნა ამორთული მოდულებით">
<!ENTITY helpSafeMode.accesskey             "R">
<!ENTITY updateCmd.label                    "შემოწმება განახლებებზე…">
<!ENTITY updateCmd.accesskey                "C">
<!ENTITY aboutCmd.label					"&brandShortName; პროგრამის შესახებ">
<!ENTITY aboutCmd.accesskey				"A">
<!ENTITY aboutCommPluginsCmd.label			"მოდულების შესახებ">
<!ENTITY aboutCommPluginsCmd.accesskey			"p">

<!ENTITY direct.label                                   "ხაზზე (პროქსი: არა)">
<!ENTITY direct.accesskey                               "N">
<!ENTITY manual.label                                   "ხაზზე (პროქსი: მითითება)">
<!ENTITY manual.accesskey                               "M">
<!ENTITY pac.label                                      "ხაზზე (პროქსი: ავტო URL)">
<!ENTITY pac.accesskey                                  "A">
<!ENTITY wpad.label                                     "ხაზზე (პროქსი: ავტოაღმოჩენა)">
<!ENTITY wpad.accesskey                                 "D">
<!ENTITY system.label                                   "ხაზზე (პროქსი: სისტემური პროქსი)">
<!ENTITY system.accesskey                               "S">

<!ENTITY proxy.label                                    "პროქსის გამართვა…">
<!ENTITY proxy.accesskey                                "C">

<!ENTITY bidiSwitchTextDirectionItem.label        "ტექსტის მიმართულების გადართვა">
<!ENTITY bidiSwitchTextDirectionItem.accesskey    "w">
<!ENTITY bidiSwitchTextDirectionItem.commandkey   "X">

<!ENTITY customizeToolbarContext.label            "მორგება…">
<!ENTITY customizeToolbarContext.accesskey        "C">

<!ENTITY customizeToolbar.toolbarmode.label       "პარამეტრები ამ პულტისთვის">
<!ENTITY customizeToolbar.toolbarmode.accesskey   "e">
<!ENTITY customizeToolbar.iconsAndText.label      "პიქტოგრამები და ტექსტი">
<!ENTITY customizeToolbar.iconsAndText.accesskey  "a">
<!ENTITY customizeToolbar.icons.label             "პიქტოგრამები">
<!ENTITY customizeToolbar.icons.accesskey         "o">
<!ENTITY customizeToolbar.text.label              "ტექსტი">
<!ENTITY customizeToolbar.text.accesskey          "T">
<!ENTITY customizeToolbar.useSmallIcons.label     "პატარა პიქტოგრამები">
<!ENTITY customizeToolbar.useSmallIcons.accesskey "s">
<!ENTITY customizeToolbar.labelAlignEnd.label     "ტესტის ჩვენება პიქტოგრამებთან ერთად">
<!ENTITY customizeToolbar.labelAlignEnd.accesskey "b">
<!ENTITY customizeToolbar.useDefault.label        "ნაგულისხმები პარამეტრები">
<!ENTITY customizeToolbar.useDefault.accesskey    "U">

<!-- Popup Blocked notification menu -->
<!ENTITY allowPopups.accesskey        "p">
<!ENTITY showPopupManager.label       "ფირნიშების მართვა">
<!ENTITY showPopupManager.accesskey   "M">
<!ENTITY dontShowMessage.label        "ამ შეტყობინების დამალვა თუ ფირნიშები დაბლოკილია">
<!ENTITY dontShowMessage.accesskey    "D">

<!ENTITY syncToolbarButton.label "სინქრონიზაცია">
