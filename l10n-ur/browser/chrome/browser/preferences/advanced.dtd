<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Note: each tab panel must contain unique accesskeys -->

<!ENTITY generalTab.label                "عمومی">

<!ENTITY useCursorNavigation.label       "صفحات میں آگے پیچھے جانے کے لیے ہمیشہ کرسر کلیدیں استعمال کریں">
<!ENTITY useCursorNavigation.accesskey   "ک">
<!ENTITY searchOnStartTyping.label       "میرے ٹائپ کرنے پر متن کے لیے تلاش شروع کریں">
<!ENTITY searchOnStartTyping.accesskey   "م">
<!ENTITY useOnScreenKeyboard.label       "ضرورت کے وقت ٹچ کی بورڈ دکھایں">
<!ENTITY useOnScreenKeyboard.accesskey   "ک">

<!ENTITY browsing.label                  "براؤزنگ">

<!ENTITY useAutoScroll.label             "خودکار طومار استعمال کریں">
<!ENTITY useAutoScroll.accesskey         "خ">
<!ENTITY useSmoothScrolling.label        "ہموار طومار استعمال کریں">
<!ENTITY useSmoothScrolling.accesskey    "ہ">
<!ENTITY checkUserSpelling.label         "ٹائپ کرتے وقت اپنی املا کی پڑتال کریں">
<!ENTITY checkUserSpelling.accesskey     "ٹ">

<!ENTITY dataChoicesTab.label            "کوائف پسند">

<!-- LOCALIZATION NOTE (healthReportingDisabled.label): This message is displayed above
disabled data sharing options in developer builds or builds with no Telemetry support
available. -->

<!ENTITY enableHealthReport2.accesskey   "r">
<!ENTITY healthReportLearnMore.label     "مزید سیکھیں">

<!ENTITY dataCollectionPrivacyNotice.label    "رازداری کا نوٹس">

<!ENTITY alwaysSubmitCrashReports1.accesskey "c">
<!ENTITY crashReporterLearnMore.label    "مزید سیکھیں">

<!ENTITY networkTab.label                "نیٹ ورک">


<!ENTITY connectionDesc.label            "&brandShortName;  کا انٹرنیٹ سے جڑنے کا طریقہ تشکیل کریں">
<!ENTITY connectionSettings.label        "سیٹنگیں…">
<!ENTITY connectionSettings.accesskey    "س">

<!ENTITY httpCache.label                 "کیسہ بند ویب مواد">

<!ENTITY offlineStorage2.label           "آف لائن ویب مواد اور ویب کوائف">

<!--  Site Data section manages sites using Storage API and is under Network -->
<!ENTITY siteData.label                  "سائٹ کے کوائف">
<!ENTITY clearSiteData.label             "تمام کوائف صاف کریں">
<!ENTITY clearSiteData.accesskey         "ف">
<!ENTITY siteDataSettings.label          "سیٹنگیں…">
<!ENTITY siteDataSettings.accesskey      "ن">
<!ENTITY siteDataLearnMoreLink.label     "مزید سیکھیں">

<!-- LOCALIZATION NOTE:
  The entities limitCacheSizeBefore.label and limitCacheSizeAfter.label appear on a single
  line in preferences as follows:

  &limitCacheSizeBefore.label [textbox for cache size in MB] &limitCacheSizeAfter.label;
-->
<!ENTITY limitCacheSizeBefore.label      "کیسہ حد بند کریں">
<!ENTITY limitCacheSizeBefore.accesskey  "ح">
<!ENTITY limitCacheSizeAfter.label       "MB جگہ">
<!ENTITY clearCacheNow.label             "ابھی صاف کریں">
<!ENTITY clearCacheNow.accesskey         "ص">
<!ENTITY clearOfflineAppCacheNow.label   "ابھی صاف کریں">
<!ENTITY clearOfflineAppCacheNow.accesskey "ا">
<!ENTITY overrideSmartCacheSize.label    "خودکار کیسہ بندوبست زیر کریں">
<!ENTITY overrideSmartCacheSize.accesskey "ز">

<!ENTITY updateTab.label                 "تازہ کاری">

<!-- LOCALIZATION NOTE (updateApplication.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApplication.label         "&brandShortName; تازہ کاریاں:">
<!-- LOCALIZATION NOTE (updateApplication.version.*): updateApplication.version.pre
# is followed by a version number, keep the trailing space or replace it with a
# different character as needed. updateApplication.version.post is displayed
# after the version number, and is empty on purpose for English. You can use it
# if required by your language.
 -->
<!ENTITY updateApplication.version.pre   "ورژن">
<!ENTITY updateApplication.version.post  "">
<!ENTITY updateAuto3.accesskey           "A">
<!ENTITY updateCheckChoose2.accesskey    "C">
<!ENTITY updateManual2.accesskey         "N">

<!ENTITY updateHistory2.accesskey        "P">

<!ENTITY useService.label                "تازہ کاریاں تنصیب کرنے کے لیے پس منظر سروس استعمال کریں">
<!ENTITY useService.accesskey            "پ">

<!ENTITY enableSearchUpdate2.accesskey   "e">

<!ENTITY offlineStorageNotify.label               "مجھے بتائیں جب کوئی ویب سائٹ آف لائن استعمال کے لیے کوائف ذخیرہ کرنے کے لیے پوچھے">
<!ENTITY offlineStorageNotify.accesskey           "ب">
<!ENTITY offlineStorageNotifyExceptions.label     "استثنیات…">
<!ENTITY offlineStorageNotifyExceptions.accesskey "ا">

<!ENTITY offlineAppsList.height          "7em">
<!ENTITY offlineAppsListRemove.label     "ہٹائیں…">
<!ENTITY offlineAppsListRemove.accesskey "ہ">
<!ENTITY offlineAppRemove.confirm        "آف لائن کوائف ہٹائیں">

<!ENTITY certificateTab.label            "تصدیق نامے">
<!ENTITY selectCerts.auto                "خودبخود ایک منتخب کریں">
<!ENTITY selectCerts.auto.accesskey      "S">
<!ENTITY selectCerts.ask                 "مجھے ہر بار پوچھیں">
<!ENTITY selectCerts.ask.accesskey       "A">
<!ENTITY enableOCSP.label                "سوال OCSP ریسپانڈر سرور کی موجودہ سرٹیفکیٹ کی تصدیق کرتی ہے">
<!ENTITY enableOCSP.accesskey            "س">
<!ENTITY viewCerts2.accesskey            "C">
<!ENTITY viewSecurityDevices2.accesskey  "D">

<!ENTITY performance.label               "کارکردگی">
<!ENTITY useRecommendedPerformanceSettings2.accesskey
                                         "U">
<!ENTITY performanceSettingsLearnMore.label
                                         "مزید سیکھیں">
<!ENTITY limitContentProcessOption.accesskey   "L">
<!ENTITY allowHWAccel.label              "جب دستیاب ہو تو ہارڈ ویئر سرعت کاری استعمال کریں">
<!ENTITY allowHWAccel.accesskey          "ہ">
