name: firefox
version: @VERSION@-@BUILD_NUMBER@
summary: Mozilla Firefox web browser
description:  Firefox is a powerful, extensible web browser with support for modern web application technologies.
confinement: strict
grade: stable

apps:
  firefox:
    command: desktop-launch $SNAP/firefox
    desktop: distribution/firefox.desktop
    environment:
      DISABLE_WAYLAND: 1
      HOME: "$SNAP_USER_COMMON"
    plugs:
      - avahi-observe
      - browser-sandbox
      - camera
      - cups-control
      - desktop
      - desktop-legacy
      - gsettings
      - home
      - network
      - network-observe
      - opengl
      - pulseaudio
      - screen-inhibit-control
      - unity7
      - upower-observe
      - x11

plugs:
  browser-sandbox:
    interface: browser-support
    allow-sandbox: true

parts:
  firefox:
    plugin: dump
    source: source
    stage-packages:
      - libxt6
      - libdbus-glib-1-2
      - libasound2
      - libpulse0
      - libgl1-mesa-dri
      - libgl1-mesa-glx
      - libmirclient9
      - desktop-file-utils
      - xdg-utils
      - ffmpeg
    after: [desktop-gtk3]

  xdg-open:
    after: [firefox]
    plugin: nil
    source: .
    install: |
      set -eux
      mkdir -p $SNAPCRAFT_PART_INSTALL/usr/share/applications
      install -m 644 mimeapps.list $SNAPCRAFT_PART_INSTALL/usr/share/applications
      update-desktop-database -v $SNAPCRAFT_PART_INSTALL/usr/share/applications
    build-packages:
      - desktop-file-utils
    build-attributes: [no-system-libraries]

  shared-mime-info:
    after: [xdg-open]
    plugin: nil
    stage-packages:
      - shared-mime-info
    build-attributes: [no-system-libraries]
    install: |
      set -eux
      update-mime-database $SNAPCRAFT_PART_INSTALL/usr/share/mime
