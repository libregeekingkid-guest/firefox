# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

do-not-track-description = आपल्याला ट्रॅक न करण्यासाठी वेबसाइट्सना "ट्रॅक करू नका" हा इशारा पाठवा
do-not-track-learn-more = आणखी जाणा
do-not-track-option-default =
    .label = फक्त ट्रॅकिंग संरक्षण वापरत असताना
do-not-track-option-always =
    .label = नेहमी
pref-page =
    .title = { PLATFORM() ->
            [windows] पर्याय
           *[other] प्राधान्यक्रम
        }
# This is used to determine the width of the search field in about:preferences,
# in order to make the entire placeholder string visible
#
# Notice: The value of the `.style` attribute is a CSS string, and the `width`
# is the name of the CSS property. It is intended only to adjust the element's width.
# Do not translate.
search-input =
    .style = width: 15.4em
pane-general-title = सर्वसाधारण
category-general =
    .tooltiptext = { pane-general-title }
pane-search-title = शोधा
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = गोपनीयता आणि सुरक्षा
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = Firefox खाते
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = { -brand-short-name } मदत केंद्र
focus-search =
    .key = f
close-button =
    .aria-label = बंद करा

## Browser Restart Dialog

feature-enable-requires-restart = हे गुणविशेष सुरू करण्याकरिता { -brand-short-name }ला पुन्हा सुरू करा.
feature-disable-requires-restart = हे गुणविशेष बंद करण्याकरिता { -brand-short-name }ला पुन्हा सुरू करा.
should-restart-title = { -brand-short-name }ला पुन्हा सुरू करा
should-restart-ok = { -brand-short-name } लगेच पुनर्रारंभित करा
revert-no-restart-button = पूर्वस्थित करा
restart-later = नंतर पुनःसुरु करा
