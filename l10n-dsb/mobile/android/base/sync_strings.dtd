<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Ze &syncBrand.shortName.label; zwězaś'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Aby swój nowy rěd aktiwěrował, wubjeŕśo “&syncBrand.shortName.label; konfigurěrowaś” na rěźe.'>
<!ENTITY sync.subtitle.pair.label 'Za aktiwěrowanje wubjeŕśo “Rěd zwězaś” na wašom drugem rěźe.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Njamam rěd ze mnu…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Pśizjawjenja'>
<!ENTITY sync.configure.engines.title.history 'Historiju'>
<!ENTITY sync.configure.engines.title.tabs 'Rejtarki'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; na &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Meni cytańskich znamjenjow'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Wobznamjenja'>
<!ENTITY bookmarks.folder.toolbar.label 'Symbolowa rědka cytańskich znamjenjow'>
<!ENTITY bookmarks.folder.other.label 'Druge cytańske znamjenja'>
<!ENTITY bookmarks.folder.desktop.label 'Desktopowe cytańske znamjenja'>
<!ENTITY bookmarks.folder.mobile.label 'Mobilne cytańske znamjenja'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Pśipěty'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Slědk k pśeglědowanjeju'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Witajśo do &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Pśizjawśo se, aby swóje rejtarki, cytańske znamjenja, gronidła a druge synchronizěrował.'>
<!ENTITY fxaccount_getting_started_get_started 'Prědne kšace'>
<!ENTITY fxaccount_getting_started_old_firefox 'Staršu wersiju &syncBrand.shortName.label; wužywaś?'>

<!ENTITY fxaccount_status_auth_server 'Kontowy serwer'>
<!ENTITY fxaccount_status_sync_now 'Něnto synchronizěrowaś'>
<!ENTITY fxaccount_status_syncing2 'Synchronizěrujo se…'>
<!ENTITY fxaccount_status_device_name 'Rědowe mě'>
<!ENTITY fxaccount_status_sync_server 'Synchronizaciski serwer'>
<!ENTITY fxaccount_status_needs_verification2 'Wašo konto musy se pśeglědaś. Potusniśo, aby wobkšuśeńsku mailku znowego pósłał.'>
<!ENTITY fxaccount_status_needs_credentials 'Zwisk njejo móžny. Potusniśo, aby se pśizjawił.'>
<!ENTITY fxaccount_status_needs_upgrade 'Musyśo &brandShortName; aktualizěrowaś, aby se pśizjawił.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; jo konfigurěrowany, ale njesynchronizěrujo awtomatiski. Zmóžniśo “Daty awtomatiski synchronizěrowaś” w Androidowych nastajenjach &gt; Wužywanje datow.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; jo konfigurěrowany, ale njesynchronizěrujo awtomatiski. Zmóžniśo “Daty awtomatiski synchronizěrowaś” w meniju Androidowe nastajenja a konta.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Pótusniśo, aby se pla swójogo nowego konta Firefox pśizjawił.'>
<!ENTITY fxaccount_status_choose_what 'Wubjeŕśo, což ma se synchronizěrowaś'>
<!ENTITY fxaccount_status_bookmarks 'Cytańske znamjenja'>
<!ENTITY fxaccount_status_history 'Historija'>
<!ENTITY fxaccount_status_passwords2 'Pśizjawjenja'>
<!ENTITY fxaccount_status_tabs 'Wócynjone rejtarki'>
<!ENTITY fxaccount_status_additional_settings 'Pśidatne nastajenja'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Jano pśez Wi-Fi synchronizěrowaś'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 '&brandShortName; pśi synchronizěrowanju pśez mobilny telefon abo měrjonu seś zajźowaś'>
<!ENTITY fxaccount_status_legal 'Legalny' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Słužbne wuměnjenja'>
<!ENTITY fxaccount_status_linkprivacy2 'Powěźeńka priwatnosći'>
<!ENTITY fxaccount_remove_account 'Zwisk źěliś&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Zwisk ze Sync źěliś?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Waše pśeglědowańske daty na toś tom rěźe wóstanu, ale njebuźo wěcej z wašym kontom synchronizěrowaś.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 '&formatS; konta Firefox źělony.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Zwisk źěliś'>

<!ENTITY fxaccount_enable_debug_mode 'Modus pytanja zmólkow zmóžniś'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; - Nastajenja'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; konfigurěrowaś'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; njejo zwězany'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Potusniśo, aby se ako &formatS; pśizjawił'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Aktualizěrowanje &syncBrand.shortName.label; skóńcyś?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Potusniśo, aby se ako &formatS; pśizjawił'>
