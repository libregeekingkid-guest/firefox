<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  trackingProtectionHeader2.label      "محافظت در برابر ردگیری">
<!ENTITY  trackingProtection2.description      "دنبال کننده اطلاعات مرور شما از پایگاه‌های اینترنتی متعددی جمع آوری میکند. دنبال کننده همچین می‌تواند از این اطلاعات برای ایجاد یک نمایه از شما استفاده کند و از اطلاعات شخصی شما جهت نمایش محتوا مرتبط استفاده کند.">
<!ENTITY  trackingProtection2.radioGroupLabel  "استفاده از محافظ دنبال کردن برای مسدود کردن دنبال کنندگان ناشناس">
<!ENTITY  trackingProtection3.description      "محافظت از ردیابی دنبال کنندگان انلاینی را که اطلاعات مرورکردن شما را از چند وب سایت مختلف دریافت می کردند مسدود کرده است.">
<!ENTITY  trackingProtection3.radioGroupLabel  "استفاده از محافظ ردیابی برای مسدود کردند دنبال کنندگان ناشناس">
<!ENTITY  trackingProtectionAlways.label       "همیشه">
<!ENTITY  trackingProtectionAlways.accesskey   "م">
<!ENTITY  trackingProtectionPrivate.label      "تنها در پنجره‌های ناشناس">
<!ENTITY  trackingProtectionPrivate.accesskey  "ت">
<!ENTITY  trackingProtectionNever.label        "هرگز">
<!ENTITY  trackingProtectionNever.accesskey    "ه">
<!ENTITY  trackingProtectionLearnMore.label    "اطلاعات بیشتر">
<!ENTITY  trackingProtectionLearnMore2.label    "در مورد محافظت از ردیابی و حریم شخصی خود بیشتر بدانید">
<!ENTITY  trackingProtectionExceptions.label   "استثناها…">
<!ENTITY  trackingProtectionExceptions.accesskey "ت">

<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the release and beta channel. -->
<!ENTITY trackingProtectionPBM6.label         "استفاده از محافظ دنبال کننده در مرورگر خصوصی جهت مسدود کردن دنبال کننده های ناشناس">
<!ENTITY trackingProtectionPBM6.accesskey     "خ">
<!ENTITY trackingProtectionPBMLearnMore.label "اطلاعات بیشتر">
<!ENTITY changeBlockList2.label               "تغییر فهرست مسدودی‌ها… ">
<!ENTITY changeBlockList2.accesskey           "ت">

<!ENTITY  doNotTrack.description        "ارسال یک سیگنال “من را دنبال نکن ” برای پایگاه‌های اینترنتی که شما نمی‌خواهید توسط آن ها دنبال شوید">
<!ENTITY  doNotTrack.learnMore.label    "اطلاعات بیشتر">
<!ENTITY  doNotTrack.default.label      "تنها زمانی که از محافظ دنبال کردن استفاده ‌می‌شود">
<!ENTITY  doNotTrack.always.label       "همیشه">

<!ENTITY  history.label                 "تاریخچه">
<!ENTITY  permissions.label             "مجوزها">

<!ENTITY  addressBar.label              "نوار نشانی">
<!ENTITY  addressBar.suggest.label      "هنگام استفاده از نوار مکان، پیشنهاد بده">
<!ENTITY  locbar.history2.label         "تاریخچه‌ی مرورگر">
<!ENTITY  locbar.history2.accesskey     "م">
<!ENTITY  locbar.bookmarks.label        "نشانک‌ها">
<!ENTITY  locbar.bookmarks.accesskey    "ن">
<!ENTITY  locbar.openpage.label         "زبانه‌های باز">
<!ENTITY  locbar.openpage.accesskey     "ز">
<!ENTITY  locbar.searches.label         "جست‌وجو‌های مرتبط از موتور جست‌وجوی پیش‌فرض">
<!ENTITY  locbar.searches.accesskey     "م">

<!ENTITY  suggestionSettings2.label     "تغییر ترجیحات مربوط به پیشنهادهای موتورهای جست‌وجو">

<!ENTITY  acceptCookies2.label          "اجازه استفاده از کوکی‌ها توسط پایگاه‌های اینترنتی">
<!ENTITY  acceptCookies2.accesskey      "ک">

<!ENTITY  acceptThirdParty2.pre.label     "پذیرفتن کوکی‌های شخص ثالث">
<!ENTITY  acceptThirdParty2.pre.accesskey "ث">
<!ENTITY  acceptThirdParty.always.label   "همیشه">
<!ENTITY  acceptThirdParty.never.label    "هرگز">
<!ENTITY  acceptThirdParty.visited.label  "از بازدید">

<!ENTITY  keepUntil2.label              "نگهداری شوند تا">
<!ENTITY  keepUntil2.accesskey          "ن">

<!ENTITY  expire.label                  "منقضی شوند">
<!ENTITY  close.label                   "&brandShortName; بسته شود">

<!ENTITY  cookieExceptions.label        "استثناها…">
<!ENTITY  cookieExceptions.accesskey    "ت">

<!ENTITY  showCookies.label             "نمایش کوکی‌ها…">
<!ENTITY  showCookies.accesskey         "ک">

<!ENTITY  historyHeader2.pre.label         "&brandShortName; will">
<!ENTITY  historyHeader2.pre.accesskey     "w">
<!ENTITY  historyHeader.remember.label     "تاریخچه را به خاطر خواهد داشت">
<!ENTITY  historyHeader.dontremember.label "هرگز تاریخچه را به خاطر نمی‌سپارد">
<!ENTITY  historyHeader.custom.label       "تنظیمات خاصی را برای تاریخچه استفاده می‌کند">
<!ENTITY  historyHeader.post.label         "">

<!ENTITY  rememberDescription.label      "&brandShortName; تاریخچهٔ مرور، بارگیری، فرم‌ها و جست‌وجوهای شما را به خاطر خواهد داشت، و کوکی‌های پایگاه‌هایی که بازدید می‌کنید را خواهد پذیرفت.">

<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->
<!ENTITY  rememberActions.pre.label           "ممکن است بخواهید ">
<!ENTITY  rememberActions.clearHistory.label  "تاریخچهٔ اخیر خود را پاک کنید">
<!ENTITY  rememberActions.middle.label        "، یا ">
<!ENTITY  rememberActions.removeCookies.label "برخی کوکی‌ها را حذف کنید">
<!ENTITY  rememberActions.post.label          ".">

<!ENTITY  dontrememberDescription.label  "&brandShortName; تنظیمات حالت مرور ناشناس را استفاده خواهد کرد، و هیچ تاریخچه‌ای از مرور شما در وب نگه نخواهد داشت.">

<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->
<!ENTITY  dontrememberActions.pre.label          "ممکن است همچنین بخواهید ">
<!ENTITY  dontrememberActions.clearHistory.label "تمام تاریخچهٔ موجود را پاک کنید">
<!ENTITY  dontrememberActions.post.label         ".">

<!ENTITY  privateBrowsingPermanent2.label "همیشه از حالت  مرور خصوصی استفاده کن">
<!ENTITY  privateBrowsingPermanent2.accesskey "م">

<!ENTITY  rememberHistory2.label      "مرور و بارگذاری های مرا به خاطر بسپار">
<!ENTITY  rememberHistory2.accesskey  "خ">

<!ENTITY  rememberSearchForm.label       "اطلاعاتی که در فرم‌های صفحات وب و نوار جست‌وجو وارد می‌شوند به خاطر سپرده شود">
<!ENTITY  rememberSearchForm.accesskey   "ط">

<!ENTITY  clearOnClose.label             "تاریخچه همیشه هنگام بستن &brandShortName; پاک شود">
<!ENTITY  clearOnClose.accesskey         "ه">

<!ENTITY  clearOnCloseSettings.label     "تنظیمات">
<!ENTITY  clearOnCloseSettings.accesskey "ت">

<!ENTITY  browserContainersLearnMore.label      "اطلاعات بیشتر">
<!ENTITY  browserContainersEnabled.label        "فعال‌سازی زبانه‌های حامل">
<!ENTITY  browserContainersEnabled.accesskey    "ع">
<!ENTITY  browserContainersSettings.label        "تنظیمات…">
<!ENTITY  browserContainersSettings.accesskey    "ت">

<!ENTITY  a11yPrivacy.checkbox.label     "جلوگیری از دسترسی به سرویس‌ها از طریق مرورگر شما">
<!ENTITY  a11yPrivacy.checkbox.accesskey "a">
<!ENTITY  a11yPrivacy.learnmore.label    "بیشتر بدانید">
<!ENTITY enableSafeBrowsingLearnMore.label "بیشتر بدانید">
