buildDir "${topobjdir}/gradle/build/mobile/android/geckoview"

apply plugin: 'com.android.library'

// This converts MOZ_APP_VERSION into an integer
// version code.
//
// We take something like 58.1.2a1 and come out with 5800102
// This gives us 3 digits for the major number, and 2 digits
// each for the minor and build number. Beta and Release
def computeVersionCode() {
    String appVersion = mozconfig.substs.MOZ_APP_VERSION

    // Split on the dot delimiter, e.g. 58.1.1a1 -> ["58, "1", "1a1"]
    String[] parts = appVersion.split('\\.')

    assert parts.size() == 2 || parts.size() == 3

    // Strip out any milestone string ('a1' in the above example)
    // Present in Nightly (or local) builds only.
    parts.eachWithIndex { ver, index ->
        parts[index] = ver.replaceFirst(/[a-zA-Z].*/, "")
    }

    // Major
    int code = Integer.parseInt(parts[0]) * 100000

    // Minor
    code += Integer.parseInt(parts[1]) * 100

    // Build
    if (parts.size() == 3) {
        code += Integer.parseInt(parts[2])
    }

    return code;
}

android {
    compileSdkVersion project.ext.compileSdkVersion
    buildToolsVersion project.ext.buildToolsVersion

    defaultConfig {
        defaultPublishConfig 'release'
        publishNonDefault true

        targetSdkVersion project.ext.targetSdkVersion
        minSdkVersion project.ext.minSdkVersion
        manifestPlaceholders = project.ext.manifestPlaceholders

        versionCode computeVersionCode()
        versionName "${mozconfig.substs.MOZ_APP_VERSION}-${mozconfig.substs.MOZ_UPDATE_CHANNEL}"
        consumerProguardFiles 'proguard-rules.txt'

        // TODO: ensure these fields always agree with mobile/android/geckoview/BuildConfig.java.in,
        // either by diffing the processed files or by generating the output from a single source.
        buildConfigField 'String', "GRE_MILESTONE", "\"${mozconfig.substs.GRE_MILESTONE}\""
        // This should really come from the included binaries, but that's not easy.
        buildConfigField 'String', "MOZ_APP_ABI", mozconfig.substs['COMPILE_ENVIRONMENT'] ? "\"${ mozconfig.substs.TARGET_XPCOM_ABI}\"" : '"arm-eabi-gcc3"';
        buildConfigField 'String', "MOZ_APP_BASENAME", "\"${mozconfig.substs.MOZ_APP_BASENAME}\"";

        // For the benefit of future archaeologists:
        // GRE_BUILDID is exactly the same as MOZ_APP_BUILDID unless you're running
        // on XULRunner, which is never the case on Android.
        // 
        // Mimic Python: open(os.path.join(buildconfig.topobjdir, 'buildid.h')).readline().split()[2]
        buildConfigField 'String', "MOZ_APP_BUILDID", "\"${file("${topobjdir}/buildid.h").getText('utf-8').split()[2]}\"";
        buildConfigField 'String', "MOZ_APP_ID", "\"${mozconfig.substs.MOZ_APP_ID}\"";
        buildConfigField 'String', "MOZ_APP_NAME", "\"${mozconfig.substs.MOZ_APP_NAME}\"";
        buildConfigField 'String', "MOZ_APP_VENDOR", "\"${mozconfig.substs.MOZ_APP_VENDOR}\"";
        buildConfigField 'String', "MOZ_APP_VERSION", "\"${mozconfig.substs.MOZ_APP_VERSION}\"";
        buildConfigField 'String', "MOZ_APP_DISPLAYNAME", "\"${mozconfig.substs.MOZ_APP_DISPLAYNAME}\"";
        buildConfigField 'String', "MOZ_APP_UA_NAME", "\"${mozconfig.substs.MOZ_APP_UA_NAME}\"";
        buildConfigField 'String', "MOZ_UPDATE_CHANNEL", "\"${mozconfig.substs.MOZ_UPDATE_CHANNEL}\"";

        // MOZILLA_VERSION is oddly quoted from autoconf, but we don't have to handle it specially in Gradle.
        buildConfigField 'String', "MOZILLA_VERSION", "\"${mozconfig.substs.MOZILLA_VERSION}\"";
        buildConfigField 'String', "OMNIJAR_NAME", "\"${mozconfig.substs.OMNIJAR_NAME}\"";

        buildConfigField 'String', "USER_AGENT_GECKOVIEW_MOBILE", "\"Mozilla/5.0 (Android \" + android.os.Build.VERSION.RELEASE + \"; Mobile; rv: ${mozconfig.substs.MOZ_APP_VERSION}) Gecko/${mozconfig.substs.MOZ_APP_VERSION} GeckoView/${mozconfig.substs.MOZ_APP_VERSION}\"";
        buildConfigField 'String', "USER_AGENT_GECKOVIEW_TABLET", "\"Mozilla/5.0 (Android \" + android.os.Build.VERSION.RELEASE + \"; Tablet; rv: ${mozconfig.substs.MOZ_APP_VERSION}) Gecko/${mozconfig.substs.MOZ_APP_VERSION} GeckoView/${mozconfig.substs.MOZ_APP_VERSION}\"";

        buildConfigField 'String', "ANDROID_CPU_ARCH", "\"${mozconfig.substs.ANDROID_CPU_ARCH}\"";

        buildConfigField 'int', 'MIN_SDK_VERSION', mozconfig.substs.MOZ_ANDROID_MIN_SDK_VERSION;

        // Is the underlying compiled C/C++ code compiled with --enable-debug?
        buildConfigField 'boolean', 'DEBUG_BUILD', mozconfig.substs.MOZ_DEBUG ? 'true' : 'false';

        // See this wiki page for more details about channel specific build defines:
        // https://wiki.mozilla.org/Platform/Channel-specific_build_defines
        // This makes no sense for GeckoView and should be removed as soon as possible.
        buildConfigField 'boolean', 'RELEASE_OR_BETA', mozconfig.substs.RELEASE_OR_BETA ? 'true' : 'false';
        // This makes no sense for GeckoView and should be removed as soon as possible.
        buildConfigField 'boolean', 'NIGHTLY_BUILD', mozconfig.substs.NIGHTLY_BUILD ? 'true' : 'false';
        // This makes no sense for GeckoView and should be removed as soon as possible.
        buildConfigField 'boolean', 'MOZ_CRASHREPORTER', mozconfig.substs.MOZ_CRASHREPORTER ? 'true' : 'false';

        // Official corresponds, roughly, to whether this build is performed on
        // Mozilla's continuous integration infrastructure. You should disable
        // developer-only functionality when this flag is set.
        // This makes no sense for GeckoView and should be removed as soon as possible.
        buildConfigField 'boolean', 'MOZILLA_OFFICIAL', mozconfig.substs.MOZILLA_OFFICIAL ? 'true' : 'false';
    }

    buildTypes {
        withGeckoBinaries {
            initWith release
        }
        withoutGeckoBinaries { // For clarity and consistency throughout the tree.
            initWith release
        }
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_7
        targetCompatibility JavaVersion.VERSION_1_7
    }

    dexOptions {
        javaMaxHeapSize "2g"
    }

    lintOptions {
        abortOnError false
    }

    sourceSets {
        main {
            java {
                srcDir "${topsrcdir}/mobile/android/geckoview/src/thirdparty/java"

                if (!mozconfig.substs.MOZ_ANDROID_HLS_SUPPORT ||
                    !mozconfig.substs.get('MOZ_BUILD_MOBILE_ANDROID_WITH_GRADLE')) {
                    exclude 'com/google/android/exoplayer2/**'
                    exclude 'org/mozilla/gecko/media/GeckoHlsAudioRenderer.java'
                    exclude 'org/mozilla/gecko/media/GeckoHlsPlayer.java'
                    exclude 'org/mozilla/gecko/media/GeckoHlsRendererBase.java'
                    exclude 'org/mozilla/gecko/media/GeckoHlsVideoRenderer.java'
                    exclude 'org/mozilla/gecko/media/Utils.java'
                }

                if (mozconfig.substs.MOZ_WEBRTC) {
                    srcDir "${topsrcdir}/media/webrtc/trunk/webrtc/base/java/src"
                    srcDir "${topsrcdir}/media/webrtc/trunk/webrtc/modules/audio_device/android/java/src"
                    srcDir "${topsrcdir}/media/webrtc/trunk/webrtc/modules/video_capture/android/java/src"
                }
            }

            assets {
            }
        }
    }
}

dependencies {
    compile "com.android.support:support-v4:${mozconfig.substs.ANDROID_SUPPORT_LIBRARY_VERSION}"
    compile "com.android.support:palette-v7:${mozconfig.substs.ANDROID_SUPPORT_LIBRARY_VERSION}"
}

apply from: "${topsrcdir}/mobile/android/gradle/with_gecko_binaries.gradle"

android.libraryVariants.all { variant ->
    // Like 'debug', 'release', or 'withGeckoBinaries'.
    def buildType = variant.buildType.name

    // It would be most natural for :geckoview to always include the Gecko
    // binaries, but that's difficult; see the notes in
    // mobile/android/gradle/with_gecko_binaries.gradle.  Instead :app uses
    // :geckoview:release and handles it's own Gecko binary inclusion.
    if (buildType.equals('withGeckoBinaries')) {
        configureVariantWithGeckoBinaries(variant)
    }

    // Javadoc and Sources JAR configuration cribbed from
    // https://github.com/mapbox/mapbox-gl-native/blob/d169ea55c1cfa85cd8bf19f94c5f023569f71810/platform/android/MapboxGLAndroidSDK/build.gradle#L85
    // informed by
    // https://code.tutsplus.com/tutorials/creating-and-publishing-an-android-library--cms-24582,
    // and amended from numerous Stackoverflow posts.
    def name = variant.name
    def javadoc = task "javadoc${name.capitalize()}"(type: Javadoc) {
        description = "Generate Javadoc for build variant $name"
        destinationDir = new File(destinationDir, variant.baseName)
        classpath = files(variant.javaCompile.classpath.files)

        source = files(variant.javaCompile.source)
        exclude '**/R.java', '**/BuildConfig.java', 'com/google/**', 'org/webrtc/**'
        options.addPathOption('sourcepath', ':').setValue(
            variant.sourceSets.collect({ it.javaDirectories }).flatten() +
            variant.generateBuildConfig.sourceOutputDir)

        // javadoc 8 has a bug that requires the rt.jar file from the JRE to be
        // in the bootclasspath (https://stackoverflow.com/a/30458820).
        options.bootClasspath = [
            file("${System.properties['java.home']}/lib/rt.jar")] + android.bootClasspath
        options.memberLevel = JavadocMemberLevel.PROTECTED
        options.source = 7

        options.docTitle = "GeckoView ${mozconfig.substs.MOZ_APP_VERSION} API"
        options.header = "GeckoView ${mozconfig.substs.MOZ_APP_VERSION} API"
        options.noTimestamp = true
        options.noIndex = true
        options.noQualifiers = ['java.lang']
        options.tags = ['hide:a:']
    }

    task "javadocJar${name.capitalize()}"(type: Jar, dependsOn: javadoc) {
        classifier = 'javadoc'
        from javadoc.destinationDir
    }

    task "sourcesJar${name.capitalize()}"(type: Jar) {
        classifier 'sources'
        description = "Generate Javadoc for build variant $name"
        destinationDir = new File(destinationDir, variant.baseName)
        from files(variant.javaCompile.source)
    }
}

android.libraryVariants.all { variant ->
    configureVariantWithJNIWrappers(variant, "Generated")
}

apply plugin: 'maven'
 
uploadArchives {
    repositories.mavenDeployer {
        pom.groupId = 'org.mozilla'
        pom.artifactId = "geckoview-${mozconfig.substs.MOZ_UPDATE_CHANNEL}-${mozconfig.substs.ANDROID_CPU_ARCH}"
        pom.version = mozconfig.substs.MOZ_APP_VERSION
        pom.project {
            licenses {
                license {
                    name 'The Mozilla Public License, v. 2.0'
                    url 'http://mozilla.org/MPL/2.0/'
                    distribution 'repo'
                }
            }
        }
        repository(url: "file://${project.buildDir}/maven")
    }
}

// This is all related to the withGeckoBinaries approach; see
// mobile/android/gradle/with_gecko_binaries.gradle.
afterEvaluate {
    // The bundle tasks are only present when the particular configuration is
    // being built, so this task might not exist.  (This is due to the way the
    // Android Gradle plugin defines things during configuration.)
    def bundleWithGeckoBinaries = tasks.findByName('bundleWithGeckoBinaries')
    if (!bundleWithGeckoBinaries) {
        return
    }

    // Remove default configuration, which is the release configuration, when
    // we're actually building withGeckoBinaries.  This makes `gradle install`
    // install the withGeckoBinaries artifacts, not the release artifacts (which
    // are withoutGeckoBinaries and not suitable for distribution.)
    def Configuration archivesConfig = project.getConfigurations().getByName('archives')
    archivesConfig.artifacts.removeAll { it.extension.equals('aar') }

    artifacts {
        // Instead of default (release) configuration, publish one with Gecko binaries.
        archives bundleWithGeckoBinaries
        // Javadoc and sources for developer ergononomics.
        archives javadocJarWithGeckoBinaries
        archives sourcesJarWithGeckoBinaries
    }
}

// Bug 1353055 - Strip 'vars' debugging information to agree with moz.build.
apply from: "${topsrcdir}/mobile/android/gradle/debug_level.gradle"
android.libraryVariants.all configureVariantDebugLevel
