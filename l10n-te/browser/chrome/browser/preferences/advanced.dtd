<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Note: each tab panel must contain unique accesskeys -->

<!ENTITY generalTab.label                "సాధారణం">

<!ENTITY useCursorNavigation.label       "పేజీల మధ్య సంచరణకు ఎల్లప్పుడు కర్సరు కీలను ఉపయోగించండి">
<!ENTITY useCursorNavigation.accesskey   "c">
<!ENTITY searchOnStartTyping.label       "మీరు టైపుచేయడం ప్రారంభించినప్పటి నుంచి పాఠ్యము‍ కొరకు శోధించు">
<!ENTITY searchOnStartTyping.accesskey   "x">
<!ENTITY useOnScreenKeyboard.label       "అవసరమైనప్పుడు స్పర్శా కీ బోర్డు చూపించు">
<!ENTITY useOnScreenKeyboard.accesskey   "k">

<!ENTITY browsing.label                  "విహారణ">

<!ENTITY useAutoScroll.label             "స్వయంచాలక స్క్రోలింగ్ వాడు">
<!ENTITY useAutoScroll.accesskey         "a">
<!ENTITY useSmoothScrolling.label        "సాఫీ స్క్రోలింగ్ వాడు">
<!ENTITY useSmoothScrolling.accesskey    "m">
<!ENTITY checkUserSpelling.label         "మీరు టైపు చేసినప్పుడు స్పెల్లింగ్ ను పరిశీలించు">
<!ENTITY checkUserSpelling.accesskey     "t">

<!ENTITY dataChoicesTab.label            "దత్తాంశం ఎంపికలు">

<!-- LOCALIZATION NOTE (healthReportingDisabled.label): This message is displayed above
disabled data sharing options in developer builds or builds with no Telemetry support
available. -->
<!ENTITY healthReportingDisabled.label   "ఈ బిల్డ్ కాన్ఫిగరేషన్ కోసం డేటా రిపోర్టింగ్ నిలిపివేయబడింది">

<!ENTITY enableHealthReport2.accesskey   "r">
<!ENTITY healthReportLearnMore.label     "ఇంకా తెలుసుకోండి">

<!ENTITY dataCollection.label            "&brandShortName; డేటా సేకరణ మరియు ఉపయోగం">
<!ENTITY dataCollectionPrivacyNotice.label    "గోప్యతా విధానం">

<!ENTITY alwaysSubmitCrashReports1.label  "&brandShortName; ను అనుమతించు Mozillaకు క్రాష్ నివేదికలను పంపటానికి">
<!ENTITY alwaysSubmitCrashReports1.accesskey "c">

<!ENTITY collectBrowserErrors.accesskey      "b">
<!ENTITY collectBrowserErrorsLearnMore.label "ఇంకా తెలుసుకోండి">

<!ENTITY sendBackloggedCrashReports.accesskey "c">
<!ENTITY crashReporterLearnMore.label    "ఇంకా తెలుసుకోండి">

<!ENTITY networkTab.label                "నెట్‌వర్క్">

<!ENTITY networkProxy.label              "నెట్‌వర్క్ ప్రాక్సీ">

<!ENTITY connectionDesc.label            "&brandShortName; ఎలా అంతర్జాలముకు అనుసంధానించబడుతోందో ఆకృతీకరించు">

<!ENTITY connectionSettingsLearnMore.label "ఇంకా తెలుసుకోండి">
<!ENTITY connectionSettings.label        "అమరికలు…">
<!ENTITY connectionSettings.accesskey    "e">

<!ENTITY httpCache.label                 "క్యాషేచేసిన వెబ్ విషయం">

<!--  Site Data section manages sites using Storage API and is under Network -->
<!ENTITY siteData.label                  "సైటు డేటా">
<!ENTITY clearSiteData.label             "మొత్తం డేటాను క్లియర్ చేయి">
<!ENTITY clearSiteData.accesskey         "l">
<!ENTITY clearSiteData1.label            "డేటాను తుజిచివేయి…">
<!ENTITY clearSiteData1.accesskey        "l">
<!ENTITY siteDataSettings.label          "అమరికలు…">
<!ENTITY siteDataSettings.accesskey      "i">
<!ENTITY siteDataLearnMoreLink.label     "మరింత తెలుసుకోండి">

<!-- LOCALIZATION NOTE:
  The entities limitCacheSizeBefore.label and limitCacheSizeAfter.label appear on a single
  line in preferences as follows:

  &limitCacheSizeBefore.label [textbox for cache size in MB] &limitCacheSizeAfter.label;
-->
<!ENTITY limitCacheSizeBefore.label      "క్యాచీను ఇంతకు పరిమితం చేయి">
<!ENTITY limitCacheSizeBefore.accesskey  "L">
<!ENTITY limitCacheSizeAfter.label       "MB జాగాకు">
<!ENTITY clearCacheNow.label             "ఇప్పుడు శుభ్రపరచు">
<!ENTITY clearCacheNow.accesskey         "C">
<!ENTITY overrideSmartCacheSize.label    "స్వయంచాలక క్యాషె నిర్వహణను వోవర్‌రైడ్ చేయి">
<!ENTITY overrideSmartCacheSize.accesskey "O">

<!ENTITY updateTab.label                 "నవీకరణ">

<!-- LOCALIZATION NOTE (updateApplication.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApplication.label         "&brandShortName; తాజాకరణలు">
<!-- LOCALIZATION NOTE (updateApplication.version.*): updateApplication.version.pre
# is followed by a version number, keep the trailing space or replace it with a
# different character as needed. updateApplication.version.post is displayed
# after the version number, and is empty on purpose for English. You can use it
# if required by your language.
 -->
<!ENTITY updateApplication.version.pre   "సంచిక ">
<!ENTITY updateApplication.version.post  "">
<!ENTITY updateApplication.description   "వీటికి &brandShortName;ని అనుమతించు">
<!ENTITY updateAuto3.label               "నవీకరణలను స్వయంచాలితంగా స్థాపించు (సిఫార్సు చేయబడినది)">
<!ENTITY updateAuto3.accesskey           "A">
<!ENTITY updateCheckChoose2.label        "తాజాకరణల కోసం చూస్తుంది కానీ స్థాపించుకోవాలో వద్దో మిమ్నల్ని ఎంచుకోనిస్తుంది">
<!ENTITY updateCheckChoose2.accesskey    "C">
<!ENTITY updateManual2.label             "తాజాకరణల కోసం ఎప్పుడూ చూడవద్దు (సిఫారసు చేయము)">
<!ENTITY updateManual2.accesskey         "N">

<!ENTITY updateHistory2.label            "తాజాకరణ చరిత్రను చూపించు…">
<!ENTITY updateHistory2.accesskey        "p">

<!ENTITY useService.label                "తాజాకరణలను స్థాపించడానికి బ్యాక్‌గ్రౌండ్ సేవను వాడు">
<!ENTITY useService.accesskey            "b">

<!ENTITY enableSearchUpdate2.label       "సెర్చింజన్లను స్వయంచాలకంగా తాజాకరించు">
<!ENTITY enableSearchUpdate2.accesskey   "e">

<!ENTITY certificateTab.label            "ధృవీకరణ పత్రాలు">
<!ENTITY certPersonal2.description       "సెర్వెర్ మీ వ్యక్తగిత ధృవీకరణపత్రం అభ్యర్ధించినప్పుడు">
<!ENTITY selectCerts.auto                "స్వయంచాలకంగా ఒక దానిని ఎంపికచేయి">
<!ENTITY selectCerts.auto.accesskey      "S">
<!ENTITY selectCerts.ask                 "ప్రతిసారీ మిమ్మల్ని అడుగును">
<!ENTITY selectCerts.ask.accesskey       "A">
<!ENTITY enableOCSP.label                "ధృవీకరణపత్రాల ప్రస్తుత ప్రమాణతను నిర్థారించుటకు OCSP రెస్పాండర్ సేవికలను ప్రశ్నిస్తుంది">
<!ENTITY enableOCSP.accesskey            "Q">
<!ENTITY viewCerts2.label                "ధృవీకరణ పత్రాలను దర్శించు…">
<!ENTITY viewCerts2.accesskey            "C">
<!ENTITY viewSecurityDevices2.label      "రక్షణ పరికరాలు…">
<!ENTITY viewSecurityDevices2.accesskey  "D">

<!ENTITY performance.label               "పనితనం">
<!ENTITY useRecommendedPerformanceSettings2.label
                                         "సిఫారసు చేయబడిన పనితనపు అమరికలను వాడండి">
<!ENTITY useRecommendedPerformanceSettings2.description
                                         "ఈ సెట్టింగులు మీ కంప్యూటర్ హార్డ్వేర్ మరియు ఆపరేటింగ్ సిస్టంకు అనుగుణంగా ఉంటాయి.">
<!ENTITY useRecommendedPerformanceSettings2.accesskey
                                         "U">
<!ENTITY performanceSettingsLearnMore.label
                                         "ఇంకా తెలుసుకోండి">
<!ENTITY limitContentProcessOption.label "కంటెంట్ ప్రాసెస్ పరిమితి">
<!ENTITY limitContentProcessOption.description
                                         "బహుళ ట్యాబ్లను ఉపయోగిస్తున్నప్పుడు అదనపు కంటెంట్ ప్రాసెస్లు పనితీరును మెరుగుపరుస్తాయి, అయితే మరింత మెమరీని కూడా ఉపయోగిస్తాయి.">
<!ENTITY limitContentProcessOption.accesskey   "L">
<!ENTITY allowHWAccel.label              "అందుబాటులో ఉన్నప్పుడు హార్డువేర్ యాక్సెలరేషన్ ఉపయోగించు">
<!ENTITY allowHWAccel.accesskey          "r">
