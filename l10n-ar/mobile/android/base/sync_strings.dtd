<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "تزامُن فَيَرفُكس">
<!ENTITY syncBrand.shortName.label "تزامُن">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'اتصّل ب‍&syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'لتفعيل جهازك الجديد اختر ”إعداد &syncBrand.shortName.label;“ على الجهاز.'>
<!ENTITY sync.subtitle.pair.label 'للتفعيل، اختر ”اربط جهازًا“ في جهازك الآخر.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'الجهاز ليس معي الآن…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'جلسات الولوج'>
<!ENTITY sync.configure.engines.title.history 'التأريخ'>
<!ENTITY sync.configure.engines.title.tabs 'الألسنة'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '‏&formatS1;على &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'قائمة العلامات'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'الوسوم'>
<!ENTITY bookmarks.folder.toolbar.label 'شريط العلامات'>
<!ENTITY bookmarks.folder.other.label 'العلامات الأخرى'>
<!ENTITY bookmarks.folder.desktop.label 'علامات سطح المكتب'>
<!ENTITY bookmarks.folder.mobile.label 'علامات الجوال'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'مثبَّت'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'عُد إلى التصفح'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'مرحبًا مع &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'لِج لمزامنة ألسنتك و علاماتك و جلسات ولوجك و غيرها المزيد.'>
<!ENTITY fxaccount_getting_started_get_started 'ابدأ'>
<!ENTITY fxaccount_getting_started_old_firefox 'أتستخدم إصدارة &syncBrand.shortName.label; قديمة؟'>

<!ENTITY fxaccount_status_auth_server 'خادوم الحساب'>
<!ENTITY fxaccount_status_sync_now 'زامِن الآن'>
<!ENTITY fxaccount_status_syncing2 'يُزامن…'>
<!ENTITY fxaccount_status_device_name 'اسم الجهاز'>
<!ENTITY fxaccount_status_sync_server 'خادوم التزامن'>
<!ENTITY fxaccount_status_needs_verification2 'يحتاج حسابك للتأكيد. انقر لإعادة إرسال بريد التأكيد.'>
<!ENTITY fxaccount_status_needs_credentials 'تعذّر الاتصال. انقر للولوج.'>
<!ENTITY fxaccount_status_needs_upgrade 'عليك ترقية &brandShortName; لتتمكّن من الولوج.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '‏&syncBrand.shortName.label; مُعدّ، لكن لا يُزامن تلقائيًا. انقر على ”مزامنة البيانات تلقائيًا“ في إعدادات أندرويد ← استخدام البيانات.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '‏&syncBrand.shortName.label; مُعدّ، لكن لا يُزامن تلقائيًا. انقر على ”مزامنة البيانات تلقائيًا“ في قائمة إعدادات أندرويد ← الحسابات.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'انقر للولوج إلى حساب فَيَرفُكس الجديد.'>
<!ENTITY fxaccount_status_choose_what 'اختر ما الذي تريد مزامنته'>
<!ENTITY fxaccount_status_bookmarks 'العلامات'>
<!ENTITY fxaccount_status_history 'التأريخ'>
<!ENTITY fxaccount_status_passwords2 'جلسات الولوج'>
<!ENTITY fxaccount_status_tabs 'الألسنة المفتوحة'>
<!ENTITY fxaccount_status_additional_settings 'إعدادات إضافية'>
<!ENTITY fxaccount_pref_sync_use_metered2 'زامن عبر واي فاي فقط'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'امنع &brandShortName; من المزامنة على شبكة محمول أو شبكة مقننة'>
<!ENTITY fxaccount_status_legal 'قانوني' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'بنود الخدمة'>
<!ENTITY fxaccount_status_linkprivacy2 'تنويه الخصوصية'>
<!ENTITY fxaccount_remove_account 'اقطع الاتصال&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'أأقطع الاتصال من المزامنة؟'>
<!ENTITY fxaccount_remove_account_dialog_message2 'بيانات تصفحك ستبقى على هذا الجهاز، لكن لن تُزامَن بعد الآن مع حسابك.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'قُطِع اتصال حساب فَيَرفُكس &formatS;.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'اقطع الاتصال'>

<!ENTITY fxaccount_enable_debug_mode 'فعّل وضع التنقيح'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'فَيَرفُكس'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'خيارات &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'اضبط &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '‏&syncBrand.shortName.label; غير متّصل'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'انقر للولوج باسم &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'أتريد إنهاء ترقية &syncBrand.shortName.label;؟'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'انقر للولوج باسم &formatS;'>
