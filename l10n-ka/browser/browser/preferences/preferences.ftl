# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

do-not-track-description = საიტებისთვის “არ მითვალთვალო” მოთხოვნის გაგზავნა
do-not-track-learn-more = შეიტყვეთ მეტი
do-not-track-option-default =
    .label = მხოლოდ, თვალთვალისგან დაცვის დროს
do-not-track-option-always =
    .label = ყოველთვის
pref-page =
    .title = { PLATFORM() ->
            [windows] პარამეტრები
           *[other] პარამეტრები
        }
# This is used to determine the width of the search field in about:preferences,
# in order to make the entire placeholder string visible
#
# Notice: The value of the `.style` attribute is a CSS string, and the `width`
# is the name of the CSS property. It is intended only to adjust the element's width.
# Do not translate.
search-input =
    .style = width: 15.4em
pane-general-title = მთავარი 
category-general =
    .tooltiptext = { pane-general-title }
pane-search-title = ძიება
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = პირადულობა და უსაფრთხოება
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = Firefox ანგარიში
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = { -brand-short-name } დახმარება
focus-search =
    .key = f
close-button =
    .aria-label = დახურვა

## Browser Restart Dialog

feature-enable-requires-restart = ამ ფუნქციის ჩასართავად, ხელახლა გაუშვით { -brand-short-name }
feature-disable-requires-restart = ამ ფუნქციის გამოსართავად, ხელახლა გაუშვით { -brand-short-name }
should-restart-title = { -brand-short-name }-ის ხელახლა გაშვება
should-restart-ok = { -brand-short-name } ხელახლა გაშვება ახლავე
revert-no-restart-button = გაუქმება
restart-later = მოგვიანებით გაშვება
