<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemFormatting.label             "Format">
<!ENTITY itemTags.label                   "Tag">
<!ENTITY itemAdvanced.label               "Lanjutan">

<!ENTITY style.label                      "Gaya:">
<!ENTITY style.accesskey                  "y">
<!ENTITY regularStyle.label               "Biasa">
<!ENTITY bold.label                       "Tebal">
<!ENTITY italic.label                     "Italik">
<!ENTITY boldItalic.label                 "Italik Tebal">
<!ENTITY size.label                       "Saiz:">
<!ENTITY size.accesskey                   "z">
<!ENTITY regularSize.label                "Biasa">
<!ENTITY bigger.label                     "Lebih besar">
<!ENTITY smaller.label                    "Lebih kecil">
<!ENTITY quotedTextColor.label            "Warna:">
<!ENTITY quotedTextColor.accesskey        "n">
<!ENTITY displayWidth.label               "Mesej Teks Biasa">
<!ENTITY displayText.label                "Apabila memaparkan petikan mesej teks biasa:">

<!-- LOCALIZATION NOTE : (emoticonsAndStructs.label) 'Emoticons' are also known as 'Smileys', e.g. :-)   -->
<!ENTITY convertEmoticons.label        "Papar ikon emotif sebagai grafik">
<!ENTITY convertEmoticons.accesskey    "e">

<!-- labels -->
<!ENTITY displayTagsText.label     "Tag boleh digunakan untuk mengelaskan dan mengutamakan mesej.">
<!ENTITY newTagButton.label        "Baru…">
<!ENTITY newTagButton.accesskey    "B">
<!ENTITY editTagButton1.label      "Edit…">
<!ENTITY editTagButton1.accesskey  "E">
<!ENTITY removeTagButton.label     "Buang">
<!ENTITY removeTagButton.accesskey "B">

<!-- Fonts and Colors -->
<!ENTITY fontsAndColors1.label   "Fon &amp; Warna">
<!ENTITY defaultFont.label       "Fon piawai:">
<!ENTITY defaultFont.accesskey   "F">
<!ENTITY defaultSize.label       "Saiz:">
<!ENTITY defaultSize.accesskey   "S">
<!ENTITY fontOptions.accesskey   "L">
<!ENTITY fontOptions.label       "Lanjutan…">
<!ENTITY colorButton.label       "Warna…">
<!ENTITY colorButton.accesskey   "W">

<!-- Advanced -->
<!ENTITY reading.caption                  "Membaca">
<!ENTITY display.caption                  "Paparan">
<!ENTITY showCondensedAddresses.label     "Papar nama paparan kenalan dalam buku alamat sahaja">
<!ENTITY showCondensedAddresses.accesskey "P">

<!ENTITY autoMarkAsRead.label             "Tandakan mesej secara automatik sebagai sudah dibaca">
<!ENTITY autoMarkAsRead.accesskey         "T">
<!ENTITY markAsReadNoDelay.label          "Serta-merta apabila dipaparkan">
<!ENTITY markAsReadNoDelay.accesskey      "r">
<!-- LOCALIZATION NOTE (markAsReadDelay.label): This will concatenate to
     "After displaying for [___] seconds",
     using (markAsReadDelay.label) and a number (secondsLabel.label). -->
<!ENTITY markAsReadDelay.label            "Selepas dipaparkan selama">
<!ENTITY markAsReadDelay.accesskey        "d">
<!ENTITY secondsLabel.label               "saat">
<!ENTITY openMsgIn.label                  "Buka mesej dalam:">
<!ENTITY openMsgInNewTab.label            "Tab baru">
<!ENTITY openMsgInNewTab.accesskey        "b">
<!ENTITY reuseExpRadio0.label             "Tetingkap mesej baru">
<!ENTITY reuseExpRadio0.accesskey         "n">
<!ENTITY reuseExpRadio1.label             "Tetingkap mesej sedia ada">
<!ENTITY reuseExpRadio1.accesskey         "e">
<!ENTITY closeMsgOnMoveOrDelete.label     "Tutup tetingkap/tab mesej apabila dipindahkan atau dibuang">
<!ENTITY closeMsgOnMoveOrDelete.accesskey "T">
