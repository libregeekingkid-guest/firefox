<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->


<!ENTITY  trackingProtectionAlways.label       "Always">
<!ENTITY  trackingProtectionAlways.accesskey   "y">
<!ENTITY  trackingProtectionPrivate.label      "Only in private windows">
<!ENTITY  trackingProtectionPrivate.accesskey  "l">
<!ENTITY  trackingProtectionNever.label        "Never">
<!ENTITY  trackingProtectionNever.accesskey    "N">
<!ENTITY  trackingProtectionLearnMore.label    "Learn more">
<!ENTITY  trackingProtectionExceptions.label   "Exceptions…">
<!ENTITY  trackingProtectionExceptions.accesskey "x">
<!-- LOCALIZATION NOTE (trackingProtectionPBM5.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to true. This currently happens on the release and beta channel. -->

<!ENTITY trackingProtectionPBM5.label         "Use Tracking Protection in Private Windows">
<!ENTITY trackingProtectionPBM5.accesskey     "v">
<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the nightly channel. -->

<!ENTITY trackingProtectionPBMLearnMore.label "Learn more">







<!ENTITY  history.label                 "History">





<!ENTITY  locbar.bookmarks.label        "Bookmarks">
<!ENTITY  locbar.bookmarks.accesskey    "k">
<!ENTITY  locbar.openpage.label         "Open tabs">
<!ENTITY  locbar.openpage.accesskey     "O">
<!ENTITY  locbar.searches.label         "Related searches from the default search engine">
<!ENTITY  locbar.searches.accesskey     "d">






<!ENTITY  acceptThirdParty.never.label    "Never">
<!ENTITY  acceptThirdParty.visited.label  "From visited">



<!ENTITY  expire.label                  "they expire">
<!ENTITY  close.label                   "I close &brandShortName;">

<!ENTITY  cookieExceptions.label        "Exceptions…">
<!ENTITY  cookieExceptions.accesskey    "E">

<!ENTITY  showCookies.label             "Show Cookies…">
<!ENTITY  showCookies.accesskey         "S">


<!ENTITY  historyHeader.remember.label     "Remember history">
<!ENTITY  historyHeader.dontremember.label "Never remember history">
<!ENTITY  historyHeader.custom.label       "Use custom settings for history">
<!ENTITY  historyHeader.post.label         "​">

<!ENTITY  rememberDescription.label      "&brandShortName; will remember your browsing, download, form and search history, and keep cookies from websites you visit.">
<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->

<!ENTITY  rememberActions.pre.label           "You may want to ">
<!ENTITY  rememberActions.clearHistory.label  "clear your recent history">
<!ENTITY  rememberActions.middle.label        ", or ">
<!ENTITY  rememberActions.removeCookies.label "remove individual cookies">
<!ENTITY  rememberActions.post.label          ".">

<!ENTITY  dontrememberDescription.label  "&brandShortName; will use the same settings as private browsing, and will not remember any history as you browse the Web.">
<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->

<!ENTITY  dontrememberActions.pre.label          "You may also want to ">
<!ENTITY  dontrememberActions.clearHistory.label "clear all current history">
<!ENTITY  dontrememberActions.post.label         ".">

<!ENTITY  privateBrowsingPermanent2.label "Always use private browsing mode">
<!ENTITY  privateBrowsingPermanent2.accesskey "p">

<!ENTITY  rememberHistory2.label      "Remember my browsing and download history">
<!ENTITY  rememberHistory2.accesskey  "b">

<!ENTITY  rememberSearchForm.label       "Remember search and form history">
<!ENTITY  rememberSearchForm.accesskey   "f">

<!ENTITY  clearOnClose.label             "Clear history when &brandShortName; closes">
<!ENTITY  clearOnClose.accesskey         "r">

<!ENTITY  clearOnCloseSettings.label     "Settings…">
<!ENTITY  clearOnCloseSettings.accesskey "t">

<!ENTITY  browserContainersLearnMore.label      "Learn more">
<!ENTITY  browserContainersEnabled.label        "Enable Container Tabs">
<!ENTITY  browserContainersEnabled.accesskey    "n">


