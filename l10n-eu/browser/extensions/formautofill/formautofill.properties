# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S(e)k orain helbideak gordetzen dituzun inprimakiak azkarrago bete ditzazun.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Inprimakiak automatikoki betetzeko aukerak
autofillOptionsLinkOSX = Inprimakiak automatikoki betetzeko hobespenak
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Inprimakiak automatikoki betetzea eta segurtasun-aukerak
autofillSecurityOptionsLinkOSX = Inprimakiak automatikoki betetzea eta segurtasun-hobespenak
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Aldatu inprimakiak automatikoki betetzeko aukerak
changeAutofillOptionsOSX = Aldatu inprimakiak automatikoki betetzeko hobespenak
changeAutofillOptionsAccessKey = d
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Partekatu helbideak sinkronizatutako gailuekin
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Partekatu kreditu-txartelak sinkronizatutako gailuekin
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Zure helbidea informazio berri honekin eguneratu nahi duzu?
updateAddressDescriptionLabel = Eguneratu beharreko helbidea:
createAddressLabel = Sortu helbide berria
createAddressAccessKey = S
updateAddressLabel = Eguneratu helbidea
updateAddressAccessKey = E
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = %S(e)k kreditu-txartel hau gordetzea nahi duzu? (Segurtasun-kodea ez da gordeko)
saveCreditCardDescriptionLabel = Gorde beharreko kreditu-txartela:
saveCreditCardLabel = Gorde kreditu-txartela
saveCreditCardAccessKey = G
cancelCreditCardLabel = Ez gorde
cancelCreditCardAccessKey = E
neverSaveCreditCardLabel = Inoiz ez gorde kreditu-txartelak
neverSaveCreditCardAccessKey = n
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Zure kreditu-txartela informazio berri honekin eguneratu nahi duzu?
updateCreditCardDescriptionLabel = Eguneratu beharreko kreditu-txartela:
createCreditCardLabel = Sortu kreditu-txartel berria
createCreditCardAccessKey = S
updateCreditCardLabel = Eguneratu kreditu-txartela
updateCreditCardAccessKey = E
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Ireki inprimakiak automatikoki betetzeko mezuen panela

# LOCALIZATION NOTE (autocompleteFooterOption, autocompleteFooterOptionOSX): Used as a label for the button,
# displayed at the bottom of the drop down suggestion, to open Form Autofill browser preferences.
autocompleteFooterOption = Inprimakiak automatikoki betetzeko aukerak
autocompleteFooterOptionOSX = Inprimakiak automatikoki betetzeko hobespenak

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Aukera gehiago
autocompleteFooterOptionOSXShort = Hobespenak
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = helbidea
category.name = izena
category.organization2 = erakundea
category.tel = telefonoa
category.email = helbide elektronikoa
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Automatikoki ere bete %S
phishingWarningMessage2 = Automatikoki bete %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S(e)k gune ez-segurua atzeman du. Inprimakiak automatikoki betetzea desgaituta dago behin-behinean.

# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Garbitu automatikoki betetako inprimakia

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Automatikoki bete helbideak
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Argibide gehiago
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Gordetako helbideak…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Automatikoki bete kreditu-txartelak
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Gordetako kreditu-txartelak…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Gordetako helbideak
manageCreditCardsTitle = Gordetako kreditu-txartelak
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Helbideak
creditCardsListHeader = Kreditu-txartelak
showCreditCardsBtnLabel = Erakutsi kreditu-txartelak
hideCreditCardsBtnLabel = Ezkutatu kreditu-txartelak
removeBtnLabel = Kendu
addBtnLabel = Gehitu…
editBtnLabel = Editatu…

# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Gehitu helbide berria
editAddressTitle = Editatu helbidea
givenName = Izena
additionalName = Bigarren izena
familyName = Abizenak
organization2 = Erakundea
streetAddress = Helbidea
city = Hiria
province = Probintzia
state = Estatua
postalCode = Posta-kodea
zip = Posta-kodea
country = Herrialdea edo eskualdea
tel = Telefonoa
email = Helbide elektronikoa
cancelBtnLabel = Utzi
saveBtnLabel = Gorde
countryWarningMessage = Inprimakiak automatikoki betetzea momentuz AEBtako helbideentzat dago erabilgarri soilik

countryWarningMessage2 = Inprimakiak automatikoki betetzea momentuz zenbait herrialdetan dago erabilgarri soilik.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Gehitu kreditu-txartel berria
editCreditCardTitle = Editatu kreditu-txartela
cardNumber = Txartelaren zenbakia
nameOnCard = Txarteleko izena
cardExpires = Iraungitze-data
