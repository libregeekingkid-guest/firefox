<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "मागे जा">
<!ENTITY safeb.palm.seedetails.label "तपशील पहा">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "ही साईट फसवी नाही…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "<a id='advisory_provider'/> द्वारे प्रदान केलेले सल्लागार.">


<!ENTITY safeb.blocked.malwarePage.title2 "या वेबसाइटला भेट दिल्यास आपल्या संगणकाला धोका पोहोचू शकतो">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; हे पृष्ठ अवरोधित केले कारण हे दुर्भावनापूर्ण सॉफ्टवेअर स्थापित करण्याचा प्रयत्न करू शकते जे आपल्या संगणकावर वैयक्तिक माहिती चोरू शकते किंवा हटवू शकते.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> ला <a id='error_desc_link'>दुर्भावनापूर्ण सॉफ्टवेअर समाविष्ट केल्याच्या रुपात नोंदवले गेले आहे</a>. आपण <a id='report_detection'>ओळख समस्या नोंदवू शकता</a> किंवा <a id='ignore_warning_link'>धोका दुर्लक्ष करू शकता</a> आणि या असुरक्षित साइटवर जाऊ शकता.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> यामध्ये <a id='error_desc_link'>धोकादायक सॉफ्टवेअर आहे असे निदर्शनास आणण्यात आलेले आहे</a>. आपण <a id='report_detection'>शोधण्याची समस्या</a> निदर्शनास आणु शकता.">

<!ENTITY safeb.blocked.malwarePage.learnMore "व्हायरस आणि मालवेअर असलेल्या धोकादायक वेब मजकूराबद्दल आणि यापासून आपला संगणक कसा वाचवायचा याबद्दल <a id='learn_more_link'>StopBadware.org</a> इथे अधिक जाणून घ्या. &brandShortName; च्या फिशिंग आणि मालवेअर सुरक्षेबद्दल <a id='firefox_support'>support.mozilla.org</a> इथे अधिक जाणून घ्या.">


<!ENTITY safeb.blocked.unwantedPage.title2 "साइटमध्ये पुढे नुकसानदेय कार्यक्रम असू शकतात">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; ने या पृष्ठाला आडवले कारण हे पृष्ठ फसवेगिरीने काही प्रणाली प्रस्थापित करून आपला ब्राऊझिंग चा अनुभव दूषित करू शकते(उदाहरणार्थ, आपले गृह पृष्ठ बदलून किंवा आपण ज्या संकेतस्थळांना भेट देता त्यावर अति जाहिराती दाखवून).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> हे संकेतस्थळ <a id='error_desc_link'>घातक सॉफ्टवेअर असलेले</a> म्हणून निदर्शनास आणून देण्यात आले आहे. आपण <a id='ignore_warning_link'>धोका दुर्लक्षित करून</a> या असुरक्षित संकेतस्थळावर जाऊ शकता.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> हे संकेतस्थळ <a id='error_desc_link'>घातक सॉफ्टवेअर असलेले</a> म्हणून निदर्शनास आणून देण्यात आले आहे.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "धोकादायक व अवांछित सॉफ्टवेअर बद्दल इथे अधिक जाणून घ्या <a id='learn_more_link'>अवांछित सॉफ्टवेअर धोरण</a>. &brandShortName; फिशिंग आणि मालवेअर सुरक्षेबद्दल इथे अधिक जाणून घ्या <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "पुढे फसवी साईट आहे">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; ने या पृष्ठाला आडवले कारण हे पृष्ठ फसवेगिरीने आपल्याला काहीतरी करण्यास प्रवृत्त करू शकते जसे की धोकादायक सॉफ्टवेअर प्रस्थापित करणे किंवा पासवर्डस व क्रेडिट कार्ड सारखी वैयक्तिक माहिती उघड करणे.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> हे <a id='error_desc_link'>धोकादायक संकेतस्थळ</a> आहे असे निदर्शनास आणून देण्यात आलेले आहे. आपण <a id='report_detection'>शोधण्याची समस्या</a> नोंद करू शकता किंवा हा <a id='ignore_warning_link'>धोका दुर्लक्षित</a> करून ह्या असुरक्षित संकेतस्थळावर जाऊ शकता.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> हे <a id='error_desc_link'>धोकादायक संकेतस्थळ</a> आहे असे निदर्शनास आणून देण्यात आलेले आहे. आपण <a id='report_detection'>शोधण्याची समस्या</a> नोंद करू शकता.">

<!ENTITY safeb.blocked.phishingPage.learnMore "फसवी संकेतस्थळे व फिशिंग याबद्दल <a id='learn_more_link'>www.antiphishing.org</a> इथे अधिक जाणून घ्या. &brandShortName; च्या फिशिंग आणि मालवेअर सुरक्षेबाबत <a id='firefox_support'>support.mozilla.org</a> इथे अधिक जाणून घ्या.">


<!ENTITY safeb.blocked.harmfulPage.title "पुढील संकेतस्थळावर मालवेअर असू शकते">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; ने हे पृष्ठ अडवले आहे कारण ते धोकादायक अँप्स प्रस्थापित करून आपली माहिती चोरू किंवा नष्ट करू शकते (उदा; प्रतिमा, पासवर्डस, संदेश किंवा क्रेडिट कार्ड्स).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> या संकेतस्थळावर <a id='error_desc_link'>संभाव्य धोकादायक प्रणाली आहे</a> असे निदर्शनास आणले आहे. आपण हा <a id='ignore_warning_link'>धोका दुर्लक्षित करून</a> या असुरक्षित संकेतस्थळाला भेट देऊ शकता.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> या संकेतस्थळावर <a id='error_desc_link'>संभाव्य धोकादायक प्रणाली आहे</a> असे निदर्शनास आणले आहे.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "&brandShortName; च्या फिशिंग आणि मालवेअर सुरक्षेबाबत <a id='firefox_support'>इथे</a> अधिक जाणून घ्या.">
