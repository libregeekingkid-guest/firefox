# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = Tällä sivulla näet Kaukomittaus-toiminnallisuuden keräämät tiedot suorituskyvystä, laitteistosta, ominaisuuksien käytöstä ja muokkauksista. Sivulla näkyvät tiedot lähetetään %1$Slle auttamaan %2$Sin kehityksessä.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = Kaukomittaus on %1$S ja laajennettu kaukomittaus on %2$S.
telemetryEnabled = käytössä
telemetryDisabled = pois käytöstä
extendedTelemetryEnabled = käytössä
extendedTelemetryDisabled = pois käytöstä

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = Kaukomittaus kerää %1$S ja tietojen lähetys on %2$S.
releaseData = release-tietoja
prereleaseData = pre-release-tietoja
telemetryUploadEnabled = käytössä
telemetryUploadDisabled = pois käytöstä

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = Kukin informaation palanen lähetetään koottuna ”%1$S”. Katselet juuri %2$S-pingiä.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = Kukin informaation palanen lähetetään koottuna ”%1$S”. Katselet juuri %2$S-pingiä.
pingExplanationLink = pingeihin
currentPing = nykyinen

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = Etsi: %1$S
filterAllPlaceholder = Etsi kaikista osioista

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = Tulokset haulle ”%1$S”
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = Ei hakutuloksia osiosta %1$S haulle ”%2$S”
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = Ei hakutuloksia mistään osiosta haulle ”%S”
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = Osiossa ”%S” ei ole tällä hetkellä dataa
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = nykyinen ping
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = kaikki

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = näytettä
histogramAverage = keskimäärin
histogramSum = yhteensä
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = Kopioi

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = Kaukomittausloki
telemetryLogHeadingId = Tunnus
telemetryLogHeadingTimestamp = Aikaleima
telemetryLogHeadingData = Tiedot

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = Hitaat SQL-lauseet pääsäikeessä
slowSqlOther = Hitaat SQL-lauseet apusäikeissä
slowSqlHits = Osumat
slowSqlAverage = Keskimääräinen aika (ms)
slowSqlStatement = Lause

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = Lisäosan tunnus
addonTableDetails = Tiedot
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = %1$S-toimittaja

keysHeader = Ominaisuus
namesHeader = Nimi
valuesHeader = Arvo

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = Jäätymisraportti #%1$S (%2$S sekuntia)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = %1$S (kaappausmäärä: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = Myöhästynyt kirjoitus #%1$S

stackTitle = Pino:
memoryMapTitle = Muistikartta:

errorFetchingSymbols = Tapahtui virhe haettaessa symboleita. Tarkista, että Internet-yhteys on kunnossa ja yritä uudestaan.

parentPayload = Isännän tiedot
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = Lapsen tiedot %1$S
timestampHeader = aikaleima
categoryHeader = luokka
methodHeader = metodi
objectHeader = objekti
extraHeader = lisätietoa
