<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "برگشتن">
<!ENTITY safeb.palm.seedetails.label "مشاهده جزئیات">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "این یک سایت گمراه‌کننده نیست…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "مشاوره فراهم شده توسط <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "بازدید از این سایت ممکن است به رایانه‌تان صدمه بزند">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; این صفحه را مسدود کرده است به این دلیل کرده این صفحه تلاش کرده است نرم افزار مخربی را نصب کند که ممکن است اطلاعات شخصی شما کامپیوتر را حذف کند یا به سرقت ببرد.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> به عنوان <a id='error_desc_link'>یک سایت با نرم‌افزارهای مخرب گزارش شده است</a>. شما می‌توانید <a id='report_detection'>یک گزارشِ تشخیصِ اشتباه ارسال کنید</a> یا <a id='ignore_warning_link'>این خطر را نادیده بگیرید</a> و به این صفحه ناامن بروید.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> به عنوان <a id='error_desc_link'>سایت با نرم‌افزارهای مخرب گزارش شده است</a>. شما می‌توانید<a id='report_detection'> یک گزارشِ تشخیصِ اشتباه ارسال کنید</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "در مورد محتوای مضر در وب شامل ویروس‌ها و جاسوس‌افزارها و نحوهٔ محافظت از رایانه خود در <a id='learn_more_link'>StopBadware.org</a> اطلاعات بیشتری کسب کنید. در مورد نحوه حفاظت &brandShortName; از فیشینگ و جاسوس‌افزارها در <a id='firefox_support'>support.mozilla.org</a> بیشتر اطلاعات کسب کنید.">


<!ENTITY safeb.blocked.unwantedPage.title2 "سایت پیش‌رو ممکن است شامل نرم‌افزارهای مخرب باشد">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; این صفحه را مسدود کرده است زیرا این صفحه احتمالا در تلاش برای فریب دادن شما برای نصب یک برنامه مضر بر روی تجربه مرورکردن شما بوده است( برای مثال تغییر صفحه اولیه شما یا نمایش تبلیغات اضافه).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> به عنوان <a id='error_desc_link'>سایت حاوی نرم‌افزار مخرب گزارش شد است</a>. شما می‌توانید <a id='ignore_warning_link'>این خطر را در نظر نادیده بگیرید</a> و به این پایگاه اینترنتی ناامن بروید.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> گزارش شده است <a id='error_desc_link'>به عنوان سایت حاوی نرم‌افزارهای مخرب گزارش شده است</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "در مورد نرم‌افزارهای مضر و غیرضروری در <a id='learn_more_link'>سیاست‌های نرم‌افزارهای ناخواسته</a> بیشتر بخوانید. در مورد نحوه رفتار &brandShortName; با فیشینگ و محافظت در برابر جاسوس‌افزارها در <a id='firefox_support'>support.mozilla.org</a> اطلاعات بیشتر کسب کنید.">


<!ENTITY safeb.blocked.phishingPage.title3 "یک پایگاه اینترنتی فریبنده در مقابل شماست">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; این صفحه را مسدود کرده است زیرا ممکن بود شما رو فریب دهد و موظف به انجام کار یا نصب نرم‌افزاری کند که باعث آشکار شدن اطلاعات شخصی شما شامل کلمه عبور یا کارت‌های اعتباری شود.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> به عنوان <a id='error_desc_link'>یک پایگاه اینترنتی فریب دهنده شناخته شده است</a>. شما می‌توانید <a id='report_detection'>این مشکل شناسایی شده رو گزارش دهید</a> یا <a id='ignore_warning_link'>این خطر را نادیده بگیرید</a> و به این پایگاه اینترنتی ناامن بروید.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> به عنوان <a id='error_desc_link'>یک پایگاه اینترنتی فریبنده گزارش شده است</a>. شما می‌توانید <a id='report_detection'>یک گزارشِ شناساییِ اشتباه ارسال کنید</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "در مورد پایگاه‌های اینترنتی فریب‌دهنده و فیشینگ در<a id='learn_more_link'>www.antiphishing.org</a> بیشتر اطلاعات کسب کنید. در مورد نحوه رفتار &brandShortName; با فیشینک و محافظت در مورد جاسوس‌افزارها در <a id='firefox_support'>support.mozilla.org</a> بیشتر اطلاعات کسب کنید.">


<!ENTITY safeb.blocked.harmfulPage.title "این سایت ممکن است شامل بدافزار باشد">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; این صفحه را به این دلیل مسدود کرده که ممکن است سعی کند برنامه‌های خطرناکی نصب کند تا اطلاعات شما را بدزدد یا حذف کند (برای مثال، عکس‌ها، گذرواژه‌ها، پیام‌ها و کارت‌های اعتباری).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> به عنوان <a id='error_desc_link'>یک پایگاه با نرم‌افزارهای مخرب گزارش شده است</a>. شما می‌توانید <a id='ignore_warning_link'>این خطر را نادیده بگیرید</a> و به این پایگاه اینترنتی ناامن بروید.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> به عنوان <a id='error_desc_link'>یک پایگاه شامل نرم‌افزارهای مخرب گزارش شده است</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "در مورد راهکارهای &brandShortName; برای محافظت در برابر فیشینگ و بدافزار‌ها در <a id='firefox_support'>support.mozilla.org</a> بیشتر اطلاعات کسب کنید.">
