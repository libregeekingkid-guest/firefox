<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  fontsDialog.title                       "फन्टहरू">

<!ENTITY  fonts.label                             "निम्नको लागि फन्ट्सहरू">
<!ENTITY  fonts.accesskey                         "F">

<!ENTITY  size2.label                             "आकार">
<!ENTITY  sizeProportional.accesskey              "z">
<!ENTITY  sizeMonospace.accesskey                 "e">

<!ENTITY  proportional2.label                     "समानुपातिक">
<!ENTITY  proportional2.accesskey                 "P">

<!ENTITY  serif2.label                            "सेरिफ">
<!ENTITY  serif2.accesskey                        "S">
<!ENTITY  sans-serif2.label                       "सान-सेरिफ">
<!ENTITY  sans-serif2.accesskey                   "n">
<!ENTITY  monospace2.label                        "मोनोस्पेस">
<!ENTITY  monospace2.accesskey                    "M">

<!-- LOCALIZATION NOTE (font.langGroup.latin) :
     Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language. -->
<!ENTITY  font.langGroup.latin                    "ल्याटिन">
<!ENTITY  font.langGroup.japanese                 "जापानी">
<!ENTITY  font.langGroup.trad-chinese             "पुरानो चिनियाँ (ताइवान )">
<!ENTITY  font.langGroup.simpl-chinese            "सरलीकृत चिनियाँ">
<!ENTITY  font.langGroup.trad-chinese-hk          "परम्परागत चिनियाँ (हंगकंग )">
<!ENTITY  font.langGroup.korean                   "कोरियाली">
<!ENTITY  font.langGroup.cyrillic                 "Cyrillic">
<!ENTITY  font.langGroup.el                       "ग्रिक">
<!ENTITY  font.langGroup.other                    "अन्य लेखन प्रणालीहरू">
<!ENTITY  font.langGroup.thai                     "थाइ">
<!ENTITY  font.langGroup.hebrew                   "हिब्रु">
<!ENTITY  font.langGroup.arabic                   "अरबी">
<!ENTITY  font.langGroup.devanagari               "देवनागरी ">
<!ENTITY  font.langGroup.tamil                    "तामिल">
<!ENTITY  font.langGroup.armenian                 "आर्मेनियन">
<!ENTITY  font.langGroup.bengali                  "बङ्गाली">
<!ENTITY  font.langGroup.canadian                 "संयुक्त क्यानाडियन सिल्याब्यारी">
<!ENTITY  font.langGroup.ethiopic                 "इथोपियाली">
<!ENTITY  font.langGroup.georgian                 "जर्जीयन">
<!ENTITY  font.langGroup.gujarati                 "गुजराती">
<!ENTITY  font.langGroup.gurmukhi                 "गुरमुखी">
<!ENTITY  font.langGroup.khmer                    "खमेर">
<!ENTITY  font.langGroup.malayalam                "मलायलम">
<!ENTITY  font.langGroup.math                     "गणित">
<!ENTITY  font.langGroup.odia                     "उडिया">
<!ENTITY  font.langGroup.telugu                   "तेल्गु">
<!ENTITY  font.langGroup.kannada                  "कन्नाडा">
<!ENTITY  font.langGroup.sinhala                  "सिन्हाली">
<!ENTITY  font.langGroup.tibetan                  "तिब्बती">
<!-- Minimum font size -->
<!ENTITY minSize2.label                           "न्यूनतम अक्षर साइज">
<!ENTITY minSize2.accesskey                       "o">
<!ENTITY minSize.none                             "कुनै पनि होइन">

<!-- default font type -->
<!ENTITY  useDefaultFontSerif.label               "सेरिफ">
<!ENTITY  useDefaultFontSansSerif.label           "सान सेरिफ">

<!ENTITY  allowPagesToUseOwn.label                "तपाईको माथि उल्लिखित चयनहरूको सट्टामा आफ्नै फन्टहरू छनौट गर्न पृष्ठहरू लाई अनुमति दिनुहोस्">
<!ENTITY  allowPagesToUseOwn.accesskey            "A">

<!ENTITY languages.customize.Fallback2.grouplabel "पुरानो सामग्रीको लागि वर्ण संकेतन">
<!ENTITY languages.customize.Fallback3.label      "निवर्तमान र पाठ सङ्केतन">
<!ENTITY languages.customize.Fallback3.accesskey  "T">
<!ENTITY languages.customize.Fallback2.desc       "आफ्नो संकेतन देखाउन नसक्ने सामग्रीमा यो संकेतन प्रयोग गरिन्छ।">

<!ENTITY languages.customize.Fallback.auto        "वर्तमान लोक्यालका लागि पूर्वनिर्धारित">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.arabic):
     Translate "Arabic" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.arabic      "अरबी">
<!ENTITY languages.customize.Fallback.baltic      "अन्य भाषाहरू">
<!ENTITY languages.customize.Fallback.ceiso       "केन्द्रीय युरोपेली, ISO">
<!ENTITY languages.customize.Fallback.cewindows   "मध्य यूरोपीय, माईक्रोसफ्ट">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.simplified):
     Translate "Chinese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.simplified  "सरलीकृत चिनियाँ">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.traditional):
     Translate "Chinese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.traditional "चिनीयाँ, परम्परागत">
<!ENTITY languages.customize.Fallback.cyrillic    "Cyrillic">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.greek):
     Translate "Greek" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.greek       "ग्रीक">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.hebrew):
     Translate "Hebrew" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.hebrew      "हिब्रु">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.japanese):
     Translate "Japanese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.japanese    "जापानी">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.korean):
     Translate "Korean" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.korean      "कोरियाली">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.thai):
     Translate "Thai" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.thai        "थाइ">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.turkish):
     Translate "Turkish" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.turkish     "टर्कीस">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.vietnamese):
     Translate "Vietnamese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.vietnamese  "भियतनामिज">
<!ENTITY languages.customize.Fallback.other       "अन्य (पश्चिमी युरोपेली सहित)">
