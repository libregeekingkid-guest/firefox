<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  fontsDialog.title                       "Şriftlər">

<!ENTITY  fonts.label                             "Şriftlər:">
<!ENTITY  fonts.accesskey                         "F">

<!ENTITY  size2.label                             "Ölçü">
<!ENTITY  sizeProportional.accesskey              "o">
<!ENTITY  sizeMonospace.accesskey                 "a">

<!ENTITY  proportional2.label                     "Mütənasib">
<!ENTITY  proportional2.accesskey                 "P">

<!ENTITY  serif2.label                            "Serif">
<!ENTITY  serif2.accesskey                        "S">
<!ENTITY  sans-serif2.label                       "Sans-serif">
<!ENTITY  sans-serif2.accesskey                   "n">
<!ENTITY  monospace2.label                        "Monospace">
<!ENTITY  monospace2.accesskey                    "M">

<!-- LOCALIZATION NOTE (font.langGroup.latin) :
     Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language. -->
<!ENTITY  font.langGroup.latin                    "Latınca">
<!ENTITY  font.langGroup.japanese                 "Yaponca">
<!ENTITY  font.langGroup.trad-chinese             "Ənənəvi Çincə (Tayvan)">
<!ENTITY  font.langGroup.simpl-chinese            "Sadə Çincə">
<!ENTITY  font.langGroup.trad-chinese-hk          "Ənənəvi Çincə (Hong Kong)">
<!ENTITY  font.langGroup.korean                   "Koreyaca">
<!ENTITY  font.langGroup.cyrillic                 "Kiril">
<!ENTITY  font.langGroup.el                       "Yunan">
<!ENTITY  font.langGroup.other                    "Başqa Yazma Sistemləri">
<!ENTITY  font.langGroup.thai                     "Tay dili">
<!ENTITY  font.langGroup.hebrew                   "İvritcə">
<!ENTITY  font.langGroup.arabic                   "Ərəbcə">
<!ENTITY  font.langGroup.devanagari               "Devanagari">
<!ENTITY  font.langGroup.tamil                    "Tamil dili">
<!ENTITY  font.langGroup.armenian                 "Ermənicə">
<!ENTITY  font.langGroup.bengali                  "Benqal dili">
<!ENTITY  font.langGroup.canadian                 "Birləşmiş Kanada heca yazısı">
<!ENTITY  font.langGroup.ethiopic                 "Etiyopik">
<!ENTITY  font.langGroup.georgian                 "Gürcü dili">
<!ENTITY  font.langGroup.gujarati                 "Gujarati dili">
<!ENTITY  font.langGroup.gurmukhi                 "Qurmuki">
<!ENTITY  font.langGroup.khmer                    "Kamboca dili">
<!ENTITY  font.langGroup.malayalam                "Malayalam dili">
<!ENTITY  font.langGroup.math                     "Riyaziyyat">
<!ENTITY  font.langGroup.odia                     "Odia">
<!ENTITY  font.langGroup.telugu                   "Teluqu dili">
<!ENTITY  font.langGroup.kannada                  "Kanada dili">
<!ENTITY  font.langGroup.sinhala                  "Seylonca">
<!ENTITY  font.langGroup.tibetan                  "Tibet Dili">
<!-- Minimum font size -->
<!ENTITY minSize2.label                           "Ən kiçik şrift ölçüsü">
<!ENTITY minSize2.accesskey                       "o">
<!ENTITY minSize.none                             "Yoxdur">

<!-- default font type -->
<!ENTITY  useDefaultFontSerif.label               "Serif">
<!ENTITY  useDefaultFontSansSerif.label           "Sans Serif">

<!ENTITY  allowPagesToUseOwn.label                "Saytlara yuxarıda seçdikləriniz şriftlərin yerinə öz şriftlərini işlətməyə icazə verin">
<!ENTITY  allowPagesToUseOwn.accesskey            "A">

<!ENTITY languages.customize.Fallback2.grouplabel "İrsi Məzmun üçün mətn simvol kodlaşdırması">
<!ENTITY languages.customize.Fallback3.label      "Alternativ Mətn Kodlaşdırması">
<!ENTITY languages.customize.Fallback3.accesskey  "T">
<!ENTITY languages.customize.Fallback2.desc       "Bu simvol kodlaşdırması öz kodlaşdırmasını təyin edə bilməyən irsi məzmun üçün işlədilib.">

<!ENTITY languages.customize.Fallback.auto        "Mövcud məkan üçün əsas dəyər">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.arabic):
     Translate "Arabic" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.arabic      "Ərəbcə">
<!ENTITY languages.customize.Fallback.baltic      "Baltik">
<!ENTITY languages.customize.Fallback.ceiso       "Orta Avropa, ISO">
<!ENTITY languages.customize.Fallback.cewindows   "Orta Avropa, Microsoft">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.simplified):
     Translate "Chinese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.simplified  "Çincə, Sadələşdirilmiş">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.traditional):
     Translate "Chinese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.traditional "Çincə, Ənənəvi">
<!ENTITY languages.customize.Fallback.cyrillic    "Kiril">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.greek):
     Translate "Greek" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.greek       "Yunanca">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.hebrew):
     Translate "Hebrew" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.hebrew      "İvritcə">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.japanese):
     Translate "Japanese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.japanese    "Yaponca">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.korean):
     Translate "Korean" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.korean      "Koreya dili">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.thai):
     Translate "Thai" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.thai        "Tay dili">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.turkish):
     Translate "Turkish" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.turkish     "Türkcə">
<!-- LOCALIZATION NOTE (languages.customize.Fallback.vietnamese):
     Translate "Vietnamese" as an adjective for an encoding, not as the name of the language. -->
<!ENTITY languages.customize.Fallback.vietnamese  "Vyetnamca">
<!ENTITY languages.customize.Fallback.other       "Digər (Qərbi Avropa daxil olmaqla)">
