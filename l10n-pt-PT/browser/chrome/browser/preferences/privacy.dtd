<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  trackingProtectionHeader2.label      "Proteção contra monitorização">
<!ENTITY  trackingProtection2.description      "A monitorização é a recolha dos seus dados de navegação através de múltiplos websites. A monitorização pode ser utilizada para construir um perfil e mostrar conteúdo baseado na sua navegação e informação pessoal.">
<!ENTITY  trackingProtection2.radioGroupLabel  "Utilizar Proteção contra monitorização para bloquear trackers conhecidos">
<!ENTITY  trackingProtection3.description      "A Proteção contra monitorização bloqueia trackers online que recolhem os seus dados de navegação através de múltiplos websites.">
<!ENTITY  trackingProtection3.radioGroupLabel  "Utilizar Proteção contra monitorização para bloquear trackers conhecidos">
<!ENTITY  trackingProtectionAlways.label       "Sempre">
<!ENTITY  trackingProtectionAlways.accesskey   "r">
<!ENTITY  trackingProtectionPrivate.label      "Apenas em janelas privadas">
<!ENTITY  trackingProtectionPrivate.accesskey  "l">
<!ENTITY  trackingProtectionNever.label        "Nunca">
<!ENTITY  trackingProtectionNever.accesskey    "N">
<!ENTITY  trackingProtectionLearnMore.label    "Saber mais">
<!ENTITY  trackingProtectionLearnMore2.label    "Saber mais acerca da Proteção contra monitorização e a sua privacidade">
<!ENTITY  trackingProtectionExceptions.label   "Exceções…">
<!ENTITY  trackingProtectionExceptions.accesskey "x">

<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the release and beta channel. -->
<!ENTITY trackingProtectionPBM6.label         "Utilizar Proteção contra monitorização na Navegação privada para bloquear trackers conhecidos">
<!ENTITY trackingProtectionPBM6.accesskey     "v">
<!ENTITY trackingProtectionPBMLearnMore.label "Saber mais">
<!ENTITY changeBlockList2.label               "Alterar lista de bloqueio…">
<!ENTITY changeBlockList2.accesskey           "A">

<!ENTITY  doNotTrack.description        "Enviar um sinal “Do Not Track” significando que não deseja ser monitorizado(a)">
<!ENTITY  doNotTrack.learnMore.label    "Saber mais">
<!ENTITY  doNotTrack.default.label      "Apenas ao utilizar a Proteção contra monitorização">
<!ENTITY  doNotTrack.always.label       "Sempre">

<!ENTITY  history.label                 "Histórico">
<!ENTITY  permissions.label             "Permissões">

<!ENTITY  addressBar.label              "Barra de endereço">
<!ENTITY  addressBar.suggest.label      "Ao utilizar a barra de endereço, sugerir">
<!ENTITY  locbar.history2.label         "Histórico de navegação">
<!ENTITY  locbar.history2.accesskey     "H">
<!ENTITY  locbar.bookmarks.label        "Marcadores">
<!ENTITY  locbar.bookmarks.accesskey    "M">
<!ENTITY  locbar.openpage.label         "Separadores abertos">
<!ENTITY  locbar.openpage.accesskey     "o">
<!ENTITY  locbar.searches.label         "Pesquisar relacionadas do motor de pesquisa por predefinição">
<!ENTITY  locbar.searches.accesskey     "d">

<!ENTITY  suggestionSettings2.label     "Alterar preferências para as sugestões dos motores de pesquisa">

<!ENTITY  acceptCookies2.label          "Aceitar cookies dos websites">
<!ENTITY  acceptCookies2.accesskey      "A">

<!ENTITY  acceptThirdParty2.pre.label     "Aceitar cookies de terceiros">
<!ENTITY  acceptThirdParty2.pre.accesskey "e">

<!ENTITY  acceptCookies3.label          "Aceitar cookies e dados de sites de websites">
<!ENTITY  acceptCookies3.accesskey      "A">

<!ENTITY  blockCookies.label            "Bloquear cookies e dados de sites (pode causar que os websites quebrem)">
<!ENTITY  blockCookies.accesskey        "B">

<!ENTITY  acceptThirdParty3.pre.label     "Aceitar cookies de terceiros e dados de sites">
<!ENTITY  acceptThirdParty3.pre.accesskey "s">
<!ENTITY  acceptThirdParty.always.label   "Sempre">
<!ENTITY  acceptThirdParty.never.label    "Nunca">
<!ENTITY  acceptThirdParty.visited.label  "Dos visitados">

<!ENTITY  keepUntil2.label              "Manter até">
<!ENTITY  keepUntil2.accesskey          "a">

<!ENTITY  expire.label                  "expirarem">
<!ENTITY  close.label                   "eu fechar o &brandShortName;">

<!ENTITY  cookieExceptions.label        "Exceções…">
<!ENTITY  cookieExceptions.accesskey    "E">

<!ENTITY  showCookies.label             "Mostrar cookies…">
<!ENTITY  showCookies.accesskey         "s">

<!ENTITY  historyHeader2.pre.label         "O &brandShortName; irá">
<!ENTITY  historyHeader2.pre.accesskey     "i">
<!ENTITY  historyHeader.remember.label     "Memorizar histórico">
<!ENTITY  historyHeader.dontremember.label "Nunca memorizar histórico">
<!ENTITY  historyHeader.custom.label       "Utilizar definições personalizadas para o histórico">
<!ENTITY  historyHeader.post.label         "">

<!ENTITY  rememberDescription.label      "O &brandShortName; irá memorizar o seu histórico de navegação, de transferências, de formulários e de pesquisa, e irá manter os cookies dos websites que visitar.">

<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->
<!ENTITY  rememberActions.pre.label           "Pode ">
<!ENTITY  rememberActions.clearHistory.label  "limpar o seu histórico">
<!ENTITY  rememberActions.middle.label        " ou ">
<!ENTITY  rememberActions.removeCookies.label "remover cookies individualmente">
<!ENTITY  rememberActions.post.label          ".">

<!ENTITY  dontrememberDescription.label  "O &brandShortName; irá utilizar as mesmas definições da navegação privada e não irá memorizar qualquer histórico enquanto navega na Web.">

<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->
<!ENTITY  dontrememberActions.pre.label          "Também pode ">
<!ENTITY  dontrememberActions.clearHistory.label "limpar todo o histórico">
<!ENTITY  dontrememberActions.post.label         ".">

<!ENTITY  clearHistoryButton.label       "Limpar histórico…">
<!ENTITY  clearHistoryButton.accesskey   "s">

<!ENTITY  privateBrowsingPermanent2.label "Utilizar sempre o modo de navegação privada">
<!ENTITY  privateBrowsingPermanent2.accesskey "p">

<!ENTITY  rememberHistory2.label      "Memorizar o meu histórico de navegação e de transferências">
<!ENTITY  rememberHistory2.accesskey  "h">

<!ENTITY  rememberSearchForm.label       "Memorizar histórico de pesquisas e de formulários">
<!ENTITY  rememberSearchForm.accesskey   "f">

<!ENTITY  clearOnClose.label             "Limpar o histórico quando o &brandShortName; for fechado">
<!ENTITY  clearOnClose.accesskey         "i">

<!ENTITY  clearOnCloseSettings.label     "Definições…">
<!ENTITY  clearOnCloseSettings.accesskey "e">

<!ENTITY  browserContainersLearnMore.label      "Saber mais">
<!ENTITY  browserContainersEnabled.label        "Ativar separadores contentores">
<!ENTITY  browserContainersEnabled.accesskey    "n">
<!ENTITY  browserContainersSettings.label        "Definições…">
<!ENTITY  browserContainersSettings.accesskey    "i">

<!ENTITY  a11yPrivacy.checkbox.label     "Impedir serviços de acessibilidade de aceder ao seu navegador">
<!ENTITY  a11yPrivacy.checkbox.accesskey "a">
<!ENTITY  a11yPrivacy.learnmore.label    "Saber mais">
<!ENTITY enableSafeBrowsingLearnMore.label "Saber mais">
