# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S lagrar no adresser slik at du kan fylle ut skjema raskare.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Innstillingar for autoutfylling av skjema
autofillOptionsLinkOSX = Innstillingar for autoutfylling av skjema
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Innstillingar for autoutfylling av skjema og sikkerheit
autofillSecurityOptionsLinkOSX = Innstillingar for autoutfylling av skjema og sikkerheit
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Endre innstillingar for autoutfyling av skjema
changeAutofillOptionsOSX = Endre innstillingar for autoutfylling av skjema
changeAutofillOptionsAccessKey = n
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Del adresser med synkroniserte einingar
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Del kredittkort med synkroniserte einingar
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Vil du oppdatere adressa di med denne nye informasjonen?
updateAddressDescriptionLabel = Adresse å oppdatere:
createAddressLabel = Opprett ny adresse
createAddressAccessKey = O
updateAddressLabel = Oppdater adresse
updateAddressAccessKey = A
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Vil du at %S skal lagre kredittkortet ditt? (Sikkerheitskoden vert ikkje lagra)
saveCreditCardDescriptionLabel = Kredittkort å lagre:
saveCreditCardLabel = Lagre kredittkort
saveCreditCardAccessKey = L
cancelCreditCardLabel = Ikkje lagre
cancelCreditCardAccessKey = a
neverSaveCreditCardLabel = Lagre aldri kredittkort
neverSaveCreditCardAccessKey = a
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Vil du oppdatere kredittkortet ditt med denne nye informasjonen?
updateCreditCardDescriptionLabel = Kredittkort å oppdatere:
createCreditCardLabel = Lag eit nytt kredittkort
createCreditCardAccessKey = L
updateCreditCardLabel = Oppdater kredittkort
updateCreditCardAccessKey = O
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Opne meldingspanel for autoutfylling av skjema

# LOCALIZATION NOTE (autocompleteFooterOption, autocompleteFooterOptionOSX): Used as a label for the button,
# displayed at the bottom of the drop down suggestion, to open Form Autofill browser preferences.
autocompleteFooterOption = Innstillingar for autoutfylling av skjema
autocompleteFooterOptionOSX = Innstillingar for autoutfylling av skjema

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Fleire innstillingar
autocompleteFooterOptionOSXShort = Innstillingar
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = adresse
category.name = namn
category.organization2 = organisasjon
category.tel = telefon
category.email = e-post
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Fyller også automatisk ut %S
phishingWarningMessage2 = Fyller ut automatisk %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S har oppdaga ein usikker nettstad. Autoutfylling av skjema er mellombels deaktivert

# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Tøm autoutfylt skjema

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Autofyll adresser
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Les meir
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Lagra adresser…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Autofyll kredittkort
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Lagra kredittkort…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Lagra adresser
manageCreditCardsTitle = Lagra kredittkort
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Adressat
creditCardsListHeader = Kredittkort
showCreditCardsBtnLabel = Vis kredittkort
hideCreditCardsBtnLabel = Gøym kredittkort
removeBtnLabel = Fjern
addBtnLabel = Legg til…
editBtnLabel = Rediger…

# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Legg til ny adresse
editAddressTitle = Rediger adresse
givenName = Fornamn
additionalName = Mellomnamn
familyName = Etternamn
organization2 = Organisasjon
streetAddress = Gateadresse
city = Stad
province = Provins
state = Stat
postalCode = Postnummer
zip = Postnummer
country = Land eller region
tel = Telefon
email = E-post
cancelBtnLabel = Avbryt
saveBtnLabel = Lagre
countryWarningMessage = Automatisk utfylling av skjema er for tida berre tilgjengeleg for adressatar i USA

countryWarningMessage2 = Automatisk utfylling av skjema er for tida berre tilgjengeleg i enkelte land.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Legg til nytt kredittkort
editCreditCardTitle = Rediger kredittkort
cardNumber = Kortnummer
nameOnCard = Namn på kort
cardExpires = Går ut
