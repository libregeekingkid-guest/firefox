<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY % brandDTD SYSTEM "chrome://branding/locale/brand.dtd">
%brandDTD;

<!ENTITY loadError.label "Gặp vấn đề khi tải trang">
<!ENTITY retry.label "Thử lại">
<!ENTITY returnToPreviousPage.label "Trở lại">
<!ENTITY advanced.label "Nâng cao">

<!-- Specific error messages -->

<!ENTITY connectionFailure.title "Không thể kết nối">
<!ENTITY connectionFailure.longDesc "&sharedLongDesc;">

<!ENTITY deniedPortAccess.title "Địa chỉ này đã bị chặn">
<!ENTITY deniedPortAccess.longDesc "">

<!ENTITY dnsNotFound.pageTitle "Không tìm thấy máy chủ">
<!-- Localization note (dnsNotFound.title1) - "Hmm" is a sound made when considering or puzzling over something. You don't have to include it in your translation if your language does not have a written word like this. -->
<!ENTITY dnsNotFound.title1 "Chúng tôi gặp khó khăn khi tìm trang web đó.">
<!ENTITY dnsNotFound.longDesc1 "<strong>Nếu địa chỉ đúng, có ba việc bạn có thể thử:</strong> <ul>
  <li>Thử lại sau.</li>
  <li>Kiểm tra kết nối mạng của bạn.</li>
  <li>Nếu bạn đã kết nối mạng nhưng có cài đặt tường lửa, hãy kiểm tra &brandShortName; có quyền truy cập web không.</li> </ul>">

<!ENTITY fileNotFound.title "Không tìm thấy tập tin">
<!ENTITY fileNotFound.longDesc "<ul> <li>Kiểm tra tên xem có lỗi gõ HOA-thường hay lỗi nào khác không.</li> <li>Kiểm tra xem tập tin có bị di chuyển, đổi tên hay bị xóa không.</li> </ul>">

<!ENTITY fileAccessDenied.title "Truy cập tập tin bị từ chối">
<!ENTITY fileAccessDenied.longDesc "<ul> <li>Nó có thể đã bị xóa, chuyển đi, hay quyền truy cập tập tin đã bị chặn.</li> </ul>">

<!ENTITY generic.title "Lỗi.">
<!ENTITY generic.longDesc "<p>Vì lý do nào đó, &brandShortName; không thể mở trang này.</p>">

<!ENTITY captivePortal.title "Đăng nhập vào mạng">
<!ENTITY captivePortal.longDesc2 "
<p>Bạn cần đăng nhập vào mạng trước khi có thể truy cập Internet.</p>
">

<!ENTITY openPortalLoginPage.label2 "Mở trang đăng nhập mạng">

<!ENTITY malformedURI.pageTitle "URL không hợp lệ">
<!-- Localization note (malformedURI.title1) - "Hmm" is a sound made when considering or puzzling over something. You don't have to include it in your translation if your language does not have a written word like this. -->
<!ENTITY malformedURI.title1 "Hmm. Địa chỉ không đúng.">

<!ENTITY netInterrupt.title "Kết nối bị ngắt">
<!ENTITY netInterrupt.longDesc "&sharedLongDesc;">

<!ENTITY notCached.title "Tài liệu bị hết hạn">
<!ENTITY notCached.longDesc "<p>Tài liệu được yêu cầu không có sẵn trong bộ đệm của &brandShortName;.</p><ul><li>Vì lí do bảo mật, &brandShortName; không tự động tải lại các tài liệu nhạy cảm.</li><li>Nhấn nút Thử Lại để yêu cầu tải lại tài liệu từ trang web.</li></ul>">

<!ENTITY netOffline.title "Chế độ ngoại tuyến">
<!ENTITY netOffline.longDesc2 "<ul> <li>Nhấn &quot;Thử lại&quot; để chuyển sang chế độ trực tuyến và tải lại trang.</li> </ul>">

<!ENTITY contentEncodingError.title "Lỗi encoding">
<!ENTITY contentEncodingError.longDesc "<ul> <li>Vui lòng liên hệ với chủ trang web để báo với họ về vấn đề này.</li> </ul>">

<!ENTITY unsafeContentType.title "Kiểu tập tin không an toàn">
<!ENTITY unsafeContentType.longDesc "<ul> <li>Vui lòng liên hệ với chủ trang web để báo với họ về vấn đề này.</li> </ul>">

<!ENTITY netReset.title "Kết nối bị khởi tạo lại">
<!ENTITY netReset.longDesc "&sharedLongDesc;">

<!ENTITY netTimeout.title "Kết nối bị hết thời gian chờ">
<!ENTITY netTimeout.longDesc "&sharedLongDesc;">

<!ENTITY unknownProtocolFound.title "Chương trình không hiểu địa chỉ này">
<!ENTITY unknownProtocolFound.longDesc "<ul> <li>Có lẽ bạn cần phải cài đặt phần mềm khác mới mở được.</li> </ul>">

<!ENTITY proxyConnectFailure.title "Máy chủ proxy từ chối kết nối">
<!ENTITY proxyConnectFailure.longDesc "<ul> <li>Kiểm tra thiết lập proxy để chắc chắn rằng mọi thứ đều đúng.</li> <li>Liên hệ với quản trị mạng của bạn để chắc chắn rằng máy chủ proxy  vẫn đang hoạt động.</li> </ul>">

<!ENTITY proxyResolveFailure.title "Không tìm thấy máy chủ proxy">
<!ENTITY proxyResolveFailure.longDesc "<ul> <li>Kiểm tra thiết lập proxy.</li> <li>Kiểm tra kết nối mạng.</li> <li>Nếu máy tính hoặc mạng được bảo vệ bởi tường lửa hoặc proxy, hãy chắc chắn rằng &brandShortName; được phép truy cập Web.</li> </ul>">

<!ENTITY redirectLoop.title "Trang này không chuyển hướng đúng cách">
<!ENTITY redirectLoop.longDesc "<ul> <li>Vấn đề này thỉnh thoảng có thể xảy ra do bạn vô hiệu hóa hoặc từ chối cookie.</li> </ul>">

<!ENTITY unknownSocketType.title "Nhận được phản hồi lạ từ máy chủ">
<!ENTITY unknownSocketType.longDesc "<ul> <li>Kiểm tra để chắc chắn rằng hệ thống của bạn có Trình quản lí Bảo mật Cá nhân đã được cài đặt.</li> <li>Điều này có thể là do cấu hình không chuẩn trên máy chủ.</li> </ul>">

<!ENTITY nssFailure2.title "Không thể kết nối an toàn">
<!ENTITY nssFailure2.longDesc2 "<ul><li>Không thể hiển thị trang bạn muốn xem vì không thể kiểm tra tính xác thực của dữ liệu nhận được.</li><li>Vui lòng liên hệ chủ trang web để báo với họ vấn đề này.</li></ul>">

<!ENTITY certerror.longpagetitle1 "Kết nối của bạn không an toàn">
<!-- Localization note (certerror.introPara) - The text content of the span tag
will be replaced at runtime with the name of the server to which the user
was trying to connect. -->
<!ENTITY certerror.introPara "Người chủ của <span class='hostname'/> đã cấu hình trang của họ không đúng.  Để tránh mất cắp thông tin của bạn, &brandShortName; đã không kết nối tới trang này.">

<!ENTITY sharedLongDesc "<ul> <li>Trang web này có thể bị gián đoạn tạm thời hoặc do quá tải. Hãy thử lại trong chốc lát.</li> <li>Nếu bạn không thể mở bất kì trang nào, hãy kiểm tra kết nối mạng.</li> <li>Nếu máy tính hoặc mạng của bạn được bảo vệ bởi tường lửa hoặc proxy, hãy chắc chắn rằng &brandShortName; được phép truy cập Web.</li> </ul>">

<!ENTITY cspBlocked.title "Bị chặn bởi Chính sách bảo mật nội dung">
<!ENTITY cspBlocked.longDesc "<p>&brandShortName; không tải trang này vì nó có một chính sách bảo vệ nội dung không cho phép việc tải theo cách này.</p>">

<!ENTITY corruptedContentErrorv2.title "Lỗi nội dung không toàn vẹn">
<!ENTITY corruptedContentErrorv2.longDesc "<p>Không thể hiển thị được trang mà bạn muốn xem vì có lỗi trong truyền tải dữ liệu.</p><ul><li>Vui lòng liên hệ chủ trang web để báo họ về vấn đề này.</li></ul>">


<!ENTITY securityOverride.exceptionButtonLabel "Thêm ngoại Lệ…">

<!ENTITY errorReporting.automatic2 "Báo cáo những lỗi như thế này để giúp Mozilla nhận diện và chặn những trang độc hại">
<!ENTITY errorReporting.learnMore "Tìm hiểu thêm…">

<!ENTITY remoteXUL.title "Remote XUL">
<!ENTITY remoteXUL.longDesc "<p><ul><li>Vui lòng liên hệ với chủ trang web để báo với họ vấn đề này.</li></ul></p>">

<!ENTITY sslv3Used.title "Không thể kết nối một cách an toàn">
<!-- LOCALIZATION NOTE (sslv3Used.longDesc2) - Do not translate
     "SSL_ERROR_UNSUPPORTED_VERSION". -->
<!ENTITY sslv3Used.longDesc2 "Thông tin bổ sung: SSL_ERROR_UNSUPPORTED_VERSION">

<!-- LOCALIZATION NOTE (certerror.wrongSystemTime2,
                        certerror.wrongSystemTimeWithoutReference) - The <span id='..' />
     tags will be injected with actual values, please leave them unchanged. -->
<!ENTITY certerror.wrongSystemTime2 "<p> &brandShortName; không thể kết nối đến <span id='wrongSystemTime_URL'/> bởi vì đồng hồ máy tính của bạn dường như hiển thị sai thời gian và điều này chặn một kết nối an toàn.</p> <p>Máy tính của bạn đang được cài đặt là <span id='wrongSystemTime_systemDate'/>, trong khi thời gian thực tế là <span id='wrongSystemTime_actualDate'/>. Để khắc phục sự cố này, thay đổi lại đồng hồ của bạn về thời gian chính xác.</p>">
<!ENTITY certerror.wrongSystemTimeWithoutReference "<p>&brandShortName; không thẻ kết nối đến <span id='wrongSystemTimeWithoutReference_URL'/> vì đồng hồ máy tính của bạn có vẻ như hiển thị sai thời gian và điều này ngăn chặn kết nối an toàn. </p> <p>Máy tính của bạn đang cài đặt là <span id='wrongSystemTimeWithoutReference_systemDate'/>. Để khắc phục, hãy thay đổi cài đặt ngày giờ cho đúng.</p>">

<!ENTITY certerror.pagetitle1  "Kết nối không bảo mật">
<!ENTITY certerror.whatShouldIDo.badStsCertExplanation "Trang này dùng HTTP Strict Transport Security (HSTS) để chỉ định &brandShortName; chỉ được kết nối với nó một cách bảo mật. Cho nên, không khả thi để thêm ngoại lệ cho chứng chỉ này.">
<!ENTITY certerror.copyToClipboard.label "Chép văn bản vào bảng ghi tạm">

<!ENTITY inadequateSecurityError.title "Kết nối của bạn không an toàn">
<!-- LOCALIZATION NOTE (inadequateSecurityError.longDesc) - Do not translate
     "NS_ERROR_NET_INADEQUATE_SECURITY". -->
<!ENTITY inadequateSecurityError.longDesc "<p><span class='hostname'></span> sử dụng công nghệ bảo mật lỗi thời và dễ bị tấn công. Một kẻ tấn công có thể dễ dàng làm lộ những thông tin mà bạn nghĩ là an toàn. Người quản lý trang web trước tiên sẽ cần phải sửa lỗi máy chủ trước khi bạn có thể vào trang.</p><p>Mã lỗi: NS_ERROR_NET_INADEQUATE_SECURITY</p>">

<!ENTITY prefReset.longDesc "Dường như là cài đặt bảo mật mạng của bạn có thể gây ra điều này. Bạn có muốn khôi phục cài đặt mặc định?">
<!ENTITY prefReset.label "Khôi phục cài đặt mặc định">
