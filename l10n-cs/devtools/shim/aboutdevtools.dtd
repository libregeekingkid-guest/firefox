<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "O nástrojích pro vývojáře">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Povolit nástroje pro vývojáře">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Povolit nástroje pro vývojáře a prozkoumat prvek">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Zkoumejte a upravovujte HTML a CSS pomocí průzkumníku v nástrojích pro vývojáře.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "S nástroji pro vývojáře můžete ve Firefoxu vyvíjet a ladit doplňky, WebExtensions, web a service workers a další.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Aktivovali jste zkratku pro otevření vývojářských nástrojů. Pokud to bylo nechtěně, můžete tento panel zavřít.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Zkoumejte a upravovujte HTML, CSS a JavaScript pomocí nástrojů pro vývojáře jako průzkumník nebo debugger.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Vypilujte HTML, CSS a JavaScript na svých webových stránkách pomocí nástrojů pro vývojáře jako průzkumník nebo debugger.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Vývojářské nástroje jsou ve výchozím stavu ve Firefoxu vypnuty pro vaši větší kontrolu nad funkcemi prohlížeče.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Zjistit více o nástrojích pro vývojáře">

<!ENTITY  aboutDevtools.enable.enableButton "Povolit nástroje pro vývojáře">
<!ENTITY  aboutDevtools.enable.closeButton "Zavřít tuto stránku">

<!ENTITY  aboutDevtools.enable.closeButton2 "Zavřít tento panel">

<!ENTITY  aboutDevtools.welcome.title "Vítají vás nástroje pro vývojáře!">

<!ENTITY  aboutDevtools.newsletter.title "Zpravodaj Mozilly pro vývojáře">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Nechte si posílat vývojářské novinky, triky a zdroje přímo do vaší schránky.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "E-mail">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Souhlasím s tím, aby Mozilla zpracovávala mé informace tak, jak je popsáno v <a class='external' href='https://www.mozilla.org/privacy/'>Zásadách ochrany soukromí</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Odebírat">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Děkujeme!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Pokud jste dříve nepotvrdili odběr zpravodaje Mozilly, budete tak muset učinit nyní. Podívejte se prosím do vaší e-mailové schránky a případně i nevyžádané pošty, jestli tam není e-mail od nás.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Hledáte více než jenom nástroje pro vývojáře? Vyzkoušejte verzi Firefoxu speciálně pro vývojáře a jejich pracovní postupy.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Zjistit více">

