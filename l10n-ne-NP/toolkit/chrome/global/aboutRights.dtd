<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
<!-- rights.locale-direction instead of the usual local.dir entity, so RTL can skip translating page. -->
<!ENTITY rights.locale-direction "ltr‌">
<!ENTITY rights.title "तपाईँको अधिकारहरूका बारेमा">
<!ENTITY rights.intro "&brandFullName; एक खुल्ला तथा स्वतन्त्र स्रोत सफ्टवेयर हो, जुन दुनियाँ भरका हजारौं समुदाय द्वारा निर्मित छ। यहाँ तपाईँले थाहा हुनुपर्छ केही कुराहरू छन्:">
<!-- Note on pointa / pointb / pointc form:
     These points each have an embedded link in the HTML, so each point is
     split into chunks for text before the link, the link text, and the text
     after the link. If a localized grammar doesn't need the before or after
     chunk, it can be left blank.

     Also note the leading/trailing whitespace in strings here, which is
     deliberate for formatting around the embedded links. -->

<!ENTITY rights.intro-point1a "&brandShortName; यी शर्तहरू अन्तर्गत उपलब्ध छ ">
<!ENTITY rights.intro-point1b "Mozilla Public License">
<!ENTITY rights.intro-point1c "। यसको अर्थ, तपाईँले &brandShortName; प्रयोग गर्नुको साथै, प्रतिलिपि बनाउन र अरूलाई बाँड्न पनि सक्नुहुन्छ।  &brandShortName; को स्रोत कोडलाई अाफ्नो चाहना अनुरूप बदल्न पनि तपाईँलाई स्वागत छ।Mozilla Public License तपाईँको बदलिएको संस्करणहरू बाँड्ने अधिकार पनि तपाईँलाई दिएको छ।">

<!ENTITY rights.intro-point2-a "तपाईँ Mozilla Foundation वा कुनै पनि पक्षाको ट्रेडमार्क अधिकार वा लाइसेन्स प्रदान गरिएको छैन। साथै तपाईँलाई Firefox को नाम र लोगो पनि प्रयोगमा पनि असमित अधिकार छैन। ‌ट्रेडमार्क सम्बन्धी थप जानकारी पाउन सकिन्छ ">
<!ENTITY rights.intro-point2-b "यहाँ">
<!ENTITY rights.intro-point2-c ".">
<!-- point 2.5 text for official branded builds -->

<!ENTITY rights.intro-point2.5 "&brandShortName; को केहि मा सुविधाहरू जस्तै क्र्यास रिपोर्टरले &vendorShortName; लाई पृष्ठपोषण पठाउने विकल्प तपाईँलाई दिन्छन्। पृष्ठपोषण निवेदन छानेर, तपाईँले &vendorShortName; लाई पृष्ठपोषण प्रयोग गरेर उत्पादन सुधार्ने, वेबसाइटमा पृष्ठपोषण प्रकाशन गर्ने र पृष्ठपोषण वितरण गर्ने अनुमति दिनसक्नुहुन्छ ।">
<!-- point 3 text for official branded builds -->

<!ENTITY rights2.intro-point3a "&brandShortName; मार्फत &vendorShortName;मा पेस गरिएको तपाईँको व्यक्तिगत जानकारी र प्रतिक्रिया हामी कसरी प्रयोग गर्छौँ भन्ने कुराको विस्तृत जानकारी ">
<!ENTITY rights2.intro-point3b "&brandShortName; गोपनीयता निति">
<!ENTITY rights.intro-point3c ".">
<!-- point 3 text for unbranded builds -->

<!ENTITY rights.intro-point3-unbranded "यो उत्पादन लागि कुनै पनि गोपनीयता नीति यहाँ सूचीबद्ध गर्नुपर्छ।">
<!-- point 4 text for official branded builds -->

<!ENTITY rights2.intro-point4a "केही &brandShortName; का विशषताहरूले वेब अाधारित सेवाहरूको प्रयोग गर्छन्, तर तिनिहरू 100&#037; सही वा त्रुटी मुक्त छन् भनेर हामी निश्चित छैनौँ। यो सेवाहरू प्रयोग गर्ने विशषताहरूलाई बन्द गर्ने लगायत थप जानकारी यहाँ प्राप्त हुनेछ ">
<!ENTITY rights.intro-point4b "सेवाका सर्तहरू">
<!ENTITY rights.intro-point4c ".">
<!-- point 4 text for unbranded builds -->

<!ENTITY rights.intro-point4a-unbranded "यदि यस उत्पादनले वेब सेवाहरू समेवेश गरेमा, कुनै सेवा(हरु) लाई लागू हुने सेवा सर्तहरू यहाँ जोडिनु पर्छ ">
<!ENTITY rights.intro-point4b-unbranded "Web Site Services">
<!ENTITY rights.intro-point4c-unbranded " खण्ड।">

<!ENTITY rights2.webservices-header "&brandFullName; वेबमा आधारित जानकारी सेवाहरू">
<!-- point 5 -->

<!ENTITY rights.intro-point5 "केहि प्रकारका चलदृश्य सामग्री पुन: बजाउन &brandShortName; ले केहि सामाग्री गुप्तिकरण उल्टाउने मोड्युलहरू (content decryption modules) तेश्रो पक्ष बाट डाउनलोड गर्छ।">
<!-- Note that this paragraph references a couple of entities from
     preferences/security.dtd, so that we can refer to text the user sees in
     the UI, without this page being forgotten every time those strings are
     updated.  -->
<!-- intro paragraph for branded builds -->

<!ENTITY rights2.webservices-a "&brandShortName; को बाइनरी संस्करणले तल वर्णन गरिएका सर्तहरू लागु हुने गरि तपाईँको प्रयोग को लागि उपलब्ध गराएको केहि विशेषता प्रयोग गर्नको लागि &brandFullName; ले वेब आधारित जानकारी सेवाहरू (&quot;सेवाहरू&quot;) प्रयोग गर्छ। यदि तपाई एक वा बढी सेवाहरू प्रयोग गर्न चाहनुहुन्न वा तलका सर्तहरू अस्वीकार्य छन्, भने तपाईँ सुविधा वा सेवा (s) लाई असक्षम गर्न सक्नुहुन्छ। एक विशेष सुविधा वा सेवा कसरी असक्षम गर्नेबारे निर्देशन पाउन सकिन्छ ">
<!ENTITY rights2.webservices-b "यहाँ">
<!ENTITY rights3.webservices-c "। अन्य सेवा र सुविधा अनुप्रयोगको प्राथमिकताहरूबाट अक्षम गर्न सकिन्छ।‌">
<!-- safe browsing points for branded builds -->

<!ENTITY rights.safebrowsing-a "सुरक्षित ब्राउजिङ्ग: ">
<!ENTITY rights.safebrowsing-b "सुरक्षित ब्राउजिङ विशेषता असक्षम गर्न सिफारिस गरिएको छैन किनकी त्यसले तपाईँलाई असुरक्षित साइटहरू तिर लान सक्छ।  यदि तपाईँ विशेषता पूर्ण रुपमा अक्षम गर्न चाहनुहुन्छ भने, यी चरणहरू पछ्याउनुहोस्:">
<!ENTITY rights.safebrowsing-term1 "अनुप्रयोग प्राथमिकताहरू खोल्नुहोस्">
<!ENTITY rights.safebrowsing-term2 "सुरक्षा छान्नुहोस्">
<!ENTITY rights2.safebrowsing-term3 "&quot;&enableSafeBrowsing.label;&quot; विकल्प जाँच नगर्नुहोस्">
<!ENTITY rights.safebrowsing-term4 "सुरक्षित ब्राउजिङ्ग अहिले निष्कृय छ">
<!-- location aware browsing points for branded builds -->

<!ENTITY rights.locationawarebrowsing-a "स्थान सचेत ब्राउजिङ्ग: ">
<!ENTITY rights.locationawarebrowsing-b "सधैँ लागू छ।  कुनै स्थान जानकारी कहिल्यै पनि तपाईँको अनुमति बिना पठाइएको छैन।  तपाईँ विशेषता पूर्ण अक्षम गर्न चाहनुहुन्छ भने, यी चरणहरू पछ्याउनुहोस्:">
<!ENTITY rights.locationawarebrowsing-term1a "URL बारमा लेख्नुहोस् ">
<!ENTITY rights.locationawarebrowsing-term1b "about:config‌">
<!ENTITY rights.locationawarebrowsing-term2 "geo.enabled टाइप गर्नुहोस्">
<!ENTITY rights.locationawarebrowsing-term3 "geo.enabled प्राथमिकतामा दुई पटक क्लिक गर्नुहोस्">
<!ENTITY rights.locationawarebrowsing-term4 "स्थान-सचेत ब्राउजिङ्ग अहिले अक्षम छ">
<!-- intro paragraph for unbranded builds -->

<!ENTITY rights.webservices-unbranded "An overview of the web site services the product incorporates, along with instructions on how to disable them, if applicable, should be included here.">
<!-- point 1 text for unbranded builds -->

<!ENTITY rights.webservices-term1-unbranded "यो उत्पादनको लागि कुनै पनि गोपनीयता नीति यहाँ सूचीबद्ध गर्नुपर्छ।">
<!-- points 1-7 text for branded builds -->

<!ENTITY rights2.webservices-term1 "&vendorShortName; र यसको योगकर्ता, अनुमति पत्र प्रदायक र साझेदारहरू सबैभन्दा सही र अद्यावधिक सेवाहरू प्रदान गर्न काम गर्छन्। तर, हामी यो जानकारी व्यापक र त्रुटी-रहित छ भन्ने ग्यारेन्टी गर्न सक्दैनौ।  उदाहरणका लागि, सुरक्षित ब्राउजिङ सेवा ले केही जोखिमपूर्ण साइटहरू पहिचान नगर्न सक्छ र त्रुटीले केही सुरक्षित साइट लाई असुरक्षित पहिचान गर्न सक्छ र स्थान जागरूक सेवा, हाम्रो सेवा प्रदायक ले फर्काउने सबै स्थानहरू अनुमान मात्र हो र न हामी न त हाम्रो सेवा प्रदायकले प्रदान गरिएका स्थानहरूको सटीकता ग्यारेन्टी गर्छ।">
<!ENTITY rights.webservices-term2 "&vendorShortName; ले आफ्नै विवेकमा सेवाहरू परिवर्तन गर्न वा टुटाउन सक्छ।">
<!ENTITY rights2.webservices-term3 "तपाईँलाई यी &brandShortName; को संस्करणमा रहेका सेवाहरू प्रयोग गर्न स्वागत छ, र &vendorShortName; ले तपाईँलाई त्यसो गर्न आफ्ना अधिकारहरू प्रदान गर्छ,  &vendorShortName; र यस्का अनुमतिपत्र दाताले ती सेवाहरूमा पर्ने अन्य सम्पुर्ण अधिकारहरू आरक्षित गर्नुहुन्छ।  यी सर्तहरू &brandShortName; र &brandShortName; को अनुरूपी स्रोत कोड संस्करण लाई लागु हुने खुला स्रोत लाइसेन्स अन्तर्गत प्रदान, कुनै पनि अधिकारहरू सीमित गर्न अभिप्रेरित छैन।">
<!ENTITY rights.webservices-term4 "सेवाहरू &quot;जस्ताको-त्यस्तै&quot; प्रदान गरिन्छ।  &vendorShortName;, यसका योगदाताहरू, अनुमतिपत्र दाताहरू, र वितरकहरू, सम्पूर्ण वारेन्टी नामन्जुर गर्नुहुन्छ चाहे ती जाहेरी वा निहित हुन्, सीमा बिनाका पनि, यस्ता वारेन्टीहरूमा सेवाहरू तपाईँका विशेष उद्देश्यहरूका लागि व्यापारीकरण र उपयुक्त हुन्छन्।  तपाईँ आफ्नो उद्देश्यको लागि सेवा चयन गर्न अनि सेवाको गुणस्तर र प्रदर्शनका लागि सम्पूर्ण जोखिम वहन गर्नुहुन्छ। केही न्यायालयहरू निहित वारेन्टीहरूको बहिष्कार वा परिसीमन गर्न अनुमति दिँदैनन्, तसर्थ यो अस्वीकारोक्ति तपाईँलाई लागू नहुन सक्छ।">
<!ENTITY rights.webservices-term5 "कानून द्वारा आवश्यक बाहेक, &vendorShortName;, यसका योगदाताहरू, अनुमतिपत्र दाताहरू, र वितरकहरू &brandShortName; र सेवाहरूको प्रयोग सम्बन्धी कुनै पनि अप्रत्यक्ष, विशेष, आकस्मिक, परिणामत, दण्डात्मक, वा उदाहरणीय हर्जानाका लागि उत्तरदायी हुने छैनन्, जुन् बाहिए वा कुनै पनि तरिकामा उत्पन्न हुनसक्छ।  यी सर्तहरू अन्तर्गत सामूहिक उत्तरदायित्व $500 (पाँच सय डलर) भन्दा बढी हुने छैन। केहि न्यायालयहरू केही खास हर्जानाको बहिष्करण वा परिसीमन गर्न अनुमति दिदैनन्, तसर्थ यो बहिष्करण वा परिसीमन तपाईंलाई लागू नहुन सक्छ।">
<!ENTITY rights.webservices-term6 "&vendorShortName; ले आवश्यक रूपमा समय समयमा यी सर्तहरू अद्यावधिक गर्न सक्छन। यी सर्तहरू &vendorShortName; को लिखित सम्झौता बिना परिमार्जन वा रद्द गर्न सकिदैन।">
<!ENTITY rights.webservices-term7 "यी सर्तहरू क्यालिफोर्निया, U.S.A. को विरोधाभास कानून प्रावधान बाहेक, राज्यको कानून द्वारा शासित छन्। यदि यी सर्तहरूको कुनै पनि भाग अवैध वा अप्रवर्तनीय हुन गएमा, बाँकी अंश पूर्ण शक्ति र प्रभावमा रहनेछ। यी सर्तहरूको एक अनुवादित संस्करण र अंग्रेजी भाषा संस्करण बीच द्वन्द्वको घटनामा, अंग्रेजी भाषा संस्करण नियन्त्रण गर्नेछ।">
