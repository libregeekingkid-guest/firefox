<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Note: each tab panel must contain unique accesskeys -->

<!ENTITY generalTab.label                "Հիմնական">

<!ENTITY useCursorNavigation.label       "Էջերում ուղղորդման համար միշտ օգտագործել կուրսորի սեղմակները:">
<!ENTITY useCursorNavigation.accesskey   "կ">
<!ENTITY searchOnStartTyping.label       "Տեքստ մուտքագրելու դեպքում փնտրել այն">
<!ENTITY searchOnStartTyping.accesskey   "ք">
<!ENTITY useOnScreenKeyboard.label       "Անհրաժեշտության դեպքում ցուցադրել հպաստեղնաշարը">
<!ENTITY useOnScreenKeyboard.accesskey   "հ">

<!ENTITY browsing.label                  "Դիտարկում">

<!ENTITY useAutoScroll.label             "Օգտագործել ինքնաթերթումը">
<!ENTITY useAutoScroll.accesskey         "ի">
<!ENTITY useSmoothScrolling.label        "Օգտագործել կոկիկ թերթումը">
<!ENTITY useSmoothScrolling.accesskey    "ո">
<!ENTITY checkUserSpelling.label         "Տեքստ մուտքագրելիս ստուգել ուղղագրությունը">
<!ENTITY checkUserSpelling.accesskey     "մ">

<!ENTITY dataChoicesTab.label            "Ընտրել տվյալները">

<!-- LOCALIZATION NOTE (healthReportingDisabled.label): This message is displayed above
disabled data sharing options in developer builds or builds with no Telemetry support
available. -->
<!ENTITY healthReportingDisabled.label   "Տվյալների զեկուցումը անջատված է կազմաձևի այս կառուցման համար">

<!ENTITY enableHealthReport2.label       "Թույլատրել &brandShortName;-ին ուղարկել տեխնիկական և փոխազդելու տվյալներ Mozilla-ին">
<!ENTITY enableHealthReport2.accesskey   "">
<!ENTITY healthReportLearnMore.label     "Իմանալ ավելին">

<!ENTITY dataCollection.label            "&brandShortName;-ի տվյալների հավաքում և օգտագործում">
<!ENTITY dataCollectionDesc.label        "Մենք փորձում ենք տրամադրել ձեզ ընտրություն և հավաքել միայն այն ժամանակ, երբ մեզ պետք է տրամադրել և լավարկել &brandShortName;-ը բոլորի համար: ՄԵնք միշտ հարցնում ենք թույլտվությյուն՝ մինչև անձնական տեղեկություններ ստանալը:">
<!ENTITY dataCollectionPrivacyNotice.label    "Գաղտնիության ծանուցում">

<!ENTITY collectBrowserErrors.label          "Թույլատրել &brandShortName;-ին ուղարկել դիտարկիչի սխալի զեկույցներ (այդ թվում՝ սխալի հաղորդագրությունները) Mozilla-ին">
<!ENTITY collectBrowserErrors.accesskey      "b">
<!ENTITY collectBrowserErrorsLearnMore.label "Իմանալ ավելին">

<!ENTITY alwaysSubmitCrashReports1.label  "Թույլատրել &brandShortName;-ին ուղարկել վթարի զեկույցներ Mozilla-ին">
<!ENTITY alwaysSubmitCrashReports1.accesskey "c">
<!ENTITY crashReporterLearnMore.label    "Իմանալ ավելին">

<!ENTITY networkTab.label                "Ցանցը">

<!ENTITY networkProxy.label              "Ցանցի պրոքսի">

<!ENTITY connectionDesc.label            "Կարգավորեք համացանցին միանալու &brandShortName;-ի ցուցիչները">

<!ENTITY connectionSettingsLearnMore.label "Իմանալ ավելին">
<!ENTITY connectionSettings.label        "Կարգավորել...">
<!ENTITY connectionSettings.accesskey    "ա">

<!ENTITY httpCache.label                 "Կուտակել ցանցային բովանդակությունը">

<!--  Site Data section manages sites using Storage API and is under Network -->
<!ENTITY siteData.label                  "Կայքի տվյալը">
<!ENTITY clearSiteData.label             "Մաքրել բոլոր տվյալները">
<!ENTITY clearSiteData.accesskey         "ա">
<!ENTITY siteDataSettings.label          "Կարգավորումներ...">
<!ENTITY siteDataSettings.accesskey      "վ">
<!ENTITY siteDataLearnMoreLink.label     "Իմանալ ավելին">

<!-- LOCALIZATION NOTE:
  The entities limitCacheSizeBefore.label and limitCacheSizeAfter.label appear on a single
  line in preferences as follows:

  &limitCacheSizeBefore.label [textbox for cache size in MB] &limitCacheSizeAfter.label;
-->
<!ENTITY limitCacheSizeBefore.label      "Սահմանափակել պահոցը՝">
<!ENTITY limitCacheSizeBefore.accesskey  "Ս">
<!ENTITY limitCacheSizeAfter.label       "ՄԲ">
<!ENTITY clearCacheNow.label             "Մաքրել հիմա">
<!ENTITY clearCacheNow.accesskey         "Մ">
<!ENTITY overrideSmartCacheSize.label    "Վերակարգավորել պահոցի կառավարումը">
<!ENTITY overrideSmartCacheSize.accesskey "Վ">

<!ENTITY updateTab.label                 "Թարմացնել">

<!-- LOCALIZATION NOTE (updateApplication.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApplication.label         "&brandShortName;-ի թարմացումներ">
<!-- LOCALIZATION NOTE (updateApplication.version.*): updateApplication.version.pre
# is followed by a version number, keep the trailing space or replace it with a
# different character as needed. updateApplication.version.post is displayed
# after the version number, and is empty on purpose for English. You can use it
# if required by your language.
 -->
<!ENTITY updateApplicationDescription.label
                                         "Պահեք &brandShortName;-ը թարմացված՝ լավագույն արտադրողականության, կայունության և անվտանգության համար:">
<!ENTITY updateApplication.version.pre   "Տարբերակ ">
<!ENTITY updateApplication.version.post  "">
<!ENTITY updateApplication.description   "Թույլատրել &brandShortName;-ին">
<!ENTITY updateAuto3.label               "Ինքնաբար տեղադրել թարմացումները (հանձնարարելի)">
<!ENTITY updateAuto3.accesskey           "A">
<!ENTITY updateCheckChoose2.label        "Ստուգել թարմացումները, բայց ես կընտրեմ, թե որոնք տեղադրել">
<!ENTITY updateCheckChoose2.accesskey    "Ս">
<!ENTITY updateManual2.label             "Երբեք չստուգել թարմացումները (խորհուրդ չի տրվում)">
<!ENTITY updateManual2.accesskey         "Ե">

<!ENTITY updateHistory2.label            "Ցուցադրել Թարմացումների Պատմությունը...">
<!ENTITY updateHistory2.accesskey        "ա">

<!ENTITY useService.label                "Թարմացումները տեղադրելիս օգտվել խորքային ծառայությունից">
<!ENTITY useService.accesskey            "խ">

<!ENTITY enableSearchUpdate2.label       "Ինքնաբար թարմացնել որոնիչները">
<!ENTITY enableSearchUpdate2.accesskey   "լ">

<!ENTITY certificateTab.label            "Վկայագրեր">
<!ENTITY certPersonal2.description       "Երբ սպասարկիչը հարցնում է ձեր անձնական վկայագիրը">
<!ENTITY selectCerts.auto                "Ընտրել ինքնաշխատ">
<!ENTITY selectCerts.auto.accesskey      "S">
<!ENTITY selectCerts.ask                 "Ամեն անգամ հարցնել">
<!ENTITY selectCerts.ask.accesskey       "A">
<!ENTITY enableOCSP.label                "Հարցում OCSP պատասխանիչի սպասարկիչին՝ հաստատելու հավաստագրի իսկությունը">
<!ENTITY enableOCSP.accesskey            "Հ">
<!ENTITY viewCerts2.label                "Դիտել վկայագրերը...">
<!ENTITY viewCerts2.accesskey            "C">
<!ENTITY viewSecurityDevices2.label      "Անվտանգության սարքեր...">
<!ENTITY viewSecurityDevices2.accesskey  "D">

<!ENTITY performance.label               "Արտադրողականություն">
<!ENTITY useRecommendedPerformanceSettings2.label
                                         "Օգտ. արտադրողականության հանձնարարելի կարգավորումները">
<!ENTITY useRecommendedPerformanceSettings2.description
                                         "Այս կարգավորումները հարմարեցված են ձեր համակարգչի սարքաշարին և օպերացիոն համակարգին:">
<!ENTITY useRecommendedPerformanceSettings2.accesskey
                                         "Օ">
<!ENTITY performanceSettingsLearnMore.label
                                         "Իմանալ ավելին">
<!ENTITY limitContentProcessOption.label "Բովանդակության ընթացքի սահմանափակում">
<!ENTITY limitContentProcessOption.description
                                         "Բովանդակության լրացուցիչ ընթացքները կարող են լավարկել արտադրողականությունը, երբ օգտագործվում են բազմակի ներդիրներ, բայց միևնույն ժամանակ՝ դրանք ավելի շատ հիշողություն կխլեն:">
<!ENTITY limitContentProcessOption.accesskey   "ս">
<!ENTITY limitContentProcessOption.disabledDescription
                                         "Հնարավոր է փոփոխել բովանդակության ընթացքները միայն բազմամշակիչ &brandShortName;-ի դեպքում:">
<!ENTITY limitContentProcessOption.disabledDescriptionLink
                                         "Կարդալ, թե ինչպես ստուգել՝ արդյոք բազմամշակիչը միացված է:">
<!ENTITY allowHWAccel.label              "Հնարավորության դեպքում օգտագործել սարքակազմի արագացումը ">
<!ENTITY allowHWAccel.accesskey          "ր">
