# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = Ara el %S desa les adreces per tal que pugueu emplenar formularis més ràpid.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Opcions d'emplenament automàtic de formularis
autofillOptionsLinkOSX = Preferències d'emplenament automàtic de formularis
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Opcions d'emplenament automàtic de formularis i seguretat
autofillSecurityOptionsLinkOSX = Preferències d'emplenament automàtic de formularis i seguretat
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Canvia les opcions d'emplenament automàtic de formularis
changeAutofillOptionsOSX = Canvia les preferències d'emplenament automàtic de formularis
changeAutofillOptionsAccessKey = C
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Comparteix les adreces amb els dispositius sincronitzats
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Comparteix les targetes de crèdit amb els dispositius sincronitzats
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Voleu actualitzar l'adreça amb aquesta informació nova?
updateAddressDescriptionLabel = Adreça que s'actualitzarà:
createAddressLabel = Crea una adreça nova
createAddressAccessKey = C
updateAddressLabel = Actualitza l'adreça
updateAddressAccessKey = u
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Voleu que el %S desi aquesta targeta de crèdit? (El codi de seguretat no es desarà)
saveCreditCardDescriptionLabel = Targeta de crèdit que es desarà:
saveCreditCardLabel = Desa la targeta de crèdit
saveCreditCardAccessKey = D
cancelCreditCardLabel = No la desis
cancelCreditCardAccessKey = N
neverSaveCreditCardLabel = No desis mai les targetes de crèdit
neverSaveCreditCardAccessKey = N
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Voleu actualitzar la targeta de crèdit amb aquesta informació nova?
updateCreditCardDescriptionLabel = Targeta de crèdit que s'actualitzarà:
createCreditCardLabel = Crea una targeta de crèdit nova
createCreditCardAccessKey = C
updateCreditCardLabel = Actualitza la targeta de crèdit
updateCreditCardAccessKey = u
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Obre la subfinestra de missatges d'emplenament automàtic de formularis

# LOCALIZATION NOTE (autocompleteFooterOption, autocompleteFooterOptionOSX): Used as a label for the button,
# displayed at the bottom of the drop down suggestion, to open Form Autofill browser preferences.
autocompleteFooterOption = Opcions d'emplenament automàtic de formularis
autocompleteFooterOptionOSX = Preferències d'emplenament automàtic de formularis

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Més opcions
autocompleteFooterOptionOSXShort = Preferències
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = adreça
category.name = nom
category.organization2 = organització
category.tel = telèfon
category.email = correu electrònic
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = També emplena automàticament %S
phishingWarningMessage2 = Emplena automàticament %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = El %S ha detectat un lloc insegur. L'emplenament automàtic de formularis es desactivarà temporalment.

# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Esborra el formulari emplenat automàticament

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Emplena automàticament les adreces
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Més informació
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Adreces desades…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Emplena automàticament les targetes de crèdit
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Targetes de crèdit desades…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Adreces desades
manageCreditCardsTitle = Targetes de crèdit desades
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Adreces
creditCardsListHeader = Targetes de crèdit
showCreditCardsBtnLabel = Mostra les targetes de crèdit
hideCreditCardsBtnLabel = Amaga les targetes de crèdit
removeBtnLabel = Elimina
addBtnLabel = Afegeix…
editBtnLabel = Edita…

# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Afegeix una adreça nova
editAddressTitle = Edita l'adreça
givenName = Nom
additionalName = Segon nom
familyName = Cognoms
organization2 = Organització
streetAddress = Adreça postal
city = Població
province = Província
state = Estat
postalCode = Codi postal
zip = Codi postal
country = País o regió
tel = Telèfon
email = Adreça electrònica
cancelBtnLabel = Cancel·la
saveBtnLabel = Desa
countryWarningMessage = Actualment l'emplenament automàtic de formularis només està disponible per a les adreces dels EUA

countryWarningMessage2 = Actualment l'emplenament automàtic de formularis només està disponible per a alguns països.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Afegeix una targeta de crèdit
editCreditCardTitle = Edita la targeta de crèdit
cardNumber = Número de targeta
nameOnCard = Nom del titular
cardExpires = Venciment
