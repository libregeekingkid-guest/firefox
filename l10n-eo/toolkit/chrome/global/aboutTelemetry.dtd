<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "
Fonto de datumoj de «ping»:
">
<!ENTITY aboutTelemetry.showCurrentPingData "
Nunaj datumoj de «ping»
">
<!ENTITY aboutTelemetry.showArchivedPingData "
Arkĥivo de datumoj de «ping»
">
<!ENTITY aboutTelemetry.showSubsessionData "
Montri datumojn de subseancoj
">
<!ENTITY aboutTelemetry.choosePing "
Elekti «ping»:
">
<!ENTITY aboutTelemetry.archivePingType "Tipo de «ping»">
<!ENTITY aboutTelemetry.archivePingHeader "
«Ping»
">
<!ENTITY aboutTelemetry.optionGroupToday "
Hodiaŭ
">
<!ENTITY aboutTelemetry.optionGroupYesterday "
Hieraŭ
">
<!ENTITY aboutTelemetry.optionGroupOlder "
Antaŭe
">
<!ENTITY aboutTelemetry.payloadChoiceHeader "
  Utilŝarĝo
">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Datumoj de telemezuro">
<!ENTITY aboutTelemetry.moreInformations "Ĉu vi serĉas pli da informo?">
<!ENTITY aboutTelemetry.firefoxDataDoc "
La <a>dokumentaro pri datumoj de Firefox</a> enhavas gvidilojn pri la maniero labori per niaj datumaj iloj.
">
<!ENTITY aboutTelemetry.telemetryClientDoc "
La <a>dokumentaro pri klientoj de telemezuro de Firefox</a> enhavas difinojn por konceptoj, dokumentaron de API kaj datumaj referencoj.
">
<!ENTITY aboutTelemetry.telemetryDashboard "
La <a>paneloj de telemezuro</a> permesas al vi vidi la datumojn, kiujn Mozilla ricevas pere de telemezuro.
">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "Malfermi per la vidigilo de JSON">

<!ENTITY aboutTelemetry.homeSection "Komenco">
<!ENTITY aboutTelemetry.generalDataSection "
  Ĝeneralaj datumoj
">
<!ENTITY aboutTelemetry.environmentDataSection "
  Datumoj de ĉirkaŭaĵo
">
<!ENTITY aboutTelemetry.sessionInfoSection "
  Informo pri seanco
">
<!ENTITY aboutTelemetry.scalarsSection "
  Skalaroj
">
<!ENTITY aboutTelemetry.keyedScalarsSection "
  Skalaroj kun ŝlosilo
">
<!ENTITY aboutTelemetry.histogramsSection "
  Grafikaĵoj
">
<!ENTITY aboutTelemetry.keyedHistogramsSection "
  Indeksitaj histogramoj
">
<!ENTITY aboutTelemetry.eventsSection "
  Eventoj
">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "
  Simplaj mezuroj
">
<!ENTITY aboutTelemetry.telemetryLogSection "
  Registro de telemezuro
">
<!ENTITY aboutTelemetry.slowSqlSection "
  Montri instrukciojn de SQL
">
<!ENTITY aboutTelemetry.chromeHangsSection "
  Paneoj de retumilo
">
<!ENTITY aboutTelemetry.addonDetailsSection "
  Detaloj de aldonaĵo
">
<!ENTITY aboutTelemetry.capturedStacksSection "
  Kaptitaj stakoj
">
<!ENTITY aboutTelemetry.lateWritesSection "
  Malfruaj skriboj
">
<!ENTITY aboutTelemetry.rawPayloadSection "Kruda utilŝarĝo">
<!ENTITY aboutTelemetry.raw "
Kruda JSON
">

<!ENTITY aboutTelemetry.fullSqlWarning "
  NOTE: Slow SQL debugging is enabled. Full SQL strings may be displayed below but they will not be submitted to Telemetry.
">
<!ENTITY aboutTelemetry.fetchStackSymbols "
  Sendi nomojn de funkcioj por stakoj
">
<!ENTITY aboutTelemetry.hideStackSymbols "
  Montri krudajn datumojn de stako
">
