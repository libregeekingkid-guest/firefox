<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Pripojiť sa k službe &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Ak chcete aktivovať nové zariadenie, zvoľte na danom zariadení voľbu “Nastaviť službu &syncBrand.shortName.label;”.'>
<!ENTITY sync.subtitle.pair.label 'Na jeho aktiváciu zvoľte položku “Spárovať zariadenie”.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Nemám zariadenie pri sebe…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Prihlasovacie údaje'>
<!ENTITY sync.configure.engines.title.history 'Históriu'>
<!ENTITY sync.configure.engines.title.tabs 'Karty'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; (&formatS2;)'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Ponuka Záložky'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Značky'>
<!ENTITY bookmarks.folder.toolbar.label 'Panel nástrojov Záložky'>
<!ENTITY bookmarks.folder.other.label 'Ostatné záložky'>
<!ENTITY bookmarks.folder.desktop.label 'Záložky z plnej verzie'>
<!ENTITY bookmarks.folder.mobile.label 'Záložky z mobilnej verzie'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Pripnuté'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Naspäť na prehliadanie'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Víta Vás &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Prihláste sa a môžete synchronizovať svoje karty, záložky, prihlasovacie údaje a ďalšie.'>
<!ENTITY fxaccount_getting_started_get_started 'Poďme na to'>
<!ENTITY fxaccount_getting_started_old_firefox 'Používate staršiu verziu služby &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Server účtu'>
<!ENTITY fxaccount_status_sync_now 'Synchronizovať'>
<!ENTITY fxaccount_status_syncing2 'Synchronizuje sa…'>
<!ENTITY fxaccount_status_device_name 'Názov zariadenia'>
<!ENTITY fxaccount_status_sync_server 'Server služby Sync'>
<!ENTITY fxaccount_status_needs_verification2 'Váš účet musí byť overený. Ťuknutím znova odošlete overovací e-mail.'>
<!ENTITY fxaccount_status_needs_credentials 'Nie je možné sa pripojiť. Ťuknutím sa prihlásite.'>
<!ENTITY fxaccount_status_needs_upgrade 'Ak sa chcete prihlásiť, musíte aktualizovať svoj &brandShortName;.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled 'Služba &syncBrand.shortName.label; je nastavená, ale synchronizácia neprebieha automaticky. Prepnite voľbu “Automatická synchronizácia” v nastaveniach systému Android.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 'Služba &syncBrand.shortName.label; je nastavená, ale synchronizácia neprebieha automaticky. Prepnite voľbu “Automatická synchronizácia” v ponuke Nastavenia systému Android &gt; Kontá.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Ťuknutím sa prihlásite k svojmu novému účtu Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Zvoľte, čo chcete synchronizovať'>
<!ENTITY fxaccount_status_bookmarks 'Záložky'>
<!ENTITY fxaccount_status_history 'História'>
<!ENTITY fxaccount_status_passwords2 'Prihlasovacie údaje'>
<!ENTITY fxaccount_status_tabs 'Otvorené karty'>
<!ENTITY fxaccount_status_additional_settings 'Ďalšie nastavenia'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Synchronizovať len prostredníctvom Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Zabrániť aplikácii &brandShortName; v synchronizácii prostredníctvom mobilných dát alebo sietí s dátovými limitmi'>
<!ENTITY fxaccount_status_legal 'Právne informácie' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Podmienky používania'>
<!ENTITY fxaccount_status_linkprivacy2 'Zásady ochrany osobných údajov'>
<!ENTITY fxaccount_remove_account 'Odpojiť&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Odpojiť sa zo služby Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Údaje vášho prehliadania zostanú aj naďalej na tomto zariadení, ale nebudú sa synchronizovať s vaším účtom.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Účet Firefox pre &formatS; bol odpojený.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Odpojiť'>

<!ENTITY fxaccount_enable_debug_mode 'Zapnúť režim ladenia'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Nastavenia &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Nastavenie &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 'Služba &syncBrand.shortName.label; nie je pripojená'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Ťuknutím sa pripojíte ako &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Dokončiť aktualizáciu účtu služby &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Ťuknutím sa prihlásite ako &formatS;'>
