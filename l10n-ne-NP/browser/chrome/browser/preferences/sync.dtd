<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- The page shown when logged in... -->

<!ENTITY engine.bookmarks.label     "पुस्तकचिनोहरू">
<!ENTITY engine.bookmarks.accesskey "m">
<!ENTITY engine.tabs.label2         "ट्याबहरू खोल्नुहोस्">
<!ENTITY engine.tabs.accesskey      "T">
<!ENTITY engine.history.label       "इतिहास">
<!ENTITY engine.history.accesskey   "r">
<!ENTITY engine.logins.label        "लगइनहरू">
<!ENTITY engine.logins.accesskey    "L">
<!-- On Windows we use the term "Options" to describe settings, but
     on Linux and Mac OS X we use "Preferences" - carry that distinction
     over into this string, used as the checkbox which indicates if prefs are synced
-->
<!ENTITY engine.prefs.label         "प्राथमिकताहरू">
<!ENTITY engine.prefs.accesskey     "s">
<!ENTITY engine.addons.label        "एडअन">
<!ENTITY engine.addons.accesskey    "A">
<!ENTITY engine.addresses.label     "ठेगानाहरू">
<!ENTITY engine.addresses.accesskey "e">
<!ENTITY engine.creditcards.label   "क्रेडिट कार्डहरू">
<!ENTITY engine.creditcards.accesskey "C">

<!-- Device Settings -->
<!ENTITY fxaSyncDeviceName.label       "यन्त्रको नामः">
<!ENTITY changeSyncDeviceName2.label "उपकरणको नाम परिवर्तन गर्नुहोस्…">
<!ENTITY changeSyncDeviceName2.accesskey "h">
<!ENTITY cancelChangeSyncDeviceName.label "रद्द गर्नुहोस्">
<!ENTITY cancelChangeSyncDeviceName.accesskey "n">
<!ENTITY saveChangeSyncDeviceName.label "सङ्ग्रह गर्नुहोस्">
<!ENTITY saveChangeSyncDeviceName.accesskey "v">

<!-- Footer stuff -->
<!ENTITY prefs.tosLink.label        "सेवाको सर्तहरू">
<!ENTITY fxaPrivacyNotice.link.label "गोपनीयता नीति">

<!-- LOCALIZATION NOTE (signedInUnverified.beforename.label,
signedInUnverified.aftername.label): these two string are used respectively
before and after the account email address. Localizers can use one of them, or
both, to better adapt this sentence to their language.
-->
<!ENTITY signedInUnverified.beforename.label "">
<!ENTITY signedInUnverified.aftername.label "प्रमाणित गरिएको छैन।">

<!-- LOCALIZATION NOTE (signedInLoginFailure.beforename.label,
signedInLoginFailure.aftername.label): these two string are used respectively
before and after the account email address. Localizers can use one of them, or
both, to better adapt this sentence to their language.
-->
<!ENTITY signedInLoginFailure.beforename.label "कृपया पुनः जडान गर्न साइन-इन गर्नुहोस्">
<!ENTITY signedInLoginFailure.aftername.label "">

<!ENTITY notSignedIn.label            "तपाईंँले साइन इन गर्नुभएको छैन।">
<!ENTITY signIn.label                 "साइन इन गर्नुहोस्">
<!ENTITY signIn.accesskey             "g">
<!ENTITY profilePicture.tooltip       "प्रोफाइल तस्वीर परिवर्तन गर्नुहोस्">
<!ENTITY verifiedManage.label         "खाता व्यवस्थापन गर्नुहोस्">
<!ENTITY verifiedManage.accesskey     "o">
<!ENTITY disconnect3.label            "विच्छेद गर्नुहोस्…">
<!ENTITY disconnect3.accesskey        "D">
<!ENTITY verify.label                "इमेल रुजु गर्नुहोस">
<!ENTITY verify.accesskey            "V">
<!ENTITY forget.label                "यो इमेलको बारेमा बिर्सनुहोस्">
<!ENTITY forget.accesskey            "F">

<!ENTITY resendVerification.accesskey "d">
<!ENTITY cancelSetup.label            "सेटअप रद्द गर्नुहोस्">
<!ENTITY cancelSetup.accesskey        "P">

<!ENTITY signedOut.caption            "तपाईँको वेब अाफुसँगै लैजानुहोस्">
<!ENTITY signedOut.description        "सबै यन्त्रहरूमा आफ्नो पुस्तकचिनो, इतिहास, ट्याबहरू, गोप्यशब्दहरू, एडअनहरू, र प्राथमिकताहरू समक्रमण गर्नुहोस्।">
<!ENTITY signedOut.accountBox.title   "एक &syncBrand.fxAccount.label; संग जडान गर्नुहोस्">
<!ENTITY signedOut.accountBox.create2 "खाता छैन ? सुरू गर्नुहोस्">
<!ENTITY signedOut.accountBox.create2.accesskey "C">
<!ENTITY signedOut.accountBox.signin2 "साइन इन…">
<!ENTITY signedOut.accountBox.signin2.accesskey "I">

<!ENTITY signedIn.settings.label       "Sync सेटिङ्गहरू">
<!ENTITY signedIn.settings.description "&brandShortName; प्रयोग गरेर तपाईँको उपकरणहरूमा के समक्रमण गर्ने हो छान्नुहोस् ।">

<!-- LOCALIZATION NOTE (mobilePromo3.*): the following strings will be used to
     create a single sentence with active links.
     The resulting sentence in English is: "Download Firefox for
     Android or iOS to sync with your mobile device." -->

<!ENTITY mobilePromo3.start            "निम्नको लागि Firefox डाउनलोड गर्नुहोस् ">
<!-- LOCALIZATION NOTE (mobilePromo3.androidLink): This is a link title that links to https://www.mozilla.org/firefox/android/ -->
<!ENTITY mobilePromo3.androidLink      "Android">

<!-- LOCALIZATION NOTE (mobilePromo3.iOSBefore): This is text displayed between mobilePromo3.androidLink and mobilePromo3.iosLink -->
<!ENTITY mobilePromo3.iOSBefore         " वा ">
<!-- LOCALIZATION NOTE (mobilePromo3.iOSLink): This is a link title that links to https://www.mozilla.org/firefox/ios/ -->
<!ENTITY mobilePromo3.iOSLink          "iOS">

<!ENTITY mobilePromo3.end              " ‌मोबाइल यन्त्रमा समक्रमण गर्नको लागि।">

<!ENTITY mobilepromo.singledevice      "अर्को उपकरण जडान गर्नुहोस्">
<!ENTITY mobilepromo.multidevice       "यन्त्रहरू प्रबन्धन गर्नुहोस्">
