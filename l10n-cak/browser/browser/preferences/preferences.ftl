# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

do-not-track-description = Ketaq ri taq ruxaq ajk'amaya'l jun “Mani Tojqäx” raqän kumal chi man nojowäx ta chi tikanöx
do-not-track-learn-more = Tetamäx ch'aqa' chik
do-not-track-option-default =
    .label = Xa xe toq nokisäx Chajinïk chuwäch Ojqanïk
do-not-track-option-always =
    .label = Junelïk
pref-page =
    .title = { PLATFORM() ->
            [windows] Taq cha'oj
           *[other] Taq ajowab'äl
        }
# This is used to determine the width of the search field in about:preferences,
# in order to make the entire placeholder string visible
#
# Notice: The value of the `.style` attribute is a CSS string, and the `width`
# is the name of the CSS property. It is intended only to adjust the element's width.
# Do not translate.
search-input =
    .style = width: 15.4em
pane-general-title = Chijun
category-general =
    .tooltiptext = { pane-general-title }
pane-search-title = Tikanöx
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = Ichinanem & Jikomal
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = Rub'i' rutaqoya'l Firefox
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = Ruto'ik { -brand-short-name } Temeb'äl
focus-search =
    .key = f
close-button =
    .aria-label = Titz'apïx

## Browser Restart Dialog

feature-enable-requires-restart = { -brand-short-name } k'o chi nitikirisäx chik richin nitzijtäj re jun rub'anikil re'.
feature-disable-requires-restart = { -brand-short-name } k'o chi nitikirisäx chik richin nichup re rub'anikil re'.
should-restart-title = Titikirisäx chik ri { -brand-short-name }
should-restart-ok = Titikirisäx chik { -brand-short-name } wakami
revert-no-restart-button = Tik'ex
restart-later = Titikirisäx pa jun mej
