# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pref-page =
    .title = { PLATFORM() ->
            [windows] ရွေးစရာများ
           *[other] နှစ်သက်ရာ အပြင်အဆင်များ
        }
pane-general-title = အထွေထွေ
category-general =
    .tooltiptext = { pane-general-title }
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = ကိုယ်ရေးကာကွယ်မှု & လုံခြုံမှု
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = Firefox အကောင့်
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = { -brand-short-name } အထောက်အပံ့
focus-search =
    .key = f
close-button =
    .aria-label = ပိတ်ပါ 

## Browser Restart Dialog

feature-enable-requires-restart = ယခု လုပ်ဆောင်ချက်ကို အသုံးပြုရန် { -brand-short-name } ကို ပြန်ဖွင့်ရမည်။
feature-disable-requires-restart = ယခု လုပ်ဆောင်ချက်ကို ပိတ်ရန် { -brand-short-name } ကို ပြန်ဖွင့်ရမည်။
should-restart-title = { -brand-short-name } ကို ပြန်ဖွင့်ပါ
should-restart-ok = ယခုပင် { -brand-short-name } ကို ပြန်လည်စတင်ပါ
revert-no-restart-button = မူလအတိုင်း ပြန်ထားရန်
restart-later = နောက်မှ ပြန်ဖွင့်ပါ
