<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  trackingProtectionHeader2.label      "Protection contra le traciamento">
<!ENTITY  trackingProtection2.description      "Traciamento es le collecta de tu datos de navigation trans multiple sitos web. Le traciamento pote esser usate pro construer un profilo e monstrar contento basate in tu navigation e in tu informationes personal.">
<!ENTITY  trackingProtection2.radioGroupLabel  "Usar le protection contra le traciamento pro blocar traciatores cognoscite">
<!ENTITY  trackingProtection3.description      "Le protection de traciamento bloca le traciatores online que collige tu datos de navigation inter plure sitos web.">
<!ENTITY  trackingProtection3.radioGroupLabel  "Usar le protection contra le traciamento pro blocar traciatores cognoscite">
<!ENTITY  trackingProtectionAlways.label       "Sempre">
<!ENTITY  trackingProtectionAlways.accesskey   "S">
<!ENTITY  trackingProtectionPrivate.label      "Solmente in fenestras private">
<!ENTITY  trackingProtectionPrivate.accesskey  "l">
<!ENTITY  trackingProtectionNever.label        "Nunquam">
<!ENTITY  trackingProtectionNever.accesskey    "n">
<!ENTITY  trackingProtectionLearnMore.label    "Saper plus">
<!ENTITY  trackingProtectionLearnMore2.label    "Apprende altero re le protection de traciamento e tu confidentialitate">
<!ENTITY  trackingProtectionExceptions.label   "Exceptiones…">
<!ENTITY  trackingProtectionExceptions.accesskey "x">

<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the release and beta channel. -->
<!ENTITY trackingProtectionPBM6.label         "Usar le protection contra le traciamento in navigation private pro blocar traciatores cognoscite">
<!ENTITY trackingProtectionPBM6.accesskey     "v">
<!ENTITY trackingProtectionPBMLearnMore.label "Saper plus">
<!ENTITY changeBlockList2.label               "Cambiar le lista de blocage…">
<!ENTITY changeBlockList2.accesskey           "C">

<!ENTITY  doNotTrack.description        "Inviar al sitos web un signal “Non traciar” indicante que tu non vole esser traciate">
<!ENTITY  doNotTrack.learnMore.label    "Saper plus">
<!ENTITY  doNotTrack.default.label      "Solmente durante que tu usa le protection contra le traciamento">
<!ENTITY  doNotTrack.always.label       "Sempre">

<!ENTITY  history.label                 "Chronologia">
<!ENTITY  permissions.label             "Permissiones">

<!ENTITY  addressBar.label              "Barra de adresse">
<!ENTITY  addressBar.suggest.label      "Durante que tu usa le barra de adresse, suggerer">
<!ENTITY  locbar.history2.label         "Chronologia de navigation">
<!ENTITY  locbar.history2.accesskey     "H">
<!ENTITY  locbar.bookmarks.label        "Marcapaginas">
<!ENTITY  locbar.bookmarks.accesskey    "M">
<!ENTITY  locbar.openpage.label         "Schedas aperite">
<!ENTITY  locbar.openpage.accesskey     "S">
<!ENTITY  locbar.searches.label         "Recercas simile ab le motor de recerca predefinite">
<!ENTITY  locbar.searches.accesskey     "d">

<!ENTITY  suggestionSettings2.label     "Cambiar le preferentias pro le suggestiones del motor de recerca">

<!ENTITY  acceptCookies2.label          "Acceptar cookies ab le sitos web">
<!ENTITY  acceptCookies2.accesskey      "A">

<!ENTITY  acceptThirdParty2.pre.label     "Acceptar cookies de tertios">
<!ENTITY  acceptThirdParty2.pre.accesskey "t">
<!ENTITY  acceptThirdParty.always.label   "Sempre">
<!ENTITY  acceptThirdParty.never.label    "Nunquam">
<!ENTITY  acceptThirdParty.visited.label  "Del visitates">

<!ENTITY  keepUntil2.label              "Conservar usque">
<!ENTITY  keepUntil2.accesskey          "u">

<!ENTITY  expire.label                  "illos expira">
<!ENTITY  close.label                   "io claude &brandShortName;">

<!ENTITY  cookieExceptions.label        "Exceptiones…">
<!ENTITY  cookieExceptions.accesskey    "E">

<!ENTITY  showCookies.label             "Monstrar le cookies…">
<!ENTITY  showCookies.accesskey         "S">

<!ENTITY  historyHeader2.pre.label         "&brandShortName; debe">
<!ENTITY  historyHeader2.pre.accesskey     "d">
<!ENTITY  historyHeader.remember.label     "Memorisar le chronologia">
<!ENTITY  historyHeader.dontremember.label "Non memorisar jammais le chronologia">
<!ENTITY  historyHeader.custom.label       "Usar parametros personalisate pro le chronologia">
<!ENTITY  historyHeader.post.label         "">

<!ENTITY  rememberDescription.label      "&brandShortName; memorisara tu chronologia de navigation, discargamentos, formularios e recercas e conservara le cookies del sitos web que tu visita.">

<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->
<!ENTITY  rememberActions.pre.label           "Forsan tu vole ">
<!ENTITY  rememberActions.clearHistory.label  "vacuar tu chronologia recente">
<!ENTITY  rememberActions.middle.label        " o ">
<!ENTITY  rememberActions.removeCookies.label "remover cookies specific">
<!ENTITY  rememberActions.post.label          ".">

<!ENTITY  dontrememberDescription.label  "&brandShortName; usara le mesme parametros que pro le navigation private e non memorisara le chronologia durante que tu naviga le Web.">

<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->
<!ENTITY  dontrememberActions.pre.label          "Forsan tu vole tamben ">
<!ENTITY  dontrememberActions.clearHistory.label "vacuar tote tu chronologia recente">
<!ENTITY  dontrememberActions.post.label         ".">

<!ENTITY  privateBrowsingPermanent2.label "Sempre usar le modo de navigation private">
<!ENTITY  privateBrowsingPermanent2.accesskey "p">

<!ENTITY  rememberHistory2.label      "Memorisar mi chronologia de navigation e de discargamentos">
<!ENTITY  rememberHistory2.accesskey  "M">

<!ENTITY  rememberSearchForm.label       "Memorisar le chronologia de recercas e de formularios">
<!ENTITY  rememberSearchForm.accesskey   "f">

<!ENTITY  clearOnClose.label             "Vacuar le chronologia quando &brandShortName; se claude">
<!ENTITY  clearOnClose.accesskey         "r">

<!ENTITY  clearOnCloseSettings.label     "Parametros…">
<!ENTITY  clearOnCloseSettings.accesskey "t">

<!ENTITY  browserContainersLearnMore.label      "Saper plus">
<!ENTITY  browserContainersEnabled.label        "Activar le schedas contextual">
<!ENTITY  browserContainersEnabled.accesskey    "n">
<!ENTITY  browserContainersSettings.label        "Parametros…">
<!ENTITY  browserContainersSettings.accesskey    "P">

<!ENTITY  a11yPrivacy.checkbox.label     "Impedir que le servicios de accessibilitate accede a tu navigator">
<!ENTITY  a11yPrivacy.checkbox.accesskey "a">
<!ENTITY  a11yPrivacy.learnmore.label    "Saper plus">
<!ENTITY enableSafeBrowsingLearnMore.label "Saper plus">
