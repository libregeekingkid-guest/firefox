<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Prejsť naspäť">
<!ENTITY safeb.palm.seedetails.label "Podrobnosti">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Toto nie je podvodná stránka…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "T">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Rady poskytla spoločnosť <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Návšteva tejto stránky môže poškodiť váš počítač">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "Aplikácia &brandShortName; zablokovala túto stránku, pretože by sa mohla pokúsiť o inštaláciu škodlivého softvéru alebo by sa mohla pokúsiť odcudziť alebo odstrániť údaje vo vašom počítači.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "Stránka <span id='malware_sitename'/> bola <a id='error_desc_link'> nahlásená ako stránka so škodlivým softvérom</a>. Môžete nahlásiť <a id='report_detection'>chybu v detekcii</a> alebo sa rozhodnúť <a id='ignore_warning_link'>ignorovať riziko</a> a pokračovať na túto nebezpečnú stránku.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "Stránka <span id='malware_sitename'/> bola <a id='error_desc_link'> nahlásená ako stránka so škodlivým softvérom</a>. Môžete nahlásiť <a id='report_detection'>chybu v detekcii</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Ďalšie informácie o škodlivom obsahu na webe, ako sú vírusy a ďalší malvér a o tom, ako chrániť svoj počítač nájdete na <a id='learn_more_link'>StopBadware.org</a>. Ďalšie informácie o ochrane pred phishingom a malvérom v aplikácii &brandShortName; nájdete na <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Táto stránka môže obsahovať škodlivé programy">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "Prehliadač &brandShortName; zablokoval túto stránku, pretože by sa mohla pokúsiť nainštalovať programy, ktoré vám budú škodiť pri prehliadaní (napr. zmenou domovskej stránky alebo zobrazovaním prídavných reklám).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Stránka <span id='unwanted_sitename'/> bola <a id='error_desc_link'> nahlásená ako stránka so škodlivým softvérom</a>. Môžete sa rozhodnúť <a id='ignore_warning_link'>ignorovať riziko</a> a pokračovať na túto nebezpečnú stránku.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Stránka <span id='unwanted_sitename'/> bola <a id='error_desc_link'> nahlásená ako stránka so škodlivým softvérom</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Ďalšie informácie o škodlivom a nechcenom softvére nájdete na stránke <a id='learn_more_link'>Unwanted Software Policy</a>. Ďalšie informácie o ochrane pred phishingom a malvérom v aplikácii &brandShortName; nájdete na <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Podvodná stránka">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "Prehliadač &brandShortName; zablokoval túto stránku, pretože by vás mohla podvodom prinútiť k niečomu nebezpečnému, napríklad k inštalácii softvéru alebo k prezradeniu osobných informácií, napríklad hesiel alebo čísla platobnej karty.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Stránka <span id='phishing_sitename'/> bola <a id='error_desc_link'> nahlásená ako podvodná stránka</a>. Môžete nahlásiť <a id='report_detection'>chybu v detekcii</a> alebo sa rozhodnúť <a id='ignore_warning_link'>ignorovať riziko</a> a pokračovať na túto nebezpečnú stránku.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Stránka <span id='phishing_sitename'/> bola <a id='error_desc_link'> nahlásená ako podvodná stránka</a>. Môžete nahlásiť <a id='report_detection'>chybu v detekcii</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Ďalšie informácie o podvodných stránkach a phishingu nájdete na <a id='learn_more_link'>www.antiphishing.org</a>. Ďalšie informácie o ochrane pred phishingom a malvérom v aplikácii &brandShortName; nájdete na <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Táto stránka môže obsahovať malvér">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "Prehliadač &brandShortName; zablokoval túto stránku, pretože by sa mohla pokúsiť nainštalovať do vášho zariadenia nebezpečné aplikácie, ktoré by mohli ukradnúť alebo odstrániť vaše údaje (napríklad fotky, heslá, správy či údaje kreditných kariet).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Stránka <span id='harmful_sitename'/> bola <a id='error_desc_link'> nahlásená ako stránka s potenciálne škodlivou aplikáciou</a>. Môžete sa rozhodnúť <a id='ignore_warning_link'>ignorovať toto riziko</a> a pokračovať na túto nebezpečnú stránku.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Stránka <span id='harmful_sitename'/> bola <a id='error_desc_link'> nahlásená ako stránka s potenciálne škodlivou aplikáciou</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Ďalšie informácie o ochrane pred phishingom a malvérom v aplikácii &brandShortName; nájdete na <a id='firefox_support'>support.mozilla.org</a>.">
