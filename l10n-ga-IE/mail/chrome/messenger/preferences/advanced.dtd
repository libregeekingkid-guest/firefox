<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "Ginearálta">
<!ENTITY dataChoicesTab.label    "Roghanna Sonraí">
<!ENTITY itemUpdate.label        "Nuashonrú">
<!ENTITY itemNetworking.label    "Líonra agus Spás Diosca">
<!ENTITY itemCertificates.label  "Teastais">
<!-- General Settings -->

<!ENTITY enableGlodaSearch.label       "Cumasaigh Cuardach Cuimsitheach agus an tInneacsóir">
<!ENTITY enableGlodaSearch.accesskey   "e">
<!ENTITY dateTimeFormatting.label      "Formáidiú Dáta agus Ama">
<!ENTITY allowHWAccel.label            "Bain úsáid as luasghéarú crua-earraí más féidir">
<!ENTITY allowHWAccel.accesskey        "l">
<!ENTITY storeType.label               "Cineál Stórais na dTeachtaireachtaí le haghaidh cuntas nua:">
<!ENTITY storeType.accesskey           "T">
<!ENTITY mboxStore2.label              "Gach fillteán ina chomhad féin (mbox)">
<!ENTITY maildirStore.label            "Gach teachtaireacht ina comhad féin (maildir)">

<!ENTITY scrolling.label               "Scrollú">
<!ENTITY useAutoScroll.label           "Úsáid uathscrollú">
<!ENTITY useAutoScroll.accesskey       "s">
<!ENTITY useSmoothScrolling.label      "Úsáid mínscrollú">
<!ENTITY useSmoothScrolling.accesskey  "m">

<!ENTITY systemIntegration.label       "Comhtháthú Córais">
<!ENTITY alwaysCheckDefault.label      "Seiceáil i gcónaí ag am tosaithe an é &brandShortName; an cliant réamhshocraithe ríomhphoist">
<!ENTITY alwaysCheckDefault.accesskey  "a">
<!ENTITY searchIntegration.label       "Ceadaigh do &searchIntegration.engineName; teachtaireachtaí a chuardach">
<!ENTITY searchIntegration.accesskey   "C">
<!ENTITY checkDefaultsNow.label        "Seiceáil Anois…">
<!ENTITY checkDefaultsNow.accesskey    "n">
<!ENTITY configEditDesc.label          "Cumraíocht Chasta">
<!ENTITY configEdit.label              "Eagarthóir Cumraíochta…">
<!ENTITY configEdit.accesskey          "C">
<!ENTITY returnReceiptsInfo.label      "Socraigh conas a láimhseálann &brandShortName; admhálacha léite">
<!ENTITY showReturnReceipts.label      "Admhálacha Léite…">
<!ENTITY showReturnReceipts.accesskey  "L">
<!-- Data Choices -->

<!ENTITY telemetrySection.label          "Teiliméadracht">
<!ENTITY telemetryDesc.label             "Seolann sé faisnéis maidir le feidhmíocht, crua-earraí, úsáid, agus saincheapadh do chliaint ríomhphoist chuig &vendorShortName; chun cabhrú linn feabhas a chur ar &brandShortName;">
<!ENTITY enableTelemetry.label           "Cumasaigh Teiliméadracht">
<!ENTITY enableTelemetry.accesskey       "T">
<!ENTITY telemetryLearnMore.label        "Tuilleadh Eolais">

<!ENTITY crashReporterSection.label      "Tuairisceoir Tuairteála">
<!ENTITY crashReporterDesc.label         "Seolann &brandShortName; tuairiscí tuairteála chuig &vendorShortName; chun cabhrú linn cliant ríomhphoist níos cobhsaí níos sábháilte a fhorbairt">
<!ENTITY enableCrashReporter.label       "Cumasaigh an Tuairisceoir Tuairteála">
<!ENTITY enableCrashReporter.accesskey   "C">
<!ENTITY crashReporterLearnMore.label    "Tuilleadh Eolais">
<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->

<!ENTITY updateApp2.label                "Nuashonruithe &brandShortName;">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "Leagan ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAuto.label                "Suiteáil nuashonruithe go huathoibríoch (molta: slándáil níos fearr)">
<!ENTITY updateAuto.accesskey            "a">
<!ENTITY updateCheck.label               "Lorg nuashonruithe, ach lig dom iad a shuiteáil nuair is mian liom">
<!ENTITY updateCheck.accesskey           "L">
<!ENTITY updateManual.label              "Ná lorg nuashonruithe riamh (rabhadh: contúirt slándála)">
<!ENTITY updateManual.accesskey          "N">
<!ENTITY updateHistory.label             "Taispeáin Stair na Nuashonruithe">
<!ENTITY updateHistory.accesskey         "p">

<!ENTITY useService.label                "Úsáid seirbhís sa chúlra chun nuashonruithe a shuiteáil">
<!ENTITY useService.accesskey            "b">
<!-- Networking and Disk Space -->

<!ENTITY showSettings.label            "Socruithe…">
<!ENTITY showSettings.accesskey        "S">
<!ENTITY proxiesConfigure.label        "Cumraigh conas a cheanglófar &brandShortName; leis an Idirlíon">
<!ENTITY connectionsInfo.caption       "Ceangal">
<!ENTITY offlineInfo.caption           "As Líne">
<!ENTITY offlineInfo.label             "Cumraigh socruithe as líne">
<!ENTITY showOffline.label             "As Líne…">
<!ENTITY showOffline.accesskey         "A">

<!ENTITY Diskspace                       "Spás Diosca">
<!ENTITY offlineCompactFolders.label     "Dlúthaigh fillteáin dá sábhálfadh sé níos mó ná">
<!ENTITY offlineCompactFolders.accesskey "a">
<!ENTITY offlineCompactFoldersMB.label   "MB iomlán">
<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->

<!ENTITY useCacheBefore.label            "Úsáid suas le">
<!ENTITY useCacheBefore.accesskey        "s">
<!ENTITY useCacheAfter.label             "MB spás don taisce">
<!ENTITY overrideSmartCacheSize.label    "Sáraigh bainistíocht uathoibríoch na taisce">
<!ENTITY overrideSmartCacheSize.accesskey "b">
<!ENTITY clearCacheNow.label             "Bánaigh Anois">
<!ENTITY clearCacheNow.accesskey         "B">
<!-- Certificates -->

<!ENTITY certSelection.description       "Nuair atá freastalaí ag iarraidh mo theastas pearsanta:">
<!ENTITY certs.auto                      "Roghnaigh go huathoibríoch">
<!ENTITY certs.auto.accesskey            "S">
<!ENTITY certs.ask                       "Fiafraigh díom i gcónaí">
<!ENTITY certs.ask.accesskey             "W">
<!ENTITY enableOCSP.label                "Iarr ar fhreastalaí freagróra OCSP bailíocht teastais a dheimhniú">
<!ENTITY enableOCSP.accesskey            "O">

<!ENTITY manageCertificates.label "Bainistigh Teastais">
<!ENTITY manageCertificates.accesskey "B">
<!ENTITY viewSecurityDevices.label "Gléasanna Slándála">
<!ENTITY viewSecurityDevices.accesskey "d">
