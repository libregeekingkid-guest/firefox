<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- The page shown when logged in... -->

<!ENTITY engine.bookmarks.label     "Σελιδοδείκτες">
<!ENTITY engine.bookmarks.accesskey "δ">
<!ENTITY engine.tabs.label2         "Άνοιγμα καρτελών">
<!ENTITY engine.tabs.accesskey      "τ">
<!ENTITY engine.history.label       "Ιστορικό">
<!ENTITY engine.history.accesskey   "ρ">
<!ENTITY engine.logins.label        "Συνδέσεις">
<!ENTITY engine.logins.title        "Ονόματα χρήστη και κωδικοί πρόσβασης που έχετε αποθηκεύσει">
<!ENTITY engine.logins.accesskey    "Σ">
<!-- On Windows we use the term "Options" to describe settings, but
     on Linux and Mac OS X we use "Preferences" - carry that distinction
     over into this string, used as the checkbox which indicates if prefs are synced
-->
<!ENTITY engine.prefsWin.label      "Επιλογές">
<!ENTITY engine.prefs.label         "Προτιμήσεις">
<!ENTITY engine.prefs.accesskey     "ρ">
<!ENTITY engine.addons.label        "Πρόσθετα">
<!ENTITY engine.addons.accesskey    "θ">
<!ENTITY engine.addresses.label     "Διευθύνσεις">
<!ENTITY engine.addresses.accesskey "Δ">
<!ENTITY engine.creditcards.label   "Πιστωτικές κάρτες">
<!ENTITY engine.creditcards.title   "Ονόματα, αριθμοί και ημερομηνίες λήξης (μόνο για υπολογιστές)">
<!ENTITY engine.creditcards.accesskey "Π">

<!-- Device Settings -->
<!ENTITY fxaSyncDeviceName.label       "Όνομα συσκευής">
<!ENTITY changeSyncDeviceName2.label "Αλλαγή ονόματος συσκευής…">
<!ENTITY changeSyncDeviceName2.accesskey "λ">
<!ENTITY cancelChangeSyncDeviceName.label "Ακύρωση">
<!ENTITY cancelChangeSyncDeviceName.accesskey "κ">
<!ENTITY saveChangeSyncDeviceName.label "Αποθήκευση">
<!ENTITY saveChangeSyncDeviceName.accesskey "θ">

<!-- Footer stuff -->
<!ENTITY prefs.tosLink.label        "Όροι υπηρεσίας">
<!ENTITY fxaPrivacyNotice.link.label "Σημείωση απορρήτου">

<!-- LOCALIZATION NOTE (signedInUnverified.beforename.label,
signedInUnverified.aftername.label): these two string are used respectively
before and after the account email address. Localizers can use one of them, or
both, to better adapt this sentence to their language.
-->
<!ENTITY signedInUnverified.beforename.label "">
<!ENTITY signedInUnverified.aftername.label "Μη επαληθευμένος.">

<!-- LOCALIZATION NOTE (signedInLoginFailure.beforename.label,
signedInLoginFailure.aftername.label): these two string are used respectively
before and after the account email address. Localizers can use one of them, or
both, to better adapt this sentence to their language.
-->
<!ENTITY signedInLoginFailure.beforename.label "Παρακαλούμε συνδεθείτε ξανά για επανασύνδεση">
<!ENTITY signedInLoginFailure.aftername.label "">

<!ENTITY notSignedIn.label            "Δεν έχετε συνδεθεί.">
<!ENTITY signIn.label                 "Σύνδεση">
<!ENTITY signIn.accesskey             "σ">
<!ENTITY profilePicture.tooltip       "Αλλαγή εικόνας προφίλ">
<!ENTITY verifiedManage.label         "Διαχείριση λογαριασμού">
<!ENTITY verifiedManage.accesskey     "η">
<!ENTITY disconnect3.label            "Αποσύνδεση…">
<!ENTITY disconnect3.accesskey        "Α">
<!ENTITY verify.label                "Επαλήθευση email">
<!ENTITY verify.accesskey            "Ε">
<!ENTITY forget.label                "Διαγραφή email">
<!ENTITY forget.accesskey            "Γ">

<!ENTITY cancelSetup.label            "Ακύρωση ρύθμισης">
<!ENTITY cancelSetup.accesskey        "ρ">

<!ENTITY signedOut.caption            "Πάρτε μαζί σας το διαδίκτυο">
<!ENTITY signedOut.description        "Συγχρονίστε τους σελιδοδείκτες, το ιστορικό, τις καρτέλες, τους κωδικούς πρόσβασης, τα πρόσθετα, καθώς και τις προτιμήσεις σας σε όλες τις συσκευές σας.">
<!ENTITY signedOut.accountBox.title   "Σύνδεση με ένα λογαριασμό Firefox">
<!ENTITY signedOut.accountBox.create2 "Δεν έχετε λογαριασμό; Ξεκινήστε">
<!ENTITY signedOut.accountBox.create2.accesskey "Γ">
<!ENTITY signedOut.accountBox.signin2 "Σύνδεση…">
<!ENTITY signedOut.accountBox.signin2.accesskey "Σ">

<!ENTITY signedIn.settings.label       "Ρυθμίσεις Sync">
<!ENTITY signedIn.settings.description "Επιλέξτε τι θα συγχρονίζεται στις συσκευές σας με το &brandShortName;.">

<!-- LOCALIZATION NOTE (mobilePromo3.*): the following strings will be used to
     create a single sentence with active links.
     The resulting sentence in English is: "Download Firefox for
     Android or iOS to sync with your mobile device." -->

<!ENTITY mobilePromo3.start            "Κάντε λήψη του Firefox για ">
<!-- LOCALIZATION NOTE (mobilePromo3.androidLink): This is a link title that links to https://www.mozilla.org/firefox/android/ -->
<!ENTITY mobilePromo3.androidLink      "Android">

<!-- LOCALIZATION NOTE (mobilePromo3.iOSBefore): This is text displayed between mobilePromo3.androidLink and mobilePromo3.iosLink -->
<!ENTITY mobilePromo3.iOSBefore         " ή ">
<!-- LOCALIZATION NOTE (mobilePromo3.iOSLink): This is a link title that links to https://www.mozilla.org/firefox/ios/ -->
<!ENTITY mobilePromo3.iOSLink          "iOS">

<!ENTITY mobilePromo3.end              " για συγχρονισμό με την κινητή σας συσκευή.">
