<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "
Burim të dhënash ping-u:
">
<!ENTITY aboutTelemetry.showCurrentPingData "
Të dhëna ping-u të tanishme
">
<!ENTITY aboutTelemetry.showArchivedPingData "
Të dhëna ping-u të arkivuara
">
<!ENTITY aboutTelemetry.showSubsessionData "
Shfaqni të dhëna nënsesioni
">
<!ENTITY aboutTelemetry.choosePing "
Zgjidhni ping:
">
<!ENTITY aboutTelemetry.archivePingType "Lloj Ping-u">
<!ENTITY aboutTelemetry.archivePingHeader "
Ping
">
<!ENTITY aboutTelemetry.optionGroupToday "Sot">
<!ENTITY aboutTelemetry.optionGroupYesterday "Dje">
<!ENTITY aboutTelemetry.optionGroupOlder "Më të vjetër">
<!ENTITY aboutTelemetry.payloadChoiceHeader "
  Ngarkesë
">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Të dhëna Telemetry-e">
<!ENTITY aboutTelemetry.moreInformations "Po kërkoni për më tepër informacion?">
<!ENTITY aboutTelemetry.firefoxDataDoc "<a>Dokumentimi i të Dhënave të Firefox-it</a> përmban udhërrëfyes se si të punohet me mjetet tona për të dhënat.">
<!ENTITY aboutTelemetry.telemetryClientDoc "<a>Dokumentimi i klientit të Firefox Telemetry-së</a> përfshin përkufizime për koncepte, dokumentim të API-t dhe referenca të dhënash.">
<!ENTITY aboutTelemetry.telemetryDashboard "<a>Pultet e Telemetry-së</a> ju lejojnë të vizualizoni të dhënat që Mozilla merr përmes Telemetry-së.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "Hape në parësin JSON">

<!ENTITY aboutTelemetry.homeSection "Kreu">
<!ENTITY aboutTelemetry.generalDataSection "
  Të dhëna të Përgjithshme
">
<!ENTITY aboutTelemetry.environmentDataSection "
  Të dhëna Mjedisi
">
<!ENTITY aboutTelemetry.sessionInfoSection "
  Të dhëna Sesioni
">
<!ENTITY aboutTelemetry.scalarsSection "
  Skalarë
">
<!ENTITY aboutTelemetry.keyedScalarsSection "
  Skalarë Me Fjalëkyçe
">
<!ENTITY aboutTelemetry.histogramsSection "
  Histograme
">
<!ENTITY aboutTelemetry.keyedHistogramsSection "
  Histograme Me Kyç
">
<!ENTITY aboutTelemetry.eventsSection "
  Akte
">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "
  Matje të Thjeshta
">
<!ENTITY aboutTelemetry.telemetryLogSection "
  Regjistër Telemetry-e
">
<!ENTITY aboutTelemetry.slowSqlSection "
  Deklarime SQL të Ngadalta
">
<!ENTITY aboutTelemetry.chromeHangsSection "
  Ngecje Shfletuesi
">
<!ENTITY aboutTelemetry.addonDetailsSection "
  Hollësi Shtese
">
<!ENTITY aboutTelemetry.capturedStacksSection "Stack-e të Ndjekur">
<!ENTITY aboutTelemetry.lateWritesSection "
  Shkrime të Vonshme
">
<!ENTITY aboutTelemetry.rawPayloadSection "Ngarkesë e Papërpunuar">
<!ENTITY aboutTelemetry.raw "
JSON të papërpunuar
">

<!ENTITY aboutTelemetry.fullSqlWarning "
  SHËNIM: Diagnostikimi i Ngadalësisë në SQL është i aktivizuar. Vargjet e plota SQL mund të shfaqen më poshtë, por ato nuk do t&apos;i parashtrohen Telemetry-së.
">
<!ENTITY aboutTelemetry.fetchStackSymbols "Sill emra funksionesh për stack-e">
<!ENTITY aboutTelemetry.hideStackSymbols "Shfaq të dhëna të papërpunuara stack-u">
