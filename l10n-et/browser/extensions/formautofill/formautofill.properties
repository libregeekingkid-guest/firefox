# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S võimaldab nüüd aadresse salvestada, et vormide täitmine oleks kiirem.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Vormide automaatse täitmise sätted
autofillOptionsLinkOSX = Vormide automaatse täitmise eelistused
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Vormide automaatse täitmise ja turvalisuse sätted
autofillSecurityOptionsLinkOSX = Vormide automaatse täitmise ja turvalisuse eelistused
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Muuda vormide automaatse täitmise sätteid
changeAutofillOptionsOSX = Muuda vormide automaatse täitmise eelistusi
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Aadresse jagatakse sünkroniseeritud seadmetega
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Krediitkaardiandmeid jagatakse sünkroniseeritud seadmetega
# LOCALIZATION NOTE (updateAddressMessage, createAddressLabel, updateAddressLabel): Used on the doorhanger
# when an address change is detected.
updateAddressMessage = Kas soovid oma aadressi uuendada nende uute andmetega?
createAddressLabel = Loo uus aadress
updateAddressLabel = Uuenda aadressi
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Kas soovid, et %S salvestaks selle krediitkaardi? (Turvakoodi ei salvestata)
saveCreditCardLabel = Salvesta krediitkaart
cancelCreditCardLabel = Ära salvesta
neverSaveCreditCardLabel = Krediitkaarte ei salvestata mitte kunagi
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Ava vormi automaatse täitmise paneel

# LOCALIZATION NOTE (autocompleteFooterOption, autocompleteFooterOptionOSX): Used as a label for the button,
# displayed at the bottom of the drop down suggestion, to open Form Autofill browser preferences.
autocompleteFooterOption = Vormide automaatse täitmise sätted
autocompleteFooterOptionOSX = Vormide automaatse täitmise eelistused
# LOCALIZATION NOTE (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the drop down suggestion, to open Form Autofill browser preferences. This version is used
# instead of autocompleteFooterOption* when the menu width is below 185px.
autocompleteFooterOptionShort = Rohkem sätteid
autocompleteFooterOptionOSXShort = Eelistused
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = aadress
category.name = nimi
category.organization2 = asutus
category.tel = telefon
category.email = e-posti aadress
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Automaatselt täidetakse ka %S
phishingWarningMessage2 = Automaatselt täidetakse %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S tuvastas ebaturvalise saidi. Vormide automaatne täitmine on ajutiselt keelatud.

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Aadresside osa vormides täidetakse automaatselt
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Rohkem teavet
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Salvestatud aadressid…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Krediitkaartide osa vormides täidetakse automaatselt
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Salvestatud krediitkaardid…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Salvestatud aadressid
manageCreditCardsTitle = Salvestatud krediitkaardid
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Aadressid
creditCardsListHeader = Krediitkaardid
showCreditCardsBtnLabel = Kuva krediitkaarte
hideCreditCardsBtnLabel = Peida krediitkaardid
removeBtnLabel = Eemalda
addBtnLabel = Lisa…
editBtnLabel = Muuda…

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Uue aadressi lisamine
editAddressTitle = Aadressi muutmine
givenName = Eesnimi
additionalName = Teine nimi
familyName = Perekonnanimi
organization2 = Asutus
streetAddress = Tänava aadress
city = Linn
province = Provints
state = Maakond
postalCode = Sihtnumber
zip = Sihtkood
country = Riik või piirkond
tel = Telefon
email = E-posti aadress
cancelBtnLabel = Loobu
saveBtnLabel = Salvesta
countryWarningMessage = Praegu täidetakse vorme automaatselt ainult USA aadresside puhul

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Uue krediitkaardi lisamine
editCreditCardTitle = Krediitkaardi muutmine
cardNumber = Kaardi number
nameOnCard = Nimi kaardil
cardExpires = Aegumise aeg
