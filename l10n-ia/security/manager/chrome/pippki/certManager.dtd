<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY certmgr.title                       "Gestor de certificatos">

<!ENTITY certmgr.tab.mine                     "Tu certificatos">
<!ENTITY certmgr.tab.others2                  "Personas">
<!ENTITY certmgr.tab.websites3                "Servitores">
<!ENTITY certmgr.tab.ca                       "Autoritates">
<!ENTITY certmgr.tab.orphan2                  "Alteres">

<!ENTITY certmgr.mine2                        "Tu ha certificatos ex iste organisationes que identifica te">
<!ENTITY certmgr.others2                      "Tu ha certificatos sur le file que identifica iste personas">
<!ENTITY certmgr.websites3                    "Tu ha certificatos sur le file que identifica iste servitores">
<!ENTITY certmgr.cas2                         "Tu ha certificatos sur le file que identifica iste autoritates">
<!ENTITY certmgr.orphans2                     "Tu ha certificatos que non reentra in ulle del altere categorias">

<!ENTITY certmgr.detail.general_tab.title     "General">
<!ENTITY certmgr.detail.general_tab.accesskey "G">
<!ENTITY certmgr.detail.prettyprint_tab.title "Detalios">
<!ENTITY certmgr.detail.prettyprint_tab.accesskey "D">

<!ENTITY certmgr.pending.label                "Verificante ora le certificato…">
<!ENTITY certmgr.subjectinfo.label            "Emittite a">
<!ENTITY certmgr.issuerinfo.label             "Emittite per">
<!ENTITY certmgr.periodofvalidity.label       "Periodo de validitate" >
<!ENTITY certmgr.fingerprints.label           "Impressiones digital">
<!ENTITY certmgr.certdetail.title             "Detalio del certificato">
<!ENTITY certmgr.certdetail.cn                "Nomine Commun (CN)">
<!ENTITY certmgr.certdetail.o                 "Organisation (O)">
<!ENTITY certmgr.certdetail.ou                "Unitate organisative (OU)">
<!ENTITY certmgr.certdetail.serialnumber      "Numero serial">
<!ENTITY certmgr.certdetail.sha256fingerprint "Impression digital SHA-256">
<!ENTITY certmgr.certdetail.sha1fingerprint   "Impression digital SHA1">

<!ENTITY certmgr.editcacert.title             "Modificar le parametros de confidentia del CA">
<!ENTITY certmgr.editcert.edittrust           "Modificar le parametros de confidentialitate:">
<!ENTITY certmgr.editcert.trustssl            "Iste certificato pote identificar sitos web.">
<!ENTITY certmgr.editcert.trustemail          "Iste certificato pote identificar usatores de email.">
<!ENTITY certmgr.editcert.trustobjsign        "Iste certificato pote identificar developpatores de software.">

<!ENTITY certmgr.deletecert.title             "Deler le certificato">

<!ENTITY certmgr.certname                     "Nomine del certificato">
<!ENTITY certmgr.certserver                   "Servitor">
<!ENTITY certmgr.override_lifetime            "Duration del vita">
<!ENTITY certmgr.tokenname                    "Dispositivo de securitate">
<!ENTITY certmgr.begins                       "Initia in">
<!ENTITY certmgr.expires                      "Expira le">
<!ENTITY certmgr.email                        "Adresse email">
<!ENTITY certmgr.serial                       "Numero serial">

<!ENTITY certmgr.close.label                  "Clauder">
<!ENTITY certmgr.close.accesskey              "C">
<!ENTITY certmgr.view2.label                  "Vider…">
<!ENTITY certmgr.view2.accesskey              "V">
<!ENTITY certmgr.edit3.label                  "Modificar le confidentia…">
<!ENTITY certmgr.edit3.accesskey              "E">
<!ENTITY certmgr.export.label                 "Exportar…">
<!ENTITY certmgr.export.accesskey             "x">
<!ENTITY certmgr.delete2.label                "Deler…">
<!ENTITY certmgr.delete2.accesskey            "D">
<!ENTITY certmgr.delete_builtin.label         "Deler o non confider plus…">
<!ENTITY certmgr.delete_builtin.accesskey     "D">
<!ENTITY certmgr.backup2.label                "Salveguardar…">
<!ENTITY certmgr.backup2.accesskey            "S">
<!ENTITY certmgr.backupall2.label             "Salveguardar toto…">
<!ENTITY certmgr.backupall2.accesskey         "t">
<!ENTITY certmgr.restore2.label               "Importar…">
<!ENTITY certmgr.restore2.accesskey           "m">
<!ENTITY certmgr.details.label                "Campos del certificato">
<!ENTITY certmgr.details.accesskey            "F">
<!ENTITY certmgr.fields.label                 "Valor del campo">
<!ENTITY certmgr.fields.accesskey             "V">
<!ENTITY certmgr.hierarchy.label              "Hierarchia de certificatos">
<!ENTITY certmgr.hierarchy.accesskey2         "H">
<!ENTITY certmgr.addException.label           "Adder un exception…">
<!ENTITY certmgr.addException.accesskey       "x">

<!ENTITY exceptionMgr.title                   "Adder un exception de securitate">
<!ENTITY exceptionMgr.exceptionButton.label   "Confirmar le exception de securitate">
<!ENTITY exceptionMgr.exceptionButton.accesskey "C">
<!ENTITY exceptionMgr.supplementalWarning     "Bancas, botecas e altere sitos public legitime non te demandara pro facer isto.">
<!ENTITY exceptionMgr.certlocation.caption2   "Servitor">
<!ENTITY exceptionMgr.certlocation.url        "Adresse:">
<!ENTITY exceptionMgr.certlocation.download   "Obtener le certificato">
<!ENTITY exceptionMgr.certlocation.accesskey  "G">
<!ENTITY exceptionMgr.certstatus.caption      "Stato del certificato">
<!ENTITY exceptionMgr.certstatus.viewCert     "Vider…">
<!ENTITY exceptionMgr.certstatus.accesskey    "V">
<!ENTITY exceptionMgr.permanent.label         "Conservar permanentemente iste exception">
<!ENTITY exceptionMgr.permanent.accesskey     "P">
