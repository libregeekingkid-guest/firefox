# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S memorisescha ussa adressas per che ti possias emplenir pli svelt formulars.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Preferenzas da l'endataziun automatica per formulars
autofillOptionsLinkOSX = Preferenzas da l'endataziun automatica per formulars
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Preferenzas da l'endataziun automatica & da segirezza
autofillSecurityOptionsLinkOSX = Preferenzas da l'endataziun automatica & da segirezza
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Modifitgar las preferenzas da l'endataziun automatica per formulars
changeAutofillOptionsOSX = Modifitgar las preferenzas da l'endataziun automatica per formulars
changeAutofillOptionsAccessKey = c
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Cundivider las adressas cun ils apparats sincronisads
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Cundivider las cartas da credit cun ils apparats sincronisads
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Vuls ti actualisar tia adressa cun questa nova infurmaziun?
updateAddressDescriptionLabel = Adressa per actualisar:
createAddressLabel = Crear ina nova adressa
createAddressAccessKey = C
updateAddressLabel = Actualisar l'adressa
updateAddressAccessKey = u
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Vuls ti che %S memoriseschia questa carta da credit? (Il code da segirezza na vegn betg memorisà.)
saveCreditCardDescriptionLabel = Carta da credit per memorisar:
saveCreditCardLabel = Memorisar la carta da credit
saveCreditCardAccessKey = s
cancelCreditCardLabel = Betg memorisar
cancelCreditCardAccessKey = B
neverSaveCreditCardLabel = Mai memorisar cartas da credit
neverSaveCreditCardAccessKey = M
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Vuls ti actualisar tia carta da credit cun questa nova infurmaziun?
updateCreditCardDescriptionLabel = Carta da credit per actualisar:
createCreditCardLabel = Crear ina nova carta da credit
createCreditCardAccessKey = C
updateCreditCardLabel = Actualisar la carta da credit
updateCreditCardAccessKey = u
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Avrir la panela da messadis da l'endataziun automatica per formulars

# LOCALIZATION NOTE (autocompleteFooterOption, autocompleteFooterOptionOSX): Used as a label for the button,
# displayed at the bottom of the drop down suggestion, to open Form Autofill browser preferences.
autocompleteFooterOption = Preferenzas da l'endataziun automatica per formulars
autocompleteFooterOptionOSX = Preferenzas da l'endataziun automatica per formulars

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Ulteriuras opziuns
autocompleteFooterOptionOSXShort = Preferenzas
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = adressa
category.name = num
category.organization2 = organisaziun
category.tel = telefon
category.email = e-mail
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Endatescha era automaticamain %S
phishingWarningMessage2 = Endatescha automaticamain %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = Tenor %S è questa pagina malsegira. L'endataziun automatica per formulars è temporarmain deactivada.

# LOCALIZATION NOTE (clearFormBtnLabel): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel = Svidar il formular

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Endatar automaticamain las adressas
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Ulteriuras infurmaziuns
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Adressas memorisadas…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Endatar automaticamain datas da cartas da credit
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Cartas da credit memorisadas…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Adressas memorisadas
manageCreditCardsTitle = Cartas da credit memorisadas
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Adressas
creditCardsListHeader = Cartas da credit
showCreditCardsBtnLabel = Mussar las cartas da credit
hideCreditCardsBtnLabel = Zuppentar las cartas da credit
removeBtnLabel = Allontanar
addBtnLabel = Agiuntar…
editBtnLabel = Modifitgar…

# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Agiuntar ina nova adressa
editAddressTitle = Modifitgar l'adressa
givenName = Prenum
additionalName = Segund prenum
familyName = Num
organization2 = Organisaziun
streetAddress = Adressa postala
city = Lieu
province = Regiun
state = Stadi
postalCode = Numer postal
zip = Numer postal (USA)
country = Pajais u regiun
tel = Telefon
email = E-mail
cancelBtnLabel = Interrumper
saveBtnLabel = Memorisar
countryWarningMessage = L'endataziun automatica per formulars è actualmain mo disponibla per adressas en ils Stadis Unids

countryWarningMessage2 = L'endataziun automatica per formulars è actualmain mo disponibla per tscherts pajais.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Agiuntar ina nova carta da credit
editCreditCardTitle = Modifitgar la carta da credit
cardNumber = Numer da la carta
nameOnCard = Num sin la carta
cardExpires = Data da scadenza
