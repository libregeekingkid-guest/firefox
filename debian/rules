#!/usr/bin/make -f
TESTDIR = $(shell dh_testdir || echo no)
ifeq (,$(TESTDIR))
include debian/make.mk
# Use dpkg-buildflags to get hardening flags, exclude non-hardening flags,
# and disable read-only relocations.
dpkg_buildflags = $(and $(1),$(shell DEB_BUILD_MAINT_OPTIONS=hardening=-relro DEB_CFLAGS_MAINT_STRIP="$(shell DEB_BUILD_MAINT_OPTIONS=hardening=-all dpkg-buildflags --get $(1))" dpkg-buildflags --get $(1)))
$(call lazy,CFLAGS,$$(call dpkg_buildflags,CFLAGS))
$(call lazy,CPPFLAGS,$$(call dpkg_buildflags,CPPFLAGS))
$(call lazy,LDFLAGS,$$(call dpkg_buildflags,LDFLAGS))

$(call lazy,DEB_HOST_GNU_TYPE,$$(shell dpkg-architecture -qDEB_HOST_GNU_TYPE))
$(call lazy,DEB_BUILD_GNU_TYPE,$$(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE))
$(call lazy,DEB_HOST_ARCH,$$(shell dpkg-architecture -qDEB_HOST_ARCH))
$(call lazy,DEB_HOST_ARCH_ENDIAN,$$(shell dpkg-architecture -qDEB_HOST_ARCH_ENDIAN))

PYTHON := python -B

PRODUCT := browser
include debian/upstream.mk

SYSTEM_LIBS = zlib bz2 ffi libevent
ifeq (,$(filter wheezy jessie stretch,$(DIST)))
SYSTEM_LIBS += hunspell nspr nss sqlite
endif
ifeq (,$(filter wheezy jessie,$(DIST)))
SYSTEM_LIBS += vpx
endif

define system_lib
USE_SYSTEM_$(1) ?= 1
SYSTEM_LIBS_VARS += $$(if $$(USE_SYSTEM_$(1)),USE_SYSTEM_$(1))
endef
$(foreach lib,$(sort $(call uc,$(SYSTEM_LIBS))),$(eval $(call system_lib,$(lib))))

ifeq ($(DIST),wheezy)
CC := /usr/lib/gcc-mozilla/bin/gcc
CXX := /usr/lib/gcc-mozilla/bin/g++
endif

OFFICIAL_BRANDING := browser/branding/official
MOZILLA_OFFICIAL := 1
# ESR, Beta and Releases use the official branding
ifneq (,$(filter release beta esr%,$(SHORT_SOURCE_CHANNEL)))
BRANDING ?= $(OFFICIAL_BRANDING)
else
ifneq (,$(filter aurora,$(SHORT_SOURCE_CHANNEL)))
BRANDING ?= browser/branding/aurora
else
ifneq (,$(filter central,$(SHORT_SOURCE_CHANNEL)))
BRANDING ?= browser/branding/nightly
else
$(error $(PRODUCT_NAME) branding for $(SOURCE_CHANNEL) is unsupported)
endif
endif
endif

GTK3 ?= $(shell grep -q "platform_choices = .'cairo-gtk3'" toolkit/moz.configure && echo 1)

ifeq (firefox-esr,$(DEBIAN_SOURCE))
TRANSITION = 1
endif

BRANDING_CONFIGURE_FLAG = $(if $(filter $(BRANDING),$(OFFICIAL_BRANDING)),--enable-official-branding,--with-branding=$(BRANDING))

# Define PRODUCT, product and Product variables (replacing product with the
# actual value of $(PRODUCT))
$(PRODUCT) := $(call lc,$(PRODUCT_NAME))
uc_first = $(strip $(eval __tmp := $(1))$(foreach l,$(letters),$(eval __tmp := $(subst $(l),$(l) ,$(__tmp))))$(call uc,$(firstword $(__tmp)))$(1:$(firstword $(__tmp))%=%))
DISPLAY_NAME := $(call uc_first,$(PRODUCT_DOWNLOAD_NAME))$(if $(filter %-esr,$(PRODUCT_NAME)), ESR)
$(call uc,$(PRODUCT)) := $(call uc,$(DISPLAY_NAME))
Product := $(call uc_first,$(PRODUCT))
$(Product) := $(DISPLAY_NAME)

MOZ_APP_REMOTINGNAME := $(call uc_first,$($(PRODUCT)))

LIB_DIR := /usr/lib/$($(PRODUCT))
SHARE_DIR := /usr/share/$($(PRODUCT))

LDFLAGS := -Wl,--as-needed

# Reduce memory usage of the linker at the expense of processing time
# This should help on lower-end architectures like arm and mips, which
# spend an immense amount of time swapping.
LDFLAGS += -Wl,--reduce-memory-overheads
LDFLAGS += -Wl,--no-keep-memory
# Also add execution time and memory usage stats in the logs
LDFLAGS += -Wl,--stats

AUTOCONF_DIRS := build/autoconf

ifneq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
	CONFIGURE_FLAGS += --disable-optimize
endif

ifneq (,$(filter debug,$(DEB_BUILD_OPTIONS)))
	CONFIGURE_FLAGS += --enable-debug
endif
ifneq (,$(filter %i386 %amd64 armel armhf arm64,$(DEB_HOST_ARCH)))
	MOZ_FFVPX = 1
endif
ifneq (,$(filter i386 amd64 armel armhf,$(DEB_HOST_ARCH)))
	CRASH_REPORTER = 1
endif

ifeq (default,$(origin CC))
CC := gcc
endif
ifeq (default,$(origin CXX))
CXX := g++
endif

ifneq (,$(findstring gcc,$(CC)))
ifeq (,$(filter 4.% 5.%,$(shell $(CC) -dumpversion)))
CFLAGS += -fno-schedule-insns2 -fno-lifetime-dse -fno-delete-null-pointer-checks
ifneq (,$(filter armel armhf,$(DEB_HOST_ARCH)))
CFLAGS += -fno-schedule-insns
endif
endif
endif

CXXFLAGS = $(CFLAGS)

ifeq ($(DEB_HOST_ARCH_ENDIAN),little)
ICU_DATA_FILE = icudt60l.dat
else
ICU_DATA_FILE = icudt60b.dat
endif

EXPORTS := CC CXX CFLAGS CXXFLAGS CPPFLAGS LDFLAGS MOZILLA_OFFICIAL ICU_DATA_FILE
$(call lazy,CONFIGURE_ENV,$$(foreach export,$(EXPORTS),$$(export)="$$($$(export))"))

IN_FILES := $(wildcard debian/*.in)
ifeq ($(PRODUCT_NAME),firefox)
IN_FILES := $(filter-out debian/browser.preinst.in debian/browser.postrm.in debian/$($(PRODUCT)).in,$(IN_FILES))
endif
preprocessed_filename = $(subst $(PRODUCT),$($(PRODUCT)),$(subst GRE_VERSION,$(GRE_VERSION),$(1:.in=)))
define preprocess
$(call preprocessed_filename,$(1)): $(1)
PREPROCESSED_FILES += $(call preprocessed_filename,$(1))
endef
$(foreach f,$(IN_FILES),$(eval $(call preprocess, $(f))))

GENERATED_FILES += $(PREPROCESSED_FILES) debian/l10n/$(PRODUCT)-l10n.control
debian/control: debian/rules debian/changelog debian/l10n/$(PRODUCT)-l10n.control

$(call lazy,L10N_PACKAGES,$$(foreach lang,$$(L10N_LANGS),$($(PRODUCT))-l10n-$$(call lc,$$(lang))))
$(call lazy,L10N_PACKAGES_DEPS,$$(shell echo $$(L10N_PACKAGES) | sed 's/ /, /g'))

debian/l10n/$(PRODUCT)-l10n.control: %: %.in
	$(PYTHON) debian/l10n/gen $(L10N_LANGS) > $@

DICT_DIR := /usr/share/hunspell

$(PREPROCESSED_FILES): VARS = GRE_VERSION DICT_DIR $(PRODUCT) $(call uc,$(PRODUCT)) $(Product) MOZ_APP_REMOTINGNAME $(SYSTEM_LIBS_VARS) SHORT_SOURCE_CHANNEL GTK3 DIST TRANSITION MOZ_FFVPX ICU_DATA_FILE CRASH_REPORTER DEB_HOST_ARCH
debian/control: VARS += L10N_PACKAGES_DEPS PRODUCT
$(PREPROCESSED_FILES):
	$(PYTHON) python/mozbuild/mozbuild/preprocessor.py --marker % -Fsubstitution $(foreach var,$(VARS),-D$(var)="$($(var))" )$< > $@

ifdef TRANSITION
MAINTSCRIPTS := $(addprefix debian/,$(addsuffix .maintscript,$(shell awk -F/ '$$1 != "searchplugins" { next } $$2 == "locale" && $$3 != "en-US" { p="iceweasel-l10n-" $$3 } $$2 == "common" || ($$2 "/" $$3 == "locale/en-US") { p="iceweasel" } !done[p] { print p; done[p] = 1 }' debian/removed_conffiles)))

GENERATED_FILES += $(MAINTSCRIPTS)

debian/iceweasel.maintscript: debian/removed_conffiles
	(grep -v searchplugins/locale $<; grep searchplugins/locale/en-US $<) | awk '{print "rm_conffile /etc/iceweasel/" $$0, "45.0esr-2~", "iceweasel"}' > $@

debian/iceweasel-l10n-%.maintscript: debian/removed_conffiles
	grep -i searchplugins/locale/$* $< | awk '{print "rm_conffile /etc/iceweasel/" $$0, "45.0esr-2~", "iceweasel-l10n-$*"}' > $@
endif

override_dh_auto_configure:

stamps/prepare-configure:: stamps/dummy
	for dir in $(AUTOCONF_DIRS); do \
		for file in config.guess config.sub; do \
			sed -i '2!b;/^#/ i\exec "/usr/share/misc/'$$file'" "$$@"' $$dir/$$file; \
		done; \
	done
	cp -f configure.in configure && chmod +x configure
	cp -f js/src/configure.in js/src/configure && chmod +x js/src/configure

stamps/configure-check-$(PRODUCT):: stamps/configure-$(PRODUCT)
# Ensure --{with,enable}-system options properly set expected variables
# according to the definitions in the mozconfig file.
	@awk -F' *# *| *$$' ' \
	BEGIN { confs="$(foreach f,autoconf.mk emptyvars.mk,$(CURDIR)/build-$(PRODUCT)/config/$(f))" } \
	$$1 ~ /system/ { \
		if (! $$2) { \
			print FILENAME ": Missing variable for",$$1; \
			error=1; \
		} else { \
			split($$2,var,"="); \
			cmd = "grep -l " var[1] " " confs; \
			cmd | getline dir; \
			sub(/\/[^\/]*$$/, "", dir); \
			cmd = "$(MAKE) -C " dir " --no-print-directory echo-variable-" var[1]; \
			cmd | getline value; \
			if (value != var[2]) { print $$1, "triggered", var[1] "=" value,"instead of",$$2; error=1 } \
		} \
	} \
	END { if (error) { exit 1 }}' debian/$($(PRODUCT)).mozconfig

stamps/configure-$(PRODUCT):: stamps/prepare-configure debian/$($(PRODUCT)).mozconfig
	$(if $(wildcard build-$(PRODUCT)),,mkdir build-$(PRODUCT))
ifeq ($(DEB_HOST_ARCH_ENDIAN),big)
	echo mk_add_options MOZ_OBJDIR=build-$(PRODUCT) > build-$(PRODUCT)/mozconfig.icu
	cd build-$(PRODUCT) && MOZCONFIG=mozconfig.icu ../mach python ../intl/icu_sources_data.py "$(CURDIR)"
endif
	cd build-$(PRODUCT) && \
	MOZCONFIG=$(CURDIR)/debian/$($(PRODUCT)).mozconfig \
	ASFLAGS="-g" \
	$(CONFIGURE_ENV) \
	../configure --target=$(DEB_HOST_GNU_TYPE) --host=$(DEB_BUILD_GNU_TYPE) --prefix=/usr $(CONFIGURE_FLAGS) \
		$(BRANDING_CONFIGURE_FLAG)
	sed -i "/MOZ_APP_REMOTINGNAME/s/'[^']*',/'$(MOZ_APP_REMOTINGNAME)',/" build-$(PRODUCT)/config.status

stamps/build-$(PRODUCT):: stamps/configure-check-$(PRODUCT)
	dh_auto_build --parallel --builddirectory=build-$(PRODUCT) -- \
		LD_LIBS=-Wl,--no-gc-sections \
		_LEAKTEST_FILES=leaktest.py

L10N_BUILD_STAMPS = $(foreach lang,$(L10N_LANGS),stamps/build-l10n-$(lang))
stamps/build-l10n:: $(L10N_BUILD_STAMPS)

stamps/configure-l10n:: stamps/prepare-configure
	$(if $(wildcard build-l10n),,mkdir build-l10n)
	$(if $(wildcard l10n),,mkdir -p l10n)
	cd build-l10n && \
	MOZCONFIG=$(CURDIR)/debian/$($(PRODUCT)).mozconfig \
	$(CONFIGURE_ENV) \
	../configure --target=$(DEB_HOST_GNU_TYPE) --host=$(DEB_BUILD_GNU_TYPE) --prefix=/usr $(CONFIGURE_FLAGS) \
		--with-l10n-base=$(CURDIR)/l10n \
		$(BRANDING_CONFIGURE_FLAG)
	$(MAKE) -C build-l10n config/host CURRENT_TIER=compile

$(L10N_BUILD_STAMPS):: stamps/build-l10n-%: stamps/configure-l10n
	$(if $(wildcard l10n/$*),,ln -sf ../l10n-$* l10n/$*)
	-cd $(CURDIR)/build-l10n && PYTHONIOENCODING=utf-8 $(CURDIR)/mach compare-locales --merge $(CURDIR)/build-l10n/l10n-$* $(CURDIR)/$(PRODUCT)/locales/l10n.toml $(CURDIR)/l10n $*
	$(MAKE) -C build-l10n/$(PRODUCT)/locales langpack-$* MOZ_CHROME_FILE_FORMAT=flat MOZ_LANGPACK_EID=langpack-$*@$($(PRODUCT)).mozilla.org PKG_LANGPACK_BASENAME='$$(MOZ_LANGPACK_EID)' PKG_LANGPACK_PATH=xpi/ LOCALE_MERGEDIR=$(CURDIR)/build-l10n/l10n-$*

override_dh_auto_build override_dh_auto_build-arch: stamps/build-$(PRODUCT)

override_dh_auto_build-indep: stamps/build-l10n

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
# Disable tests on stable-security
ifeq (,$(findstring ~deb,$(DEBIAN_RELEASE)))
# Disable tests until they're fixed
#include debian/test.mk
endif
endif

override_dh_auto_clean::
	rm -f MPL
	rm -f $(filter-out debian/control,$(GENERATED_FILES))
	rm -f configure js/src/configure old-configure js/src/old-configure
	rm -rf stamps l10n
	debian/rules debian/control TESTDIR=

ifeq ($(DEB_HOST_ARCH_ENDIAN),big)
	rm -f config/external/icu/data/$(ICU_DATA_FILE)
endif

	dh_auto_clean --builddirectory=build-$(PRODUCT)
	dh_auto_clean --builddirectory=build-l10n
	find . -name "*.pyc" -o -name "*.pyo" | xargs --no-run-if-empty rm -f

	for dir in $(AUTOCONF_DIRS); do \
		for file in config.guess config.sub; do \
			sed -i '2!b;/^exec "/ d' $$dir/$$file; \
		done; \
	done

override_dh_clean:
	dh_clean -XCargo.toml.orig

override_dh_auto_install: stamps/install-$(PRODUCT)

stamps/install-$(PRODUCT)::
ifneq (,$(filter mips,$(DEB_HOST_ARCH)))
	sed -i '/"javascript.options.\(baselinejit\|ion\)"/s/true/false/' build-$(PRODUCT)/dist/bin/greprefs.js
endif
	dh_auto_install --builddirectory=build-$(PRODUCT) -- installdir=$(LIB_DIR) \
		MOZ_APP_BASE_VERSION=$(GRE_VERSION) \
		MOZ_PKG_MANIFEST=$(CURDIR)/debian/installer/package-manifest.$(PRODUCT) \
		TAR_CREATE_FLAGS="--exclude=.mkdir.done --hard-dereference -chf" \
		SIGN_NSS=

	-TZ=UTC unzip -d debian/tmp$(LIB_DIR)/browser debian/tmp$(LIB_DIR)/browser/omni.ja 'defaults/preferences/*'

MPL-1.1: gfx/cairo/cairo/COPYING-MPL-1.1
	cp -f $< $@

MPL-2.0: nsprpub/LICENSE
	cp -f $< $@

override_dh_installdocs: MPL-1.1 MPL-2.0
	dh_installdocs -A $^

L10N_DH_INSTALL_STAMPS = $(foreach lang,$(L10N_LANGS),stamps/dh_install-l10n-$(lang))
stamps/dh_install-l10n:: $(L10N_DH_INSTALL_STAMPS)

$(L10N_DH_INSTALL_STAMPS):: stamps/dh_install-l10n-%: stamps/build-l10n-%
	dh_install -p$($(PRODUCT))-l10n-$(call lc,$*) build-l10n/dist/xpi/langpack-$*@$($(PRODUCT)).mozilla.org.xpi $(LIB_DIR)/browser/extensions/

stamps/dh_install:: debian/noinstall
	awk '{print "debian/tmp/" $$1 }' < debian/noinstall | xargs rm -r
	dh_install --fail-missing

override_dh_install: stamps/dh_install

override_dh_strip:
ifneq (,$(filter wheezy jessie,$(DIST)))
	dh_strip -a --dbg-package=$($(PRODUCT))-dbg
else
	dh_strip --dbgsym-migration='$($(PRODUCT))-dbg (<< 49.0-4~)'
endif

override_dh_shlibdeps:
	dh_shlibdeps -a -l$(CURDIR)/debian/tmp$(LIB_DIR)

ifdef TRANSITION
override_dh_gencontrol:
	dh_gencontrol$(foreach pkg,$(subst $($(PRODUCT)),iceweasel,$(L10N_PACKAGES)) iceweasel-l10n-all, -p$(pkg)) -- -v1:$(DEBIAN_VERSION)
	dh_gencontrol$(foreach pkg,$(subst $($(PRODUCT)),iceweasel,$(L10N_PACKAGES)) iceweasel-l10n-all, -N$(pkg))
endif

install binary binary-arch binary-indep: $(filter-out %.pc,$(GENERATED_FILES))

binary binary-indep: stamps/dh_install-l10n

binary binary-arch binary-indep build build-arch build-indep clean install:
	dh $@

.PHONY: build clean binary-indep binary-arch binary install

.NOTPARALLEL:

$(eval ALL_STAMPS := $(shell awk -F:: '$$1 ~ /^stamps\// && !/%/ { print $$1 }' debian/rules) stamps/dummy $(L10N_BUILD_STAMPS) $(L10N_DH_INSTALL_STAMPS))
$(ALL_STAMPS)::
	@mkdir -p stamps
	$(if $(wildcard $@),,touch $@)
endif
