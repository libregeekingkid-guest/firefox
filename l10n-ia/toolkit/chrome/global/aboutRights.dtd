<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
<!-- rights.locale-direction instead of the usual local.dir entity, so RTL can skip translating page. -->
<!ENTITY rights.locale-direction "ltr">
<!ENTITY rights.title "A proposito de tu derectos">
<!ENTITY rights.intro "&brandFullName; es software gratis e de codice aperite, construite per un communitate de milles de contributores de tote le mundo. Il ha poc cosas que tu deberea saper:">

<!-- Note on pointa / pointb / pointc form:
     These points each have an embedded link in the HTML, so each point is
     split into chunks for text before the link, the link text, and the text
     after the link. If a localized grammar doesn't need the before or after
     chunk, it can be left blank.

     Also note the leading/trailing whitespace in strings here, which is
     deliberate for formatting around the embedded links. -->
<!ENTITY rights.intro-point1a "&brandShortName; es facite disponibile a te sub le terminos del ">
<!ENTITY rights.intro-point1b "Licentia public Mozilla">
<!ENTITY rights.intro-point1c ". Isto significa que tu pote usar, copiar e distribuer &brandShortName; a alteres.  Tu tamben es benvenite de modificar le codice fonte de &brandShortName; comocunque tu vole pro attinger tu necessitates. Le licentia public de Mozilla tamben te da le derecto de distribuer tu versiones modificate.">

<!ENTITY rights.intro-point2-a "Il non es concedite a te ulle derecto sur le marcas registrate o sur le licentias al marcas registrate del fundation Mozilla o de ulle tertie, includente le nomine e le logotypo de Firefox. Altere informationes sur le marcas registrate pote esser trovate ">
<!ENTITY rights.intro-point2-b "hic">
<!ENTITY rights.intro-point2-c ".">

<!-- point 2.5 text for official branded builds -->
<!ENTITY rights.intro-point2.5 "Alcun functionalitates in &brandShortName;, como le reportator de collapsos, te da le option de provider feedback a &vendorShortName;. Eligente inviar feedback, tu dara a &vendorShortName; permission de usar le feedback pro meliorar su productos, de publicar le feedback in su sitos web e de distribuer le feedback.">

<!-- point 3 text for official branded builds -->
<!ENTITY rights2.intro-point3a "Como nos usa tu information personal e feedback submittite a &vendorShortName; per &brandShortName; es describite in le ">
<!ENTITY rights2.intro-point3b "&brandShortName; Regulas de confidentialitate">
<!ENTITY rights.intro-point3c ".">

<!-- point 3 text for unbranded builds -->
<!ENTITY rights.intro-point3-unbranded "Omne politicas de confidentialitate applicabile pro iste producto debe esser listate hic.">

<!-- point 4 text for official branded builds -->
<!ENTITY rights2.intro-point4a "Alcun functionalitates de &brandShortName; usa servicios de information basate sur le web, totevia nos non garanti que illos es 100&#37; accurate o libere de errores. Plus detalios, incluso informationes sur como inactivar le functionalitates que usa iste servicios pote esser trovate in le ">
<!ENTITY rights.intro-point4b "terminos de servicio">
<!ENTITY rights.intro-point4c ".">

<!-- point 4 text for unbranded builds -->
<!ENTITY rights.intro-point4a-unbranded "Si iste producto incorpora servicios web, qualcunque terminos de servicio applicabile pro le servicio(s) debe esser ligate al ">
<!ENTITY rights.intro-point4b-unbranded "Servicios de sito web">
<!ENTITY rights.intro-point4c-unbranded " section.">

<!ENTITY rights2.webservices-header "&brandFullName; Servicios de information basate sur web">

<!-- point 5 -->
<!ENTITY rights.intro-point5 "A fin de poter leger alcun typos de contento de video, &brandShortName; discarga certe modulos de deciframento de contento de terties.">

<!-- Note that this paragraph references a couple of entities from
     preferences/security.dtd, so that we can refer to text the user sees in
     the UI, without this page being forgotten every time those strings are
     updated.  -->
<!-- intro paragraph for branded builds -->
<!ENTITY rights2.webservices-a "&brandFullName; usa servicios de informationes web (&quot;Servicios&quot;) pro fornir certe functionalitates fornite pro tu uso con iste version binari de &brandShortName; sub le terminos describite a basso. Si tu non vole usar le un o le altere Servicio(s) o le terminos a basso es inacceptabile, tu pote inactivar le functionalitate o le Servicio(s). Instructiones sur como inactivar un functionalitate o Servicio particular pote esser trovate ">
<!ENTITY rights2.webservices-b "hic">
<!ENTITY rights3.webservices-c ". Altere functionalitates e Servicios pote esser inactivate in le preferentias del application.">

<!-- safe browsing points for branded builds -->
<!ENTITY rights.safebrowsing-a "SafeBrowsing: ">
<!ENTITY rights.safebrowsing-b "Inactivar le functionalitate de navigation secur non es recommendate perque illo pote resultar in navigar per sitos insecur.  Si tu desira inactivar le functionalitate completemente, seque iste passos:">
<!ENTITY rights.safebrowsing-term1 "Aperir le preferentias de application">
<!ENTITY rights.safebrowsing-term2 "Eliger le selection de securitate">
<!ENTITY rights2.safebrowsing-term3 "Dismarca le option pro &quot;&enableSafeBrowsing.label;&quot;">
<!ENTITY rights.safebrowsing-term4 "Le navigation secur es disactivate">

<!-- location aware browsing points for branded builds -->
<!ENTITY rights.locationawarebrowsing-a "Navigation secundo le geolocalisation: ">
<!ENTITY rights.locationawarebrowsing-b "es sempre optional.  Nulle information de localisation es inviate sin tu permission.  Si tu desira inactivar le functionalitate completemente, seque iste passos:">
<!ENTITY rights.locationawarebrowsing-term1a "In barra del URL, insere ">
<!ENTITY rights.locationawarebrowsing-term1b "about:config">
<!ENTITY rights.locationawarebrowsing-term2 "Insere geo.enabled">
<!ENTITY rights.locationawarebrowsing-term3 "Clicca duple sur le preferentia geo.enabled">
<!ENTITY rights.locationawarebrowsing-term4 "Le navigation con localisation geographic es disactivate">

<!-- intro paragraph for unbranded builds -->
<!ENTITY rights.webservices-unbranded "Un resumpto del servicios del sito web que le producto incorpora, accompaniate de instructiones sur como inactivar los, si applicabile, debe esser includite hic.">

<!-- point 1 text for unbranded builds -->
<!ENTITY rights.webservices-term1-unbranded "Tote le terminos de servicio applicabile pro iste producto debe ser listate ci.">

<!-- points 1-7 text for branded builds -->
<!ENTITY rights2.webservices-term1 "&vendorShortName; e su contributores, licentiantes e partenarios labora pro provider le servicios le plus accurate e actualisate.  Totevia nos non pote garantir que iste informationes es exhaustive e libere de errores.  Pro exemplo, le servicio de navigation secur non pote identificar alcun sitos malfaciente e pote suspectar de alcun sitos secur erroneemente e in le servicio de geolocalisation, tote le localisationes retornate per nostre fornitores de servicio es estimate solmente e ni nos ni nostre fornitores garanti le precision del localisationes fornite.">
<!ENTITY rights.webservices-term2 "&vendorShortName; pote discontinuar o cambiar le Servicios a su discretion.">
<!ENTITY rights2.webservices-term3 "Tu es benvenite a usar iste Servicios con le version accompaniante de &brandShortName;, e &vendorShortName; te concede derectos de facer lo.  &vendorShortName; e su licentiantes reserva tote le altere derectos sur le Servicios.  Iste terminos non es intendite a limitar qualcunque derectos concedite sub altere licentias de codice aperite applicabile a &brandShortName; e al versiones de codice fonte correspondente de &brandShortName;.">
<!ENTITY rights.webservices-term4 "Le Servicios es fornite &quot;tal qual.&quot;  &vendorShortName;, su contributores, licentiantes e distributores, renega tote le garantias, expresse o implicite, inclusive sin limitation, garantias que le Servicios es commerciabile e conveni pro tu propositos particular.  Tu supporta tote le risco de seliger le Servicios pro tu propositos concernente le qualitate e le rendimento del Servicios. Alcun jurisdictiones non permitte le exclusion o limitation de garantias implicite, alora iste clausula non se applica a te.">
<!ENTITY rights.webservices-term5 "Salvo si requirite per lege, &vendorShortName;, su contributores, licentiantes e distributores non essera responsabilisate per qualcunque damno indirecte, incidental, consequential, punitive o exemplar decurrente de o de omne maniera concernente le uso de &brandShortName; e le Servicios.  Le responsabilitate collective sub iste terminos non excedera $500 (cinque centos dollars). Alcun jurisdictiones non permitte le exclusion o limitation de certe damnos, alora iste exclusion e limitation pote non applicar se a te. ">
<!ENTITY rights.webservices-term6 "Alicun tempore &vendorShortName; pote actualisar le terminos si necessari. Iste terminos non pote ser modificate o cancellate sin consenso scripte de &vendorShortName; .">
<!ENTITY rights.webservices-term7 "Iste terminos es governate per le leges del stato de California, SUA, excludente su conflicto de provisiones legal. Si qualcunque potion de iste terminos es tenite como invalide o non applicabile, le portiones restante remanera in tote fortia e effectos. In un eventual conflicto inter un version traducite de iste terminos e le version in lingua anglese, le version in lingua anglese prevalera.">
