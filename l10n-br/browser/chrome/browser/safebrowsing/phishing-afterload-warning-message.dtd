<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Mont war-gil">
<!ENTITY safeb.palm.seedetails.label "Gwelet ar munudoù">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "N'eo ket ul lec’hienn dagus…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Alioù kinniget gant <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Gweladenniñ al lec'hienn-mañ a c'hall ober droug d’hoc’h urzhiataer">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "Stanket eo bet ar bajenn gant &brandShortName; dre ma c'hall klask staliañ meziantoù touellus a c'hallfe laerañ pe dilemel titouroù personel war hoc'h urzhiataer.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> a zo bet <a id='error_desc_link'>danevellet abalamour d'ur meziant arvarus enni</a>. Gallout a rit <a id='report_detection'>danevelliñ ur fazi dinoiñ</a> pe <a id='ignore_warning_link'>leuskel ar gemennadenn a-gostez</a> ha kenderc'hel war al lec'hienn diasur-mañ.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> a zo bet <a id='error_desc_link'>danevellet abalamour d'ur meziant arvarus enni</a>. Gallout a rit <a id='report_detection'>danevelliñ ur fazi dinoiñ</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Deskit hiroc'h a-zivout endalc'hadoù web arvarus, viruzoù ha drougveziantoù en o zouez, ha penaos gwareziñ hoc'h urzhiataer war <a id='learn_more_link'>StopBadware.org</a>. Deskit hiroc'h a-zivout gwarez a-enep d'an higennañ ha d'an drougveziantoù enkorfet e &brandShortName; war <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Al lec'hienn a c'hall enderc'hel goulevioù arvarus">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "Stanket eo bet ar bajenn-mañ gant &brandShortName; peogwir e c'hall gwallbakañ ac'hanoc'h da staliañ goulevioù a dag hoc'h arnod merdeiñ (da skouer, en ur gemmañ ho fennbajenn pe o diskouez bruderezhioù ouzhpenn war lec'hiennoù a weladennit).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Danevellet eo bet <span id='unwanted_sitename'/> evel <a id='error_desc_link'>ul lec'hienn gant meziantoù arvarus enni</a>. Gallout a rit <a id='ignore_warning_link'>leuskel ar riskl-mañ a-gostez</a> ha mont etrezek al lec'hienn diasur.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Danevellet eo bet <span id='unwanted_sitename'/> evel <a id='error_desc_link'>ul lec'hienn gant meziantoù arvarus enni</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Deskit hiroc'h a-zivout meziantoù arvarus ha dic'hoantet war <a id='learn_more_link'>Reolenn a-fed Meziantoù Dic'hoantet</a>. Deskit hiroc'h a-zivout gwarez &brandShortName; a-enep d'an higennañ hag an drougveziantoù e <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Lec'hienn douellus">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "Stanket eo bet ar bajenn gant &brandShortName; rak gallout a ra touellañ ac'hanoc'h d'ober traoù arvarus eel staliañ meziantoù pe diskouez titouroù personel evel gerioù-tremen pe kartennoù kred.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Danevellet eo bet <span id='phishing_sitename'/> evel <a id='error_desc_link'>ul lec'hienn touellus</a>. Gallout a rit <a id='report_detection'>danevelliñ ur gudenn dinoiñ</a> pe <a id='ignore_warning_link'>leuskel ar riskl a-gostez</a> ha mont etrezek al lec'hienn diasur.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Danevellet eo bet <span id='phishing_sitename'/> evel <a id='error_desc_link'>ul lec'hienn touellus</a>. Gallout a rit <a id='report_detection'>danevelliñ ur gudenn dinoiñ</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Deskit hiroc'h a-zivout lec'hiennoù touellus ha higennañ war <a id='learn_more_link'>www.antiphishing.org</a>. Deskit hiroc'h a-zivout gwarez &brandShortName; a-enep an higennañ hag an drougveziantoù war <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Al lec'hienn a c'hell enderc'hel drougveziantoù">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "Stanket eo bet ar bajenn gant &brandShortName; peogwir e c'hall klask staliañ arloadoù arvarus a laer pe lamm kuit ho titouroù (skeudennoù, gerioù-tremen, kemennadennoù ha kartennoù kred).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Danevellet eo bet <span id='harmful_sitename'/> evel <a id='error_desc_link'>ul lec'hienn gant un arload arvarus</a>. Gallout a rit <a id='ignore_warning_link'>leuskel ar riskl a-gostez</a> ha mont etrezek al lec'hienn diasur.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Dinoet eo bet <span id='harmful_sitename'/> evel <a id='error_desc_link'>ul lec'hienn gant un arload a c'hall bezañ arvarus</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Deskit hiroc'h a-zivout gwarez &brandShortName; a-enep d'an higennañ hag an drougveziantoù war <a id='firefox_support'>support.mozilla.org</a>.">
