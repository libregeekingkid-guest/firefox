<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Oer ûntwikkelershelpmiddelen">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Firefox-ûntwikkelershelpmiddelen ynskeakelje">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Skeakel Firefox-ûntwikkelershelpmiddelen yn omElemint ynspektearje te brûken">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Bestudearje en bewurkje HTML en CSS mei de Inspector fan de ûntwikkelershelpmiddelen.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Untwikkel en debug WebExtensions, web workers, service workers en mear mei de Firefox-ûntwikkelershelpmiddelen.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Jo hawwe in snelkeppeling fan de ûntwikkelershelpmiddelen aktivearre. As dat net de bedoeling wie, kinne jo dit ljepblêd slute.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Bestudearje, bewurkje en debug HTML, CSS en JavaScript mei helpmiddelen lykas de Inspector en Debugger.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Optimalisearje HTML, CSS en JavaScript fan jo website mei helpmiddelen lykas de Inspector en Debugger.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Firefox-ûntwikkelershelpmiddelen binne standert útskeakele, sadat jo mear kontrôle hawwe oer jo browser.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Lês mear oer Untwikkelershelpmiddelen">

<!ENTITY  aboutDevtools.enable.enableButton "Untwikkelershelpmiddelen ynskeakelje">
<!ENTITY  aboutDevtools.enable.closeButton "Dizze side slute">

<!ENTITY  aboutDevtools.enable.closeButton2 "Dit ljepblêd slute">

<!ENTITY  aboutDevtools.welcome.title "Wolkom by Firefox Developer Tools!">

<!ENTITY  aboutDevtools.newsletter.title "Mozilla Developer nijsbrief">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Untfang ûntwikkelersnijs, trúks en helpboarnen streekrjocht yn jo Postfek YN.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "E-mailadres">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Ik bin it iens mei hoe't jo mei dizze gegevens omgeane, lykas útlein yn jo <a class='external' href='https://www.mozilla.org/privacy/'>privacybelied</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Abonnearje">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Tank!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "As jo noch net earder in ynskriuwing foar in Mozilla-relatearre nijsbrief befêstige hawwe, moatte jo dit mooglik dwaan. Kontrolearje jo Postfek YN of jo spamfilter foar in e-mail fan ús.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Sykje jo mear as allinnich ûntwikkelershelpmiddelen? Probearje de Firefox-browser dy't spesifyk boud is foar ûntwikkelers en moderne wurkstreamen.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Mear ynfo">

