<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  fontsAndEncodingsDialog.title           "შრიფტები და კოდირებები">

<!ENTITY  language.label                          "შრიფტები:">
<!ENTITY  language.accesskey                      "t">

<!ENTITY  size.label                              "ზომა:">
<!ENTITY  sizeProportional.accesskey              "გ">
<!ENTITY  sizeMonospace.accesskey                 "ი">

<!ENTITY  proportional.label                      "პროპორციული:">
<!ENTITY  proportional.accesskey                  "P">

<!ENTITY  serif.label                             "კონტრასტული:">
<!ENTITY  serif.accesskey                         "s">
<!ENTITY  sans-serif.label                        "ერთგვაროვანი:">
<!ENTITY  sans-serif.accesskey                    "n">
<!ENTITY  monospace.label                         "მონოსიგანის:">
<!ENTITY  monospace.accesskey                     "m">

<!-- LOCALIZATION NOTE (font.langGroup.latin) :
     Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language. -->
<!ENTITY  font.langGroup.latin                    "ლათინური">
<!ENTITY  font.langGroup.japanese                 "იაპონური">
<!ENTITY  font.langGroup.trad-chinese             "ჩინური ტრადიციული (ტაივანი)">
<!ENTITY  font.langGroup.simpl-chinese            "ჩინური გამარტივებული">
<!ENTITY  font.langGroup.trad-chinese-hk          "ჩინური ტრადიციული (ჰონგ კონგი)">
<!ENTITY  font.langGroup.korean                   "კორეული">
<!ENTITY  font.langGroup.cyrillic                 "კირილიცა">
<!ENTITY  font.langGroup.el                       "ბერძნული">
<!ENTITY  font.langGroup.other                    "სხვა დამწერლობები">
<!ENTITY  font.langGroup.thai                     "ტაი">
<!ENTITY  font.langGroup.hebrew                   "ებრაული">
<!ENTITY  font.langGroup.arabic                   "არაბული">
<!ENTITY  font.langGroup.devanagari               "დევანაგირი">
<!ENTITY  font.langGroup.tamil                    "თამილური">
<!ENTITY  font.langGroup.armenian                 "სომხური">
<!ENTITY  font.langGroup.bengali                  "ბენგალური">
<!ENTITY  font.langGroup.canadian                 "უნიფიცირებული კანადური მარცვლოვანი">
<!ENTITY  font.langGroup.ethiopic                 "ეთიოპური">
<!ENTITY  font.langGroup.georgian                 "ქართული">
<!ENTITY  font.langGroup.gujarati                 "გუჯარათი">
<!ENTITY  font.langGroup.gurmukhi                 "გურმუხი">
<!ENTITY  font.langGroup.khmer                    "ქჰმერული">
<!ENTITY  font.langGroup.malayalam                "მალაილამი">
<!ENTITY  font.langGroup.math                     "მათემატიკა">
<!ENTITY  font.langGroup.odia                     "ორია">
<!ENTITY  font.langGroup.telugu                   "ტელუგუ">
<!ENTITY  font.langGroup.kannada                  "კანადა">
<!ENTITY  font.langGroup.sinhala                  "სინჰალური">
<!ENTITY  font.langGroup.tibetan                  "ტიბეტური">
<!-- Minimum font size -->
<!ENTITY minSize.label                            "შრიფტის უმცირესი ზომა:">
<!ENTITY minSize.accesskey                        "z">
<!ENTITY minSize.none                             "არა">

<!-- default font type -->
<!ENTITY  useDefaultFontSerif.label               "კონტრასტული">
<!ENTITY  useDefaultFontSansSerif.label           "ერთგვაროვანი">

<!-- fonts in message -->
<!ENTITY  fontControl.label                       "შრიფტების მართვა">
<!ENTITY  useFixedWidthForPlainText.label         "მონოსიგანის შრიფტებით სარგებლობა ტექსტური გზავნილებისთვის">
<!ENTITY  fixedWidth.accesskey                    "x">
<!ENTITY  useDocumentFonts.label                  "წერილებში სხვა შრიფტების გამოყენების უფლება">
<!ENTITY  useDocumentFonts.accesskey              "o">

<!-- Language settings -->
<!ENTITY sendDefaultCharset.label         "გამავალი ფოსტა:">
<!ENTITY sendDefaultCharset.accesskey     "u">
<!ENTITY languagesTitle2.label            "ტექსტის კოდირება">
<!ENTITY composingDescription2.label      "ტექსტის ნაგულისხმევი კოდირების მითითება, წერილების გაგზავნა/მიღებისთვის">

<!ENTITY viewDefaultCharsetList.label     "შემოსული ფოსტა:">
<!ENTITY viewDefaultCharsetList.accesskey  "I">
<!ENTITY replyInDefaultCharset3.label     "როცა შესაძლებელია, ტექსტის ნაგულისხმევი კოდირების გამოყენება პასუხისას">
<!ENTITY replyInDefaultCharset3.accesskey "პ">
